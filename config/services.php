<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => 'sandbox0cab25f24064433890b3338acf5d51a8.mailgun.org',
        'secret' => 'key-6f1f977bf637f6a9e3532a38fe012573',
    ],

    'mandrill' => [
        'secret' => 'HD_5bDovpOaakb98Rffi3w',
    ],

    'ses' => [
        'key'    => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => '',
        'secret' => '',
    ],


    'facebook' => [
        'client_id' => env('FB_CLIENT_ID'),
        'client_secret' => env('FB_SECRET_ID'),
        'redirect' => env('FB_REDIRECT'),
    ]

/*    'facebook' => [
        'client_id' => '1472761816354669',
        'client_secret' => 'ee080c9cf9cd603e59db71c27560ee8e',
        'redirect' => 'http://stephane.livetestcar.dev/auth/facebook/callback',
    ]
*/
];
