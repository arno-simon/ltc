<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->timestamps();
			$table->string('last_name', 30)->nullable();
			$table->string('first_name', 30)->nullable();
			$table->string('email', 100);
			$table->string('password', 100);
			$table->string('phone', 12)->nullable();
			$table->string('register_ip', 15)->nullable();
			$table->string('remember_token', 100)->nullable();
			$table->boolean('isTesteur')->default(false);
			$table->boolean('isPreteur')->default(false);
		});
	}

	public function down()
	{
		Schema::drop('users');
	}
}