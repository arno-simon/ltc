<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCarCountryTable extends Migration {

	public function up()
	{
		Schema::create('car_country', function(Blueprint $table) {
			$table->bigInteger('car_id')->unsigned();
			$table->integer('country_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('car_country');
	}
}