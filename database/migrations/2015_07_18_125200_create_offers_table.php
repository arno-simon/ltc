<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOffersTable extends Migration {

	public function up()
	{
		Schema::create('offers', function(Blueprint $table) {
			$table->increments('id');
			$table->bigInteger('testeur_id')->unsigned()->nullable();
			$table->bigInteger('preteur_id')->unsigned();
			$table->integer('localisation_id')->unsigned();
			$table->bigInteger('car_id')->unsigned();
			$table->datetime('test_date');
			$table->timestamps();
			$table->boolean('isDone')->default(false);
		});
	}

	public function down()
	{
		Schema::drop('offers');
	}
}