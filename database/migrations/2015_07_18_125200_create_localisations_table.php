<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocalisationsTable extends Migration {

	public function up()
	{
		Schema::create('localisations', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 100);
			$table->string('address', 100);
			$table->string('city', 30);
			$table->string('zip_code', 16);
			$table->integer('country_id')->unsigned();
			$table->float('latitude');
			$table->float('longitude');
			$table->integer('radius');
		});
	}

	public function down()
	{
		Schema::drop('localisations');
	}
}