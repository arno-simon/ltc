<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCarsTable extends Migration {

	public function up()
	{
		Schema::create('cars', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('brand', 30);
			$table->string('model', 30);
			$table->smallInteger('year');
		});
	}

	public function down()
	{
		Schema::drop('cars');
	}
}