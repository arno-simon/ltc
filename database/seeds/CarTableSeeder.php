<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Excel::filter('chunk')->load('storage/vehicules2.csv')->chunk(250, function($results)
        {
            foreach($results as $row)
            {
                DB::table('cars')->insert([
                    'brand' => $row->make,
                    'model' => $row->model,
                    'year' => (int)$row->year,
                ]);
            }
        });

        Model::reguard();
    }
}
