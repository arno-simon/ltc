$(function() {

	var connexionHeight = $(".connexion-header").height();

	var isMobile = window.matchMedia("only screen and (max-width: 767px)");

	$(".connexion-header").css("top",-connexionHeight);

	$(".link-connexion-header").on('click',function() {

		var pTop = $(".connexion-header").position();

		if(pTop.top == 100) {
			$(".connexion-header").animate({
			    top: -connexionHeight,
			  	}, 1000, function() {
			    // Animation complete.
	  		});
		}else if(isMobile.matches) {
			$(".connexion-header").animate({
			    top: 51,
			  	}, 1000, function() {
			    // Animation complete.
		  	});
		}else {
			$(".connexion-header").animate({
			    top: 100,
			  	}, 1000, function() {
			    // Animation complete.
		  	});
		}

		return false;
	});

	$(".close-connexion").on('click',function() {
		$(".connexion-header").animate({
		    top: -connexionHeight,
		  	}, 1000, function() {
		    // Animation complete.
  		});
	});
	
});