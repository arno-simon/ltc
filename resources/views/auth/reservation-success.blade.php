@extends('template')

@section('title')
   Réservation d'un test - Live Test Car
@stop

@section('body')

<!--============================== content =================================-->
<section class="content gabarit">
       <!--============================== création compte succès =================================-->
       <div class="container-fluid creation-compte-success">
           <div class="row">
               <div class="col-sm-3"></div>
               <div class="col-xs-12 col-sm-6">
                   <h1 data-icon="K" class="bandeau-titre-noir">Réservation d'un test</h1>
                   <div class="content-creation-success">
                       <h2 class="message-success">Votre réservation s'est déroulée avec succès</h2>
                       <p>Un email de confirmation va vous être envoyé.</p>
                       <a href="/" class="link-ltc">Retourner à la page d'accueil</a>
                   </div>
               </div>
               <div class="col-sm-3"></div>
           </div>
       </div>
</section><!-- fin div id="content" -->
@stop