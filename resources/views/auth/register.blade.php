@extends('template')

@section('title')
   Créer un compte - Live Test Car
@stop

@section('body')

<!--============================== content =================================-->
<section class="content gabarit">


    <!--============================== formulaire création compte =================================-->
    <div class="container-fluid creation-compte">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-xs-12 col-sm-6">
                {!! Form::open(array('route' => 'register.post', 'class' => 'form-creation-compte')) !!}
                    <h1 data-icon="K" class="bandeau-titre-noir">Créer un compte</h1>
                    <div class="content-form-creation">
                        @if (count($errors) > 0)
                            <p class="message-erreur">Votre inscription ne s'est pas effectuée correctement.</p>
                            <p class="message-erreur">Nous avons rencontré {{ count($errors) > 1 ? 'les erreurs suivantes' : 'l\'erreur suivante' }} :</p>
                            <ul>
                                 @foreach ($errors->all() as $error)
                                <li class="message-erreur">{{ $error }}</li>
                                @endforeach  
                            </ul>
                        @endif

                        <label for="email">Adresse mail :</label>
                        <input type="email" name="email" value="{{ old('email') }}" class="input-ltc" placeholder="E-mail">
                        <label for="password">Mot de passe :</label>
                        <input type="password" name="password" class="input-ltc" placeholder="******">
                        <label for="password_confirmation">Confirmation du mot de passe :</label>
                        <input type="password" name="password_confirmation" class="input-ltc" placeholder="******">
                        <input type="hidden" name="parrainage" value="{{$parrainage->id}}">
                        <input type="submit" value="ok" class="btn-ltc">
                        <p>Pour essayer ou proposer un véhicule vous devez préalablement compléter votre profil</p>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="col-sm-3"></div>
        </div>
    </div>
</section><!-- fin div id="content" -->
@stop