@extends('template')

@section('title')
    Live Test Car
    @stop

    @section('body')
            <!--============================== content =================================-->
    <section class="content gabarit">
        <!--============================== page messagerie =================================-->
        <div class="container-fluid page-messagerie">
            <h1>Messagerie</h1>
            <div class="row">
                <!-- partie messagerie -->
                <div class="col-xs-12 col-sm-8">
                    <h2 data-icon="4" class="bandeau-titre-noir">Boite de réception</h2>
                    <div class="container-reception">
                        <div class="row">
                            <ul class="block-reception-gauche col-xs-12 col-sm-6 col-md-5">
                                @for($i=0; $i<count($reservations); $i++)
                                    <li @if($reservations[$i]->id == $reservationActive->id){!! 'class="active"' !!}@endif id="message{{$i}}">
                                    @if (\Auth::user()->id == $reservations[$i]->testeur_id)
                                        @include('auth.messagerie.testeur')
                                    @else
                                        @include('auth.messagerie.preteur')
                                    @endif
                                    </li>
                                @endfor
                            </ul>
                            <ul class="block-reception-droite col-xs-12 col-sm-6 col-md-7">
                                @for($i=0; $i<count($reservations); $i++)
                                <div class="contenu-messagerie @if($reservations[$i]->id == $reservationActive->id){{ "active" }}@endif" id="contenu-message{{$i}}">
                                    <p>Rendez vous le {{$reservations[$i]->reservation_at->format('j F')}}</p>
                                    @foreach($reservations[$i]->retrieveMessages() as $message)
                                        <li>
                                            <figure class="photo-messagerie">
                                                <img src="{{$message->expediteur->picture or "/img/photo-profil-vide.png"}}">
                                            </figure><div class="texte-message">
                                                <h3>{{$message->expediteur->first_name}} {{substr($message->expediteur->last_name, 0, 1)}}<span class="heure-message">{{$message->created_at->format('j M\. H\hi')}}</span></h3>
                                                <p>{{$message->texte}}</p>
                                            </div>
                                        </li>
                                    @endforeach
                                    <div class="suivant">
                                        <span></span>
                                        {!! Form::open(array('route' => 'messages.post', 'class' => 'form-creation-compte')) !!}
                                        <input type="hidden" name="reservation_id" value="{{$reservations[$i]->id}}">
                                        <input type="hidden" name="expediteur_id" value="{{\Auth::user()->id}}">
                                        <textarea name="messagerie" placeholder="ecrivez votre réponse" rows="5"></textarea>
                                        <input type="submit" name="submit" class="pull-right btn-ltc" value="Valider">
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                @endfor
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- partie droite -->
                <div class="col-xs-12 col-sm-4">
                <article class="col-xs-12 colonne-guide" ontouchstart="this.classList.toggle('hover');">
                    <div class="block-all-guide">
                        <h2>Un titre un peu plus grand qui fait deux lignes</h2>
                        <a href="#"  style="background-image: url('../img/test-guide.png')">
                            <span class="guide-overlay"></span>
                            <span class="btn-link-ltc">En savoir +</span>
                        </a>
                    </div>
                </article>
                </div>
            </div>
        </div>
    </section>
@stop