@extends('template')

@section('title')
   Création de compte - Live Test Car
@stop

@section('body')

<!--============================== content =================================-->
<section class="content gabarit">

   @if(session()->has('register_ok'))
       <!--============================== création compte succès =================================-->
       <div class="container-fluid creation-compte-success">
           <div class="row">
               <div class="col-sm-3"></div>
               <div class="col-xs-12 col-sm-6">
                   <h1 data-icon="K" class="bandeau-titre-noir">Créer un compte</h1>
                   <div class="content-creation-success">
                       <h2 class="message-success">Votre inscription s'est déroulée avec succès</h2>
                       <p>Un email de confirmation va vous être envoyé. Suivez les instructions contenus dans le mail afin de valider votre inscritpion</p>
                       <a href="/" class="link-ltc">Retourner à la page d'accueil</a>
                   </div>
               </div>
               <div class="col-sm-3"></div>
           </div>
       </div>
   @elseif(session()->has('confirmation_ok'))
         <!--============================== confirmation email compte succès =================================-->
        <div class="container-fluid creation-compte-success">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-xs-12 col-sm-6">
                    <h1 data-icon="K" class="bandeau-titre-noir">Créer un compte</h1>
                    <div class="content-creation-success">
                        <h2 class="message-success">Votre e-mail a bien été validé</h2>
                        <a href="/" class="link-ltc">Retourner à la page d'accueil</a>
                    </div>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </div>
    @endif
</section><!-- fin div id="content" -->
@stop