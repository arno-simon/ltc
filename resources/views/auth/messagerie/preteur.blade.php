<!-- Nbr de conversation a inclure sur les liste non active uniquement, variable pour le nombre de convesations en suspens-->
        <!--<span class="nbr-conversation"></span>-->
        <figure class="photo-messagerie">
            <img src="{{$reservations[$i]->testeur->picture or "/img/photo-profil-vide.png"}}">
        </figure><div class="texte-message">
            <h3>{{$reservations[$i]->testeur->first_name}} {{substr($reservations[$i]->testeur->last_name, 0, 1)}}.</h3>
            <p>{{$reservations[$i]->offer->car->model}}</p>
        </div>
        <div class="lien-message">
            <a href="{{ url('user/profil', [$reservations[$i]->testeur_id]) }}" class="link-profil"><span data-icon="," class="icon"></span><span class="texte">Voir le profil</span></a>
        </div>
