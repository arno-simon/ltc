@extends('template')

@section('title')
    Connexion - Live Test Car
    @stop

    @section('body')
            <!--============================== content =================================-->
    <section class="content gabarit">
        <!--============================== formulaire login =================================-->
        <div class="container-fluid creation-compte">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-xs-12 col-sm-6">
                    {!! Form::open(array('route' => 'login.post', 'class' => 'form-creation-compte')) !!}
                    <h1 data-icon="K" class="bandeau-titre-noir">Connexion</h1>
                    <div class="content-form-creation">
                        @if (count($errors->login) > 0)
                            <p class="message-erreur">
                                @foreach ($errors->login->all() as $error)
                                    <span class="txt-underline">{{ $error }}</span><br/>
                                @endforeach
                            </p>
                        @endif
                        <div class="container-login-fb">
                            <a class="login-fb" href="auth/facebook" role="button" data-icon="B"><span>connexion avec facebook</span></a>
                            <p class="separator"><span>ou</span></p>
                        </div>
                        <label for="email">Adresse mail :</label>
                        <input type="email" name="email" class="input-ltc" placeholder="E-mail">
                        <label for="password">Mot de passe :</label>
                        <input type="password" name="password" class="input-ltc" placeholder="******">
                        <input type="submit" value="ok" class="btn-ltc">
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-sm-3"></div>
            </div>
        </div>
    </section><!-- fin div id="content" -->
@stop