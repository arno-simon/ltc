@extends('template')

@section('title')
   Réinitialiser le mot de passe - Live Test Car
@stop

@section('body')
 <!--============================== content =================================-->
   <section class="content gabarit">
       @if(session()->has('status'))
               <!--============================== création compte succès =================================-->
       <div class="container-fluid creation-compte-success">
           <div class="row">
               <div class="col-sm-3"></div>
               <div class="col-xs-12 col-sm-6">
                   <h1 data-icon="K" class="bandeau-titre-noir">Réinitialisation du mot de passe</h1>
                   <div class="content-creation-success">
                       <h2 class="message-success">E-mail de changement de mot de passe envoyé</h2>
                       <p>Suivez les instructions contenus dans le mail afin de changer votre mot de passe</p>
                       <a href="/" class="link-ltc">Retourner à la page d'accueil</a>
                   </div>
               </div>
               <div class="col-sm-3"></div>
           </div>
       </div>
       @else
       <!--============================== formulaire email =================================-->
       <div class="container-fluid creation-compte">
           <div class="row">
               <div class="col-sm-3"></div>
               <div class="col-xs-12 col-sm-6">
                   {!! Form::open(array('route' => 'password.email.post', 'class' => 'form-creation-compte')) !!}
                       <h1 data-icon="K" class="bandeau-titre-noir">Réinitialisation du mot de passe</h1>
                       <div class="content-form-creation">
                           @if (count($errors) > 0)
                               <p class="message-erreur">
                                   @foreach ($errors->all() as $error)
                                       <span class="txt-underline">{{ $error }}</span><br/>
                                   @endforeach
                               </p>
                           @endif
                           <label for="email">Adresse mail :</label>
                           <input type="email" name="email" class="input-ltc" placeholder="e-mail">
                           <input type="submit" value="Envoyer un nouveau mot de passe" class="btn-ltc">
                       </div>
                   {!! Form::close() !!}
               </div>
               <div class="col-sm-3"></div>
           </div>
       </div>
       @endif
   </section><!-- fin div id="content" -->
@stop