@extends('template')

@section('title')
   Créer un compte - Live Test Car
@stop

@section('body')

<!--============================== content =================================-->
<section class="content gabarit">


    <!--============================== formulaire création compte =================================-->
    <div class="container-fluid creation-compte">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-xs-12 col-sm-6">
                {!! Form::open(array('route' => 'password.reset.post', 'class' => 'form-creation-compte')) !!}
                <h1 data-icon="K" class="bandeau-titre-noir">Changer son mot de passe</h1>
                <div class="content-form-creation">
                    @if (count($errors) > 0)
                        <p class="message-erreur">Votre inscription ne s'est pas effectuée correctement.<br/>
                            Nous avons rencontré {{ count($errors) > 1 ? 'les erreurs suivantes' : 'l\'erreur suivante' }} :<br/>
                            @foreach ($errors->all() as $error)
                                <span class="txt-underline">{{ $error }}</span><br/>
                            @endforeach
                        </p>
                    @endif
                    <input type="hidden" name="token" value="{{ $token }}">
                    <label for="email">Adresse mail :</label>
                    <input type="email" name="email" value="{{ old('email') }}" class="input-ltc" placeholder="e-mail">
                    <label for="password">Mot de passe :</label>
                    <input type="password" name="password" class="input-ltc" placeholder="******">
                    <label for="password_confirmation">Confirmation du mot de passe :</label>
                    <input type="password" name="password_confirmation" class="input-ltc" placeholder="******">
                    <input type="submit" value="Changer le mot de passe" class="btn-ltc">
                </div>
                {!! Form::close() !!}

            </div>
            <div class="col-sm-3"></div>
        </div>
    </div>
</section><!-- fin div id="content" -->
@stop