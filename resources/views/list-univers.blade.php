<div class="container-list-univers">
    <div id="carousel-link-univers" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <ul class="col-xs-8 col-xs-offset-2 item active">
                <li>
                    <a href="citadine" style="background-image: url('../img/citadine.png')"><span>Citadine</span></a>
                </li><li>
                    <a href="break" style="background-image: url('../img/break.png')"><span>Break</span></a>
                </li><li>
                    <a href="monospace" style="background-image: url('../img/monospace.png')"><span>Monospace</span></a>
                </li><li>
                    <a href="berline" style="background-image: url('../img/berline.png')"><span>Berline</span></a>
                </li>
            </ul>
            <ul class="col-xs-8 col-xs-offset-2 item">
                <li>
                    <a href="4x4" style="background-image: url('../img/4x4.png')"><span>4 x 4</span></a>
                </li><li>
                    <a href="sportive" style="background-image: url('../img/sportive.png')"><span>Sportive</span></a>
                </li><li>
                    <a href="collection" style="background-image: url('../img/collection.png')"><span>Collection</span></a>
                </li><li>
                    <a href="electrique" style="background-image: url('../img/electrique.png')"><span>Electrique</span></a>
                </li>
            </ul>
            <ul class="col-xs-8 col-xs-offset-2 item">
                <li>
                    <a href="tuning" style="background-image: url('../img/tuning.png')"><span>Tuning</span></a>
                </li>
            </ul>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-link-univers" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" data-icon="R" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-link-univers" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" data-icon="O" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
