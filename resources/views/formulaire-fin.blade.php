@extends('template')

@section('title')
   formulaire fin de test - Live Test Car
@stop
@section('header')
    <script type="text/javascript" src="/js/formulaire-notation.js"></script>
    <script>
        $(function () {
            $('.rating').each(function () {
                $('<span class="label label-default"></span>')
                        .text($(this).val() || ' ')
                        .insertAfter(this);
            });
            $('.rating').on('change', function () {
                $(this).next('.label').text($(this).val());
            });
        });
    </script>
@stop
@section('body')
<section class="content gabarit">

	<!--============================== page formulaire fin de test =================================-->
    <div class="container-fluid page-formulaire">
        <h1>Validation de fin de test</h1>
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 texte-presentation">
                <p>Vous venez d'effectuer un test pour la {{$reservation->offer->car->model}} de {{$reservation->offer->car->brand}} qui appartient à {{$reservation->preteur->first_name}} {{$reservation->preteur->last_name}}.</p>
                <p>Afin de finaliser la transaction, merci de compléter le formulaire ci-dessous.</p>
            </div>
            <form action="/merci" class="col-xs-12 col-sm-8 col-sm-offset-2" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="reservation_id" value="{{$reservation->id}}">
                <h2 data-icon="-" class="bandeau-titre-noir">Notation et avis</h2>
                <div class="content-formulaire">

                    <p>Selectionner une note sur 5 pour chaque catégorie et donner votre avis sur ce test.</p>

                    <p class="message-erreur">Vous devez selectionner une note pour chaque catégorie et donner votre avis.</p>

                    <div class="survey-builder">
                        <label>Explications :</label>
                        <input type="hidden" name="explications" value="{{old('explications')}}" class="rating">
                    </div>

                    <div class="survey-builder">
                        <label>Accueil :</label>
                        <input type="hidden" name="accueil" value="4" class="rating">
                    </div>

                    <div class="survey-builder">
                        <label>Durée du test :</label>
                        <input type="hidden" name="duree" class="rating">
                    </div>

                    <div class="survey-builder">
                        <label>Ambiance :</label>
                        <input type="hidden" name="ambiance" class="rating">
                    </div>

                    <label>Donnez votre avis sur ce test</label>
                    <textarea name="commentaire"></textarea>

                    <div class="submit">
                        <input type="submit" class="btn-link-ltc">
                    </div>

                </div>
            </form>
        </div>
    </div>

</section><!-- fin div id="content" -->
@stop
