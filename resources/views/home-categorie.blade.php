@extends('template')

@section('title')
   Live Test Car
@stop

@section('header')
    <script src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="/js/jquery.geocomplete.js"></script>
@stop

@section('body')
    <!-- ca marche pas ! -->
    <div
            class="fb-like"
            data-share="true"
            data-width="450"
            data-show-faces="true">
    </div>
<!--============================== content =================================-->
<section class="content">
    <!--============================== Bandeau img =================================-->
    <div class="container-fluid bandeau-img-index"style="background-image: url('../img/image-{{$univers->identifiant}}.jpg');">
        <div id="carousel-bandeau" class="carousel slide block-flou row" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                  <img src="{{$univers->background}}" alt="...">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 contenu-bandeau-haut">
                <div class="col-xs-12 col-sm-4 col-sm-offset-1 col-md-4 col-md-offset-1 container-univers">
                    <div class="col-xs-12 content-univers">
                        <h2>{{$univers->titre}}</h2>
                        {!!$univers->description!!}
                        @foreach($offers as $offer)
                          <p class="suggestion"><span class="vehicule"><span class="brand">{{$offer->brand}}</span><span class="model">{{$offer->model}}</span></span><span class="prix"><span class="price">{{$offer->price}}&euro;</span></span></p>
                        @endforeach
                    </div>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-5 col-md-offset-1 container-form-search">
                    {!! Form::open(array('route' => 'results', 'class' => 'form-search-index', 'id' => 'form')) !!}
                        <h1>Tester une voiture près de chez vous !</h1>
                        <a href="{{ route('offer.new') }}" class="btn-proposer-voiture">Proposer ma voiture<span data-icon="V"></span><span class="effet"></span></a>
                        <label id="localisation-label" for="search-localisation">Localisation (dans un rayon de)</label>
                        @if ($errors->has('localisation'))
                            <p id="localisation-error" class="message-erreur">
                                    <span class="txt-underline">{{ $errors->first('localisation') }}</span><br/>
                            </p>
                        @endif
                         @if ($errors->has('lat'))
                            <p id="localisation-error" class="message-erreur">
                                    <span class="txt-underline">{{ $errors->first('lat') }}</span><br/>
                            </p>
                        @endif
                         @if ($errors->has('lng'))
                            <p id="localisation-error" class="message-erreur">
                                    <span class="txt-underline">{{ $errors->first('lng') }}</span><br/>
                            </p>
                        @endif

                        <div class="block-input-select">
                            <input id="places" type="text" name="localisation" class="input-ltc" placeholder="Ex: Rennes" value="{{ old('localisation') }}"><select id="rayon" class="select-ltc" name="rayon">
                                <option value="30" selected>30 km</option>
                                <option value="60">60 km</option>
                                <option value="90">90 km</option>
                            </select>
                        </div>

                        <label for="brand">Marque</label>
                        @if ($errors->has('brand'))
                            <p class="message-erreur">
                                <span class="txt-underline">{{ $errors->first('brand') }}</span><br/>
                            </p>
                        @endif
                        <div class="container-brand">
                            <input id="brand" class="input-ltc" type="text" name="brand" placeholder="Ex: Mini" value="{{ old('brand') }}">
                            <!-- Logo de la marque choisie a rajouter en variable dans la balise image
                            <span class="logo-brand">
                                <img src="">
                            </span>-->
                        </div>

                        <label for="model">Modèle</label>
                        @if ($errors->has('model'))
                            <p class="message-erreur">
                                <span class="txt-underline">{{ $errors->first('model') }}</span><br/>
                            </p>
                        @endif
                        <input id="model" type="text" name="model" class="input-ltc" placeholder="Ex: Cooper S" value="{{ old('model') }}">

                        <input id="lat" name="lat" type="hidden" value="" autocomplete="off">
                        <input id="lng" name="lng" type="hidden" value="" autocomplete="off">

                        <input type="submit" value="ok" class="btn-ltc">
                    {!! Form::close() !!}
                </div>
            </div>
            @include('list-univers')
        </div>
    </div>

    <!--============================== Video =================================-->

    <div class="container-fluid video-index gabarit">
        <article class="row">
            <div class="col-xs-12 col-md-10 col-md-offset-1">
                <div class="row container-video">
                    <div class="col-xs-12 texte texte-categorie-univers">
                        <div class="va-container va-container-univers">
                            <div class="va-middle">
                                @if (empty($univers->visuelBas))
                                    <iframe src="https://player.vimeo.com/video/112257960" width="300" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                @else
                                    <img src="{{$univers->visuelBas}}"/>
                                @endif
                                {!! $univers->texteBas !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>


    <!--============================== Slider Avis =================================-->

    <div class="slider-avis-accueil">
        <div id="carousel-avis" class="carousel slide" data-ride="carouse3">
            <div class="row carousel-inner" role="listbox">
                <div class="item active">
                    <div class="content-avis col-xs-10 col-xs-offset-1">
                        <div class="avis-texte">
                            <div class="titre-notation">
                                <h4>Avis laissé à {{$recommandations[0]->offer->preteur->name}}</h4>
                                <div class="notation">
                                    <span class="@if ($recommandations[0]->note()>=1) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[0]->note()>=2) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[0]->note()>=3) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[0]->note()>=4) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[0]->note()>=5) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="txt-notation">- note {{(int)$recommandations[1]->note()}}/5</span>
                                </div>
                            </div>
                            <p>"{{$recommandations[0]->commentaire}}"</p>
                            <div class="auteur-image">
                                <figure class="avis-img">
                                    <img src="{{$recommandations[0]->user->picture or '/img/no_img_profil.png'}}">
                                </figure>
                                <p><h3>{{$recommandations[0]->user->name}}</h3></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="content-avis col-xs-10 col-xs-offset-1">
                        <div class="avis-texte">
                            <div class="titre-notation">
                                <h4>Avis laissé à {{$recommandations[1]->offer->preteur->name}}</h4>
                                <div class="notation">
                                    <span class="@if ($recommandations[1]->note()>=1) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[1]->note()>=2) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[1]->note()>=3) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[1]->note()>=4) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[1]->note()>=5) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="txt-notation">- note {{(int)$recommandations[1]->note()}}/5</span>
                                </div>
                            </div>
                            <p>{{$recommandations[1]->commentaire}}</p>
                            <div class="auteur-image">
                                <figure class="avis-img">
                                    <img src="{{$recommandations[1]->user->picture or '/img/no_img_profil.png'}}">
                                </figure>
                                <p><h3>{{$recommandations[1]->user->name}}</h3></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-avis" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" data-icon="R" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-avis" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" data-icon="O" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <!--============================== Articles =================================-->

    <div class="container-fluid container-article-index gabarit">
        <div class="row">
            <article class="col-xs-12 col-sm-4 article-index">
                <h3 data-icon="9">Le prêt du volant ?</h3>
                <p>Livetestcar c’est une nouvelle façon de tester un véhicule près de chez vous ! Conduisez avant d’acheter grâce aux précieux conseils d’un propriétaire du modèle que vous souhaitez tester&nbsp;!</p>
                <a href="#" class="link-ltc">En savoir +</a>
            </article>
            <article class="col-xs-12 col-sm-4 article-index">
                <h3 data-icon="U">Je veux tester une voiture</h3>
                <p>Livetestcar vous permet de tester tous les modèles de voiture mis à disposition de la communauté et à proximité de chez vous. Rien de plus simple, il vous suffit de rechercher le modèle que vous souhaitez, de vous inscrire de réserver le véhicule et échanger avec les membres Live Test Car. Tester un modèle de voiture avec les conseils avisés d’un propriétaire n’aura jamais été aussi simple !</p>
                <a href="http://livetestcar.com/guide/6" class="link-ltc">En savoir +</a>
            </article>
            <article class="col-xs-12 col-sm-4 article-index">
                <h3 data-icon="W">Je veux proposer ma voiture</h3>
                <p>Livetestcar vous offre l’opportunité de faire tester votre véhicule tout en gagnant de l’argent ! Pour cela, devenez membre de la communauté Live Test Car, remplissez votre profil et les caractéristiques de votre véhicule. Après réservation, vous serez directement mis en relation avec un autre membre souhaitant tester votre véhicule ! Une manière simple de gagner un peu d’argent en donnant de précieux conseils automobiles !</p>
                <a href="http://livetestcar.com/guide/5" class="link-ltc">En savoir +</a>
            </article>
        </div>
    </div>

</section><!-- fin div id="content" -->
@stop

@section('script')
    <!--<script type="text/javascript" src="/js/jssor.slider-20.mini.js"></script>
    <script>
        jQuery(document).ready(function ($) {

            var jssor_1_options = {
              $AutoPlay: false,
              $SlideDuration: 1000,
              $SlideEasing: $Jease$.$OutQuint,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
    </script>-->


    <script>
        $( document ).ready(function() {
            var $places = $("#places");

            // geocomplete plugin qui va remplir les input lat et lng en fonction du lieu selectionné
            $places.geocomplete({
                details: "form",
                types: ["geocode", "establishment"]
            });


            var $brand = $("#brand");
            var $model = $("#model");

            // constructs the suggestion engine
            var brands = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote:
                {
                    url: "search/autocomplete/brand",
                    prepare: function (query, settings) {
                        settings.type = "POST";
                        settings.contentType = "application/json; charset=UTF-8";
                        settings.data = JSON.stringify({"brand" : query});

                        return settings;
                    }
                }
            });
            var models = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote:
                {
                    url: "search/autocomplete/model",
                    prepare: function (query, settings) {
                        settings.type = "POST";
                        settings.contentType = "application/json; charset=UTF-8";
                        settings.data = JSON.stringify({"model" : query, "brand":$brand.val()});

                        return settings;
                    }
                }
            });

            $brand.typeahead({
                        autoselect: true,
                        hint: true,
                        highlight: false,
                        minLength: "1",
                        classNames: {
                            input: 'tt-input',
                            hint: 'tt-hint',
                            menu: 'tt-menu',
                            suggestion: 'tt-suggestion',
                            cursor: 'tt-cursor'
                        }
                    },
                    {
                        name: 'brands',
                        source: brands,
                        limit: 10
                    }
            )
                    .bind('typeahead:select', function(ev, suggestion) {
                        $model.val('');
                    });

            $model.typeahead({
                        autoselect: true,
                        hint: true,
                        highlight: false,
                        minLength: "1",
                        classNames: {
                            input: 'tt-input',
                            hint: 'tt-hint',
                            menu: 'tt-menu',
                            suggestion: 'tt-suggestion',
                            cursor: 'tt-cursor'
                        }
                    },
                    {
                        name: 'models',
                        source: models,
                        limit: 20
                    }
            );
            $('.twitter-typeahead').css('width', '80%');


            $('#brand, #model').on('typeahead:asyncreceive', function(e)
            {
                var firstElem = $(this).data().ttTypeahead.menu.getTopSelectable();
                $(this).data().ttTypeahead.menu.setCursor(firstElem);
            });

            $model.on('typeahead:select', function(e)
            {
                $.ajax({
                    method: "POST",
                    url: "search/autocomplete/model-brand",
                    data: JSON.stringify({ 'model': $model.val()}),
                    dataType: "json",
                    contentType: "application/json; charset=UTF-8"
                })
                    .done(function( data ) {
                        $brand.val(data.brand);
                    });
            });

            $('#form').submit(function()
            {
                if($('#lat').val() !== '' && $('#lng').val() !== '')
                {
                  return true;

                }
                else
                {
                  $('#localisation-label').after("<p class='message-erreur'><span class='txt-underline'>Veuillez selectionner un lieu dans les suggestions proposées</span><br/></p>");
                  return false;
                }
            });
        });
    </script>
@stop