@extends('template')

@section('title')
    Live Test Car - Administration
@stop

@section('header')
@stop

@section('body')
        <!--============================== content =================================-->
    <section class="content gabarit">
        <!--============================== page administration guide =================================-->

        <div class="container-fluid page-administration-guide">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <h1 class="bandeau-titre-noir">Administration</h1>
                    <div class="container-list-guide">
                        <a href="guides">Les guides</a>
                        <p></p>
                        <a href="univers">Les univers</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop