@extends('template')

@section('title')
    Live Test Car
@stop

@section('header')
    <script src="//cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
    <script src="/themes/ckeditor/js/libs/modernizr-2.7.1.min.js"></script>
@stop

@section('body')
    <!--============================== content =================================-->
    <section class="content gabarit">
        <!--============================== page edition univers =================================-->

        <div class="container-fluid page-edition-guide">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <h1 class="bandeau-titre-noir">Univers</h1>
                    <div class="container-edition-guide">
                        @if(session()->has('status'))
                            <p class="message-success">
                                {{ session()->get('status') }}
                            </p>
                        @endif
                        @if (count($errors) > 0)
                            <p class="message-erreur">
                                @foreach ($errors->all() as $error)
                                    <span class="txt-underline">{{ $error }}</span><br/>
                                @endforeach
                            </p>
                        @endif
                        <form id="form" method="POST" action="/admin/updateUnivers" enctype="multipart/form-data" accept-charset="UTF-8" class="form-edition-guide">
                            {!! Form::token() !!}
                            <input type="hidden" name="idUnivers" value="@if (isset($univers)){{$univers->id}}@endif">
                            <!-- ================= Titre ================= -->
                            <div class="row">
                            {!! Form::label('titre', 'Titre: ', array('class' => 'col-xs-12 col-sm-4')) !!}
                                <div class="col-xs-12 col-sm-8">
                            {!! Form::text('titre', $univers->titre, array('class'=>'input-ltc', 'placeholder'=>'Titre univers')) !!}
                                </div>
                            </div>
                            <!-- ================= url page =============== -->
                            <div class="row">
                                {!! Form::label('identifiant', 'Identifiant:', array('class'=>'col-xs-12 col-sm-4')) !!}
                                <div class="col-xs-12 col-sm-8">
                                    {!! Form::text('identifiant', $univers->identifiant, array('class'=>'input-ltc', 'placeholder'=>'Identifiant (url page)')) !!}
                                </div>
                            </div>
                            @if ($univers->background)
                                <img src="{{$univers->background}}" width="100" height="70" />
                            @endif
                            <!-- ================ Visuel de l'univers ============== -->
                            <div class="row">
                                {!! Form::label('visuel', 'Visuel associé: ', array('class' => 'col-xs-12 col-sm-4')) !!}
                                <div class="col-xs-12 col-sm-8">
                                {!! Form::file('visuel', array('class'=>'', 'placeholder'=>'Choisissez un fichier')) !!}
                                </div>
                            </div>
                                <!-- ================ Description ============== -->
                                <div class="row">
                                    {!! Form::label('description', 'Description:', array('class'=>'col-xs-12 col-sm-4')) !!}
                                    <div class="col-xs-12 col-sm-8">
                                        {!! Form::textarea('description', $univers->description, array("width"=>5, "height"=>7, "class"=>"input-ltc", 'placeholder'=>"Description de l'univers")) !!}
                                    </div>
                                </div>
                                <script>
                                    CKEDITOR.replace( 'description' );
                                </script>
                                <!-- ================ Description bas ============== -->
                                <div class="row">
                                    {!! Form::label('texteBas', 'Description du bas:', array('class'=>'col-xs-12 col-sm-4')) !!}
                                    <div class="col-xs-12 col-sm-8">
                                        {!! Form::textarea('texteBas', $univers->texteBas, array("width"=>5, "height"=>7, "class"=>"input-ltc", 'placeholder'=>"Description de l'univers")) !!}
                                    </div>
                                </div>
                                <script>
                                    CKEDITOR.replace( 'texteBas' );
                                </script>
                                <!-- ================ Visuel du bas ============== -->
                                <div class="row">
                                    {!! Form::label('visuelBas', 'Image du bas: ', array('class' => 'col-xs-12 col-sm-4')) !!}
                                    <div class="col-xs-12 col-sm-8">
                                        {!! Form::file('visuelBas', array('class'=>'', 'placeholder'=>'Choisissez un fichier')) !!}
                                    </div>
                                </div>
                                @if ($univers->visuelBas)
                                    <img src="{{$univers->visuelBas}}" width="100" height="70" />
                                @endif
                            <div class="container-btn-save">
                                {!! Form::submit('Enregistrer', array('class'=>'btn-ltc')) !!}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop