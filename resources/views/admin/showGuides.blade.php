@extends('template')

@section('title')
Live Test Car
@stop

@section('header')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
@stop

@section('body')
    <!--============================== content =================================-->
   <section class="content gabarit">
        <!--============================== page administration guide =================================-->

        <div class="container-fluid page-administration-guide">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <h1 class="bandeau-titre-noir">Liste des guides</h1>
                    <div class="container-list-guide">
                        <ul class="forfait forfait-classique">
                    @foreach($guides as $guide)
                            <li>
                                <a href="{{url("admin/guide", $guide->id)}}" class="link-ltc">{{$guide->titre}}</a>
                            </li>
                    @endforeach
                        </ul>
                        <div class="container-btn-ajout">
                            <a href="/admin/guide/new" class="btn-link-ltc">Ajouter un guide</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop