@extends('template')

@section('title')
   Proposer ma voiture - Live Test Car
@stop

@section('body')

<!--============================== content =================================-->
<section class="content gabarit">
       <!--============================== création annonce succès =================================-->
       <div class="container-fluid creation-compte-success">
           <div class="row">
               <div class="col-sm-3"></div>
               <div class="col-xs-12 col-sm-6">
                   <h1 data-icon="K" class="bandeau-titre-noir">Créer un guide</h1>
                   <div class="content-creation-success">
                       <h2 class="message-success">Votre guide a été enregistré avec succès</h2>
                       <p><a href="/admin/guides" class="link-ltc">Administration des guides</a></p>
                   </div>
               </div>
               <div class="col-sm-3"></div>
           </div>
       </div>
</section><!-- fin div id="content" -->
@stop