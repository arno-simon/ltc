@extends('template')

@section('title')
    Live Test Car
@stop

@section('header')
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="/js/jquery.geocomplete.js"></script>
@stop

@section('body')
    <table>
    @foreach($cars as $car)
        <tr><td>{{$car->model}}</td><td>{{$car->year}}</td></tr>
    @endforeach
    </table>
@stop