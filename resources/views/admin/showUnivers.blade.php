@extends('template')

@section('title')
Live Test Car
@stop

@section('header')
@stop

@section('body')
    <!--============================== content =================================-->
   <section class="content gabarit">
        <!--============================== page administration guide =================================-->

        <div class="container-fluid page-administration-guide">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <h1 class="bandeau-titre-noir">Liste des univers</h1>
                    <div class="container-list-guide">
                        <ul class="forfait forfait-classique">
                    @foreach($universes as $univers)
                            <li>
                                <a href="{{url("admin/univers", $univers)}}" class="link-ltc">{{$univers}}</a>
                            </li>
                    @endforeach
                        </ul>
                        <div class="container-btn-ajout">
                            <a href="/admin/univers/new" class="btn-link-ltc">Ajouter un univers</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop