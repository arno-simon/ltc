@extends('template')

@section('title')
    Live Test Car
@stop

@section('header')
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>

    <script src="//cdn.ckeditor.com/4.5.4/basic/ckeditor.js"></script>
    <script src="/themes/ckeditor/js/libs/modernizr-2.7.1.min.js"></script>
@stop

@section('body')
    <!--============================== content =================================-->
    <section class="content gabarit">

        <!--============================== page edition guide =================================-->

        <div class="container-fluid page-edition-guide">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <h1 class="bandeau-titre-noir">Guide</h1>
                    <div class="container-edition-guide">
                        @if(session()->has('status'))
                            <p class="message-success">
                                {{ session()->get('status') }}
                            </p>
                        @endif
                        @if (count($errors) > 0)
                            <p class="message-erreur">
                                @foreach ($errors->all() as $error)
                                    <span class="txt-underline">{{ $error }}</span><br/>
                                @endforeach
                            </p>
                        @endif
                        <form id="form" method="POST" action="/admin/showGuide" enctype="multipart/form-data" accept-charset="UTF-8" class="form-edition-guide">
                            {!! Form::token() !!}
                            <input type="hidden" name="idGuide" value="@if (isset($guide)){{$guide->id}}@endif">
                            <!-- ================= Titre ================= -->
                            <div class="row">
                            {!! Form::label('titre', 'Titre: ', array('class' => 'col-xs-12 col-sm-4')) !!}
                                <div class="col-xs-12 col-sm-8">
                            {!! Form::text('titre', $guide->titre, array('class'=>'input-ltc', 'placeholder'=>'Titre guide')) !!}
                                </div>
                            </div>
                            <!-- ================= Auteur =============== -->
                            <div class="row">
                                {!! Form::label('auteur', 'Ecrit par:', array('class'=>'col-xs-12 col-sm-4')) !!}
                                <div class="col-xs-12 col-sm-8">
                                    {!! Form::text('auteur', $guide->auteur, array('class'=>'input-ltc', 'placeholder'=>'Auteur')) !!}
                                </div>
                            </div>
                            <!--     ================ Visuel du guide ==============-->
                            <div class="row">
                                {!! Form::label('visuel', 'Visuel associé: ', array('class' => 'col-xs-12 col-sm-4')) !!}
                                <div class="col-xs-12 col-sm-8">
                                {!! Form::file('visuel', array('class'=>'', 'placeholder'=>'Choissiez un fichier')) !!}
                                </div>
                            </div>
                            <div class="row">
                                {!! Form::label('contenu', 'Contenu:', array('class'=>'col-xs-12 col-sm-4')) !!}
                                <div class="col-xs-12 col-sm-8">
                                    {!! Form::textarea('contenu', $guide->contenu, array("width"=>5, "height"=>7, "class"=>"input-ltc", 'placeholder'=>'Contenu')) !!}
                                </div>
                            </div>
                            <script>
                                CKEDITOR.replace( 'contenu' );
                            </script>
                            <div class="container-annonce row form-modification-info">
                                {!! Form::label('url', 'URL: ', array('class'=>'col-xs-12 col-sm-4')) !!}
                                <div  class="col-xs-12 col-sm-8">
                                    {!! Form::text('url', $guide->url, array('class'=>'input-ltc', 'placeholder'=>'mon-url-personnalisee')) !!}
                                </div>
                            </div>
                            <div class="row">
                                {!! Form::label('actif', 'Guide visible:', array('class'=>'col-xs-12 col-sm-4')) !!}
                                <div class="col-xs-12 col-sm-8">
                                    {!! Form::checkbox('actif', 1, $guide->actif, ['class' => 'field']) !!}
                                </div>
                            </div>
                            <div class="container-btn-save">
                                {!! Form::submit('Enregistrer', array('class'=>'btn-ltc')) !!}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop