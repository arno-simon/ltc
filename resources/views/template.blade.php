<!doctype html>

<!--[if IE 8]>         <html class="no-js ie8" lang="fr">    <![endif]-->
<!--[if IE 9]>         <html class="no-js ie9" lang="fr">    <![endif]-->
<!--[if IE 10]>        <html class="no-js ie10" lang="fr">   <![endif]-->
<!--[if gt IE 8]><!--> <html lang="fr">    <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/livetestcar-theme.css" rel="stylesheet">
    <link href="/css/font-livetestcar.css" rel="stylesheet">
    <link href="/css/autocomplete.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">
    <!-- crop -->
    <link rel="stylesheet" href="/crop/cropper.css">
    <!--<link rel="stylesheet" href="/crop/main.css">-->

    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
    <meta name="robots" content="noindex,nofollow" />

    <!--[if lte IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="/js/header.js"></script>
    <script src="/js/layout.js"></script>
    <!--<script src="/js/accordeon.js"></script>-->
    <script src="/js/canvas-to-blob.min.js"></script>
    <script src="/js/fileinput.min.js"></script>
    <script src="/js/ekko-lightbox.js"></script>

    @yield('header')

</head>
<body class="{{ Agent::browser() }}">

@include('header')

@yield('body')

@include('footer')

@yield('script')

</body>
</html>
