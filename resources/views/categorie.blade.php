@extends('template')

@section('title')
    Live Test Car
@stop
@section('header')
    <script src="/js/conversation-event.js"></script>
@stop

    @section('body')
            <!--============================== content =================================-->
    <section class="content gabarit">
        <!--============================= page forfait et tarif ====================-->
        <div>
            il y a {{count($cars)}} voitures dans la catégorie {{$categorie}}
        </div>
        <div>
            Il y a {{count($offers)}} offres dans la catégorie {{$categorie}}
        </div>
        @foreach($offers as $offer)
            <p>{{$offer->model}}</p>
        @endforeach
        <h2>Quelques offres de {{$categorie}}</h2>
            <!--========= lien pour ecran large ========-->
            <div class="row container-link-ltc">
                <div class="col-sm-4 col-sm-offset-2 pr-0 filtre-modele">
                    <a class="link-ltc">Trier par modèle</a><a href="#" data-icon="N" class="sort link-ltc fleche-tri" data-sort="model:asc" onclick="return false"></a><a href="#" data-icon="P" class="sort link-ltc fleche-tri" data-sort="model:desc" onclick="return false"></a>
                </div>
                <div class="col-sm-4 pl-0 pr-0 filtre-localisation">
                    <a class="link-ltc">Trier par localisation</a><a href="#" data-icon="N" class="sort link-ltc fleche-tri" data-sort="location:asc" onclick="return false"></a><a href="#" data-icon="P" class="sort link-ltc fleche-tri" data-sort="location:desc" onclick="return false"></a>
                </div>
                <div class="col-sm-2 pl-0 filtre-prix">
                    <a class="link-ltc">Trier par prix</a><a href="#" data-icon="N" class="sort link-ltc fleche-tri" data-sort="price:asc" onclick="return false"></a><a href="#" data-icon="P" class="sort link-ltc fleche-tri" data-sort="price:desc" onclick="return false"></a>
                </div>
            </div>

            <div id="Container">
                <!--========= liste des résultats ========-->
                @for($i=0; $i<count($offers); $i++)
                    <div id="{{$offers[$i]->id}}" href="" class="mix row container-liste-resultat" data-model="{{ $offers[$i]->brand." ".$offers[$i]->model }}" data-location="{{ $offers[$i]->location_name }}" data-price="{{ $offers[$i]->price }}">
                        <div class="col-xs-12 col-sm-2 photo-liste-resultat">
                            <a href="" class="link-ltc">
                                <figure>
                                    <img src="/img/photo-profil-vide.png">
                                </figure>
                            </a>
                        </div>
                        <div class="col-xs-12 col-sm-8 descriptif-liste-resultat">
                            <div class="row">
                                <a href="{{ url('user/profil', [$offers[$i]->id]) }}" class="col-xs-12 col-sm-6 model">{{ $offers[$i]->brand }} <span class="bold">{{ $offers[$i]->model }}</span></a>
                                <p class="col-xs-12 col-sm-6 location"><span class="bold">Localisation :</span> {{ $offers[$i]->location_name }}</p>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <p class="text-descriptif">« {{ $offers[$i]->description }} »</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 prix-liste-resultat">
                            <a href="{{  url('reservation', [$offers[$i]->id]) }}" class="link-ltc">
                                <div class="text-prix">
                                    <p class="link-prix"><span>Réservez à</span><span class="prix">{{ $offers[$i]->price }} €</span><span>l'essai</span></p>
                                </div>
                            </a>
                        </div>
                    </div>
                @endfor
                <div class="modal" id="modal-gallery" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button class="close" type="button" data-dismiss="modal">×</button>
                            </div>
                            <div class="modal-body">
                                <div id="modal-carousel" class="carousel">
                                    <div class="carousel-inner">
                                    </div>
                                    <a class="carousel-control left" href="#modal-carousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                    <a class="carousel-control right" href="#modal-carousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section><!-- fin div id="content" -->
@stop
