@extends('template')

@section('title')
    Live Test Car
@stop

@section('header')
    <link href="/css/datepicker/jquery.datepick.css" rel="stylesheet">
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="/js/jquery.geocomplete.js"></script>
    <script src="/js/datepicker/jquery.plugin.js"></script>
    <script src="/js/datepicker/jquery.datepick.js"></script>
    <script src="/js/datepicker/jquery.datepick-fr.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@stop

@section('body')
        <!--============================== content =================================-->

    <section class="content gabarit">

        <!--============================= page réservation ==================================-->

        <div class="container-fluid page-reservation">

            <h1>Réservation</h1>

            <div class="row">

                <!--============================= procédure ==================================-->
                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                    <article class="container-procedure">
                        <h2>Procédure de commande</h2>
                        <ul>
                            <li><span>1 -</span> <strong>vérifiez</strong> que la réservation correspond à votre choix</li>
                            <li><span>2 -</span> Sélectionnez <strong>une date et un horaire</strong> parmis les choix proposés</li>
                            <li><span>3 -</span> choisissez votre <strong>type de réglement</strong> : carte de crédit, paypal ou crédit du site</li>
                            <li><span>4 -</span> Une fois votre commande confirmé vous recevrez un <strong>e-mail de confirmation</strong> avec le numéro de téléphone du préteur</li>
                            <li><span>5 -</span> 24 heures avant le rendez vous vous recevrez un <strong>e-mail de rappel</strong></li>
                        </ul>
                    </article>
                </div>

                <!--============================= récapitulatif réservation ==================================-->
                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                    <article class="container-recapitulatif">
                        <h2>Récapitulatif de votre réservation</h2>
                        <!-- Nom du preteur  -->
                        <div class="row information">
                            <div class="col-xs-12 col-sm-4 libelle">
                                <p>Nom du préteur :</p>
                            </div>
                            <div class="col-xs-12 col-sm-8 valeur">
                                <p>{{$offer->preteur->first_name}} {{$offer->preteur->last_name}}</p>
                            </div>
                        </div>
                        <!-- adresse du véhicule  -->
                        <div class="row information">
                            <div class="col-xs-12 col-sm-4 libelle">
                                <p>Adresse ou se trouve le véhicule :</p>
                            </div>
                            <div class="col-xs-12 col-sm-8 valeur">
                                <p>{{$offer->location_name}}</p>
                            </div>
                        </div>
                        <!-- véhicule  -->
                        <div class="row information">
                            <div class="col-xs-12 col-sm-4 libelle">
                                <p>Véhicule choisi :</p>
                            </div>
                            <div class="col-xs-12 col-sm-8 valeur">
                                <p>{{ $offer->car->model }}</p>
                            </div>
                        </div>
                        <!-- Carburant  -->
                        <div class="row information">
                            <div class="col-xs-12 col-sm-4 libelle">
                                <p>Carburant du véhicule :</p>
                            </div>
                            <div class="col-xs-12 col-sm-8 valeur">
                                <p>Essence</p>
                            </div>
                        </div>
                        <!-- boite de vitesse  -->
                        <div class="row information">
                            <div class="col-xs-12 col-sm-4 libelle">
                                <p>Boite de vitesse :</p>
                            </div>
                            <div class="col-xs-12 col-sm-8 valeur">
                                <p>Manuelle</p>
                            </div>
                        </div>
                        <!-- photo  -->
                        <div class="row information">
                            <div class="col-xs-12 col-sm-4 libelle">
                                <p>photo du véhicule :</p>
                            </div>
                            <div class="col-xs-12 col-sm-8 valeur">
                                <div class="row">
                                    @foreach($offer->pictures as $picture)
                                        <figure class="col-xs-4 col-lg-3">
                                          <img src="{{$picture->visuel}}">
                                        </figure>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!-- options  -->
                        <div class="row information">
                            <div class="col-xs-12 col-sm-4 libelle">
                                <p>Options du véhicule :</p>
                            </div>
                            <div class="col-xs-12 col-sm-8 valeur">
                                <ul>
                                    @foreach ($offer->options as $option)
                                        <li> {{$option->name}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- prix  -->
                        <div class="row information information-prix">
                            <div class="col-xs-12 col-sm-4 libelle">
                                <p>Prix de l'éssai :</p>
                            </div>
                            <div class="col-xs-12 col-sm-8 valeur">
                                <p>{{ $offer->price }} &euro;</p>
                            </div>
                        </div>
                        <!-- selection de la date  -->
                        {!! Form::open(array('url' => '/paiement', 'class' => 'information', 'id' => 'form')) !!}
                            <input type="hidden" name="reservation_id" value="{{$reservation_id}}">
                            <div class="row selection-date">
                                <div class="col-xs-12 col-sm-4 libelle">
                                    <label for="date-essai">Date souhaitée pour l'essai :</label>
                                </div>
                                <div class="col-xs-12 col-sm-5 valeur">
                                    <!--<input type="date" name="date-essai" id="date-essai" class="input-ltc">-->
                                    <div id="selectDate" name="selectDate"></div>
                                    <input type="hidden" id="dateSelected" name="dateSelected">
                                </div>
                            </div>
                            <div class="row selection-horaire">
                                <div class="col-xs-12 col-sm-4 libelle">
                                    <label for="horaire-essai">Horaire souhaité pour l'essai :</label>
                                </div>
                                <div class="col-xs-12 col-sm-8 valeur">
                                    <select class="select-ltc" name="heure">
                                        <option value="horaire-1" selected>6h</option>
                                        <option value="horaire-2">7h</option>
                                        <option value="horaire-3">8h</option>
                                        <option value="horaire-4">9h</option>
                                    </select>
                                </div>
                            </div>

                            <!-- valider paiement  -->
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                            <article class="container-paiement">
                                <h2>Paiement</h2>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="container-checkbox">
                                            <label for="validation-paiement"><input type="checkbox" id="validation-paiement" name="validation-paiement">J'accepte les conditions de réglement</label>
                                        </div>
                                        <div class="container-autre-paiement">
                                            <div class="container-type-paiement paiement-cb">
                                                <a href="">
                                                    <img src="/img/logo-cb.png">
                                                </a>
                                            </div>
                                            <div class="container-type-paiement">
                                                <a href="">
                                                    <img src="/img/logo-paypal.png">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                            <div class="row demande-paiement">
                                <div class="col-xs-12 btn-paiement">
                                    <button type="submit" class="btn-link-ltc" value="paiement">Payer Maintenant</button>
                                </div>
                            </div>
                        {!! Form::close() !!}

                    </article>
                </div>

                <!--============================= procédure ==================================-->
            </div>
        </div>

    </section><!-- fin div id="content" -->
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function showDate(date)
        {
            $('#selectDate').val('').datepick('option',
                    {dateFormat: $.datepick.ISO_8601});
            var dates = $('#selectDate').datepick('getDate');
            var value = '';
            for (var i = 0; i < dates.length; i++) {
                value += (i == 0 ? '' : ',') + $.datepick.formatDate(dates[i]);
            }
            $('#dateSelected').val(value || 'none');

            $.ajax({
                url: '/horaires',
                type: 'POST',
                data: {date: dates,
                offer_id: <?php echo $offer->id ?>},
                dataType: 'JSON',
                success: function (data) {
                    $("select.select-ltc").children().remove();
                    for (i=parseInt(data.debut); i<=parseInt(data.fin); i++)
                    {
                        $('<option value="'+i+'">'+i+' h </option>').appendTo("select.select-ltc");
                    }
                }
            });
        }
        $( document ).ready(function() {
            $('#selectDate').datepick({monthsToShow:1, onSelect: showDate, onDate: dateDispo, dateFormat: $.datepick.ISO_8601});
            <?php if (isset($offer)) : ?>
                function dateDispo(date, inMonth)
                {
                    var dispo = [
                            <?php foreach ($offer->disponibilites as $disponibilite) : ?>
                                <?php echo '['.$disponibilite->mois().','.$disponibilite->jour().'],'; ?>
                            <?php endforeach; ?>
                    ];
                    if (inMonth)
                    {
                        for (i=0; i<dispo.length; i++)
                        {
                            if (date.getMonth()+1 == dispo[i][0]
                                    && date.getDate() == dispo[i][1])
                            {
                                return {};
                            }
                        }
                    }
                    return{selectable:false};
                }
                <?php endif; ?>
                var $places = $("#places");
                var $brand = $("#brand");
                var $model = $("#model")
            });
    </script>
@stop