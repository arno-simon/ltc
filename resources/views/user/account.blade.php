@extends('template')

@section('title')
    Mon compte - Live Test Car
@stop

@section('header')
    <script src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="/js/jquery.geocomplete.js"></script>
    @stop

    @section('body')
            <!--============================== content =================================-->
    <section class="content gabarit">

        <!--============================== page resultat =================================-->

        <div class="container-fluid page-creer-profil accordion" id="accordion2">

            <h1>Mon compte</h1>

            <!--============================== informations personnelles =================================-->

            <div class="row accordion-group">
                <div class="col-xs-12 col-sm-7">
                    <div class="container-accordion">
                        <div class="accordion-heading">
                            <h2 data-icon="!" class="bandeau-titre-noir">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne"></a>
                                Informations personnelles
                            </h2>
                        </div>
                        <div class="content-info-perso accordion-body collapse in" id="collapseOne">
                            <div class="accordion-inner">
                                @if (count($errors) > 0)
                                    <p class="message-erreur">
                                        @foreach ($errors->all() as $error)
                                            <span class="txt-underline">{{ $error }}</span><br/>
                                        @endforeach
                                    </p>
                                @endif

                                @if(session()->has('status'))
                                    <p class="message-success">
                                        {{ session()->get('status') }}
                                    </p>
                                @endif
                                <!-- TEST -->
                                <form method="POST" action="crop.php" enctype="multipart/form-data" accept-charset="UTF-8" class="form-modification-info" id="rowForm">
                                    {!! Form::token() !!}
                                    <div class="row container-info">
                                        <div class="info-enregistre" id="rowPicture">
                                            <div class="col-xs-12 col-sm-3">
                                                <p class="libelle">Photo :</p>
                                            </div>
                                            <div class="col-xs-12 col-sm-5">
                                                <figure>
                                                    <img class="photo-view" src="{{ $user->picture or '/img/no_img_profil.png'}}">
                                                </figure>
                                            </div>
                                            <div class="col-xs-12 col-sm-offset-3 col-sm-9">
                                                <div id="crop-avatar">
                                                    <!-- Current avatar -->
                                                    <div class="avatar-view" title="changer ma photo de profil">
                                                        <!--<img src="/img/no_img.png" alt="Avatar">-->
                                                        <span class="btn-ltc">Modifier ma photo</span>
                                                    </div>
                                                    <!-- Cropping modal -->
                                                    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="avatar-form">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal"><span data-icon="6"></span></button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="image-rotate">
                                                                            <img src="/img/rotate.png" alt="image rotation telephone" title="image rotation telephone">
                                                                        </div>
                                                                        <div class="avatar-body">
                                                                            <!-- Upload image and data -->
                                                                            <div class="avatar-upload">
                                                                                <input type="hidden" class="avatar-src" name="avatar_src">
                                                                                <input type="hidden" class="avatar-data" name="avatar_data">
                                                                                <input type="file" class="avatar-input file" id="avatarInput" name="avatar_file" data-show-preview="false">
                                                                            </div>
                                                                            <!-- Crop and preview -->
                                                                            <div class="row">
                                                                                <div class="col-md-9">
                                                                                    <div class="avatar-wrapper"></div>
                                                                                </div>
                                                                                <div class="col-md-3">
                                                                                    <div class="avatar-preview preview-lg"></div>
                                                                                    <div class="avatar-preview preview-md"></div>
                                                                                    <!--<div class="avatar-preview preview-sm"></div>-->
                                                                                </div>
                                                                            </div>
                                                                            <div class="row avatar-btns">
                                                                                <div class="col-xs-12 ta-c">
                                                                                    <button type="button" class="btn btn-primary avatar-rotate" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><span data-icon="b"></span></button>
                                                                                    <button type="submit" class="btn btn-primary btn-block avatar-save">Valider</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- /.modal -->
                                                    <!-- Loading state -->
                                                    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- FIN TEST -->
                                <form method="POST" action="" enctype="multipart/form-data" accept-charset="UTF-8" class="form-modification-info" id="rowForm">
                                {!! Form::token() !!}
                                    <!--============================== Nom profil =================================-->
                                    <div class="container-info row">
                                        <label for="last_name" class="col-xs-12 col-sm-3">Nom :</label>
                                        <div class="col-xs-12 col-sm-5">
                                            <input type="text" name="last_name" class="input-ltc" placeholder="Nom du profil" value="{{ $user->last_name }}">
                                        </div>
                                    </div>

                                    <!--============================== Prénom profil =================================-->
                                    <div class="container-info row">
                                        <label for="first_name" class="col-xs-12 col-sm-3">Prénom :</label>
                                        <div class="col-xs-12 col-sm-5">
                                            <input type="text" name="first_name" class="input-ltc" placeholder="Prénom du profil" value="{{ $user->first_name }}">
                                        </div>
                                    </div>

                                    <!--============================== Année de naissance =================================-->
                                    <div class="container-info row">
                                        <label for="first_name" class="col-xs-12 col-sm-3">Année de naissance :</label>
                                        <div class="col-xs-12 col-sm-5">
                                            <input type="text" name="birthyear" class="input-ltc" placeholder="1992" value="{{ $user->birthyear }}">
                                        </div>
                                    </div>

                                    <!--============================== Mail profil =================================-->
                                    <div class="container-info row">
                                        <label for="email" class="col-xs-12 col-sm-3">Adresse E-mail :</label>
                                        <div class="col-xs-12 col-sm-5">
                                            <input type="email" name="email" class="input-ltc" placeholder="Adresse e-mail" value="{{ $user->email }}">
                                        </div>
                                        <div class="col-xs-12 col-sm-5 col-sm-offset-3 info-verifie" id="rowMailCheck">
                                            @if($user->isMailConfirmed)
                                                <div>
                                                    <p data-icon="Q" class="info-valide">E-mail vérifié</p>
                                                </div>
                                            @else
                                                <div>
                                                    <p data-icon="3" class="info-non-valide">E-mail non vérifié</p>
                                                </div>
                                                <div class="lien-validation">
                                                    <a href="{{ route('account.mail.confirm') }}" >Vérifier maintenant</a>
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <!--============================== Mdp profil =================================-->
                                    <div class="container-info">
                                        <div class="row">
                                            <label for="mdp-profil" class="col-xs-12 col-sm-3">Nouveau mot de passe :</label>
                                            <div class="col-xs-12 col-sm-5">
                                                <input type="password" name="password" class="input-ltc" placeholder="******">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="mdp-profil" class="col-xs-12 col-sm-3">Confirmation :</label>
                                            <div class="col-xs-12 col-sm-5">
                                                <input type="password" name="password_confirmation" class="input-ltc" placeholder="******">
                                            </div>
                                        </div>
                                    </div>

                                    <!--============================== Téléphone profil =================================-->
                                    <div class="container-info row">
                                        <label for="phone" class="col-xs-12 col-sm-3">Téléphone :</label>
                                        <div class="col-xs-12 col-sm-5">
                                            <input type="text" name="phone" class="input-ltc" placeholder="Téléphone" value="{{ $user->phone }}">
                                        </div>
                                    </div>

                                    <!--============================== Marques preferées profil =================================-->
                                    <div class="container-info">
                                        <div class="row">
                                            <label for="brand1" class="col-xs-12 col-sm-3">Marques préferées :</label>
                                            <div class="col-xs-12 col-sm-5">
                                                <input id="brand1" class="input-ltc input-brand" type="text" name="brand1" placeholder="Choisissez..." value="{{ $brands[0] or '' }}">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <label for="brand2" class="col-xs-12 col-sm-3">&nbsp;</label>
                                            <div class="col-xs-12 col-sm-5">
                                                <input id="brand2" class="input-ltc input-brand" type="text" name="brand2" placeholder="Choisissez..." value="{{ $brands[1] or '' }}">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <label for="brand3" class="col-xs-12 col-sm-3">&nbsp;</label>
                                            <div class="col-xs-12 col-sm-5">
                                                <input id="brand3" class="input-ltc input-brand" type="text" name="brand3" placeholder="Choisissez..." value="{{ $brands[2] or '' }}">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-5 col-sm-offset-3">
                                                <input type="checkbox" name="isEvent" @if($user->isEvent){{ "checked" }}@endif> Profiter des événements des marques en avant première
                                            </div>
                                        </div>
                                    </div>

                                    <!--============================== Ville profil =================================-->
                                    <div class="container-info row">
                                        <label for="ville" class="col-xs-12 col-sm-3">Ville :</label>
                                        <div class="col-xs-12 col-sm-5">
                                            <input type="text" id="places" name="ville" class="input-ltc" placeholder="Ville" value="{{ $user->ville }}">
                                        </div>
                                    </div>

                                    <!--============================== Photo profil =================================-->
                                    <!--<div class="row container-info">
                                        <div class="info-enregistre @if($user->picture == ''){{ "hidden" }}@endif" id="rowPicture">
                                            <div class="col-xs-12 col-sm-3">
                                                <p class="libelle">Photo :</p>
                                            </div>
                                            <div class="col-xs-12 col-sm-5">
                                                <figure>
                                                    <img src="{{ $user->picture }}">
                                                </figure>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-5 col-sm-offset-3">
                                            <input type="file" name="picture" id="input-id" class="file" data-preview-file-type="text">
                                        </div>
                                    </div>-->

                                    <div class="row container-info container-btn-valider">
                                        <div class="col-xs-12">
                                            <input type="submit" value="Valider" class="btn-ltc pull-right">
                                        </div>
                                    </div>

                                    <!--============================== texte profil =================================-->
                                    <p class="txt-explication">Pour essayer ou proposer un véhicule vous devez préalablement compléter votre profil</p>
                                    <p class="mention-champs-obligatoires">Les champs marqués * sont obligatoires</p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-5">
                    <div class="container-accordion">
                        <div class="accordion-heading">
                            <h2 data-icon="Z" class="bandeau-titre-noir">
                                <a class=" accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo"></a>
                                Informations du profil
                            </h2>
                        </div>
                        <div class="content-info-perso accordion-body collapse" id="collapseTwo">
                            <div class="accordion-inner">
                                <!--============================== Avis sur le profil =================================-->
                                <div class="row container-avis-profil">
                                    <h3 class="col-xs-12">Avis sur le profil :</h3>
                                    <p class="col-xs-12">{{count($user->recommandations)}} commentaire(s)</p>
                                    @if (count($user->recommandations) > 0)
                                        <p class="col-xs-12 col-sm-6">Note moyenne :</p>
                                        <div class="col-xs-12 col-sm-6 notation">
                                            <span class="@if ($user->note() >=1) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                            <span class="@if ($user->note() >=2) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                            <span class="@if ($user->note() >=3) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                            <span class="@if ($user->note() >=4) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                            <span class="@if ($user->note() >=5) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                            <span class="txt-notation">- note {{$user->note()}}/5</span>
                                        </div>
                                        <div class="col-xs-12">
                                            <a href="/user/profil/{{$user->id}}" class="link-ltc">Voir tous les commentaires</a>
                                        </div>
                                    @endif
                                </div>

                                <!--============================== Nombre de crédits disponibles =================================-->
                                <div class="row container-credit">
                                    <h3 class="col-xs-12">Nombre de crédits disponibles</h3>
                                    <p class="col-xs-6 libelle">Crédit utilisables :</p>
                                    <p class="col-xs-6 credit-utilisable">{{\Auth::user()->credits}}</p>
                                    <div class="col-xs-12">
                                        <a href="" class="link-ltc convertir-credit">Convertir mon crédit en argent</a>
                                    </div>
                                    <div class="col-xs-12">
                                        <a href="" class="btn-link-ltc acheter-credit">Obtenir plus de crédit</a>
                                    </div>
                                    <p class="col-xs-12 information-credit"><strong>5 crédits </strong>sont équivalent à <strong>1 euros</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Bloc Mes offres -->
                    <div class="container-accordion">
                        <div class="accordion-heading">
                            <h2 data-icon="W" class="bandeau-titre-noir">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree"></a>
                                Véhicules proposés au test
                            </h2>
                        </div>
                        <div class="content-info-perso accordion-body collapse" id="collapseThree">
                            <div class="accordion-inner">
                                @if (count($user->offers) == 0)
                                    <p>Pas d'Offres</p>
                                    @else
                                    @foreach($user->offers as $offer)
                                            <!--============================== Notation Offre =================================-->
                                    <div class="container-modif-annonce">
                                        <div class="row container-avis-profil">
                                            <h3 class="col-xs-12"> <a href=/user/profil/{{$user->id}}>{{$offer->car->model}}</a> </h3>
                                            <p class="col-xs-6 libelle">Test à partir de :</p>
                                            <p class="col-xs-6 montant-annonce">{{$offer->price}} €</p>
                                            <p class="col-xs-12">{{count($offer->recommandations)}} commentaire(s)</p>
                                            @if (count($offer->recommandations) > 0)
                                                <p class="col-xs-12 col-sm-6">Note :</p>
                                                <div class="col-xs-12 col-sm-6 notation">
                                                    <span class="@if ($offer->note()>=1){{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                                    <span class="@if ($offer->note()>=2){{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                                    <span class="@if ($offer->note()>=3){{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                                    <span class="@if ($offer->note()>=4){{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                                    <span class="@if ($offer->note()>=5){{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                                    <span class="txt-notation">- note {{(int)$offer->note()}}/5</span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <a href="/user/profil/{{$user->id}}" class="link-ltc">Voir tous les commentaires</a>
                                                </div>
                                            @endif
                                            <div class="col-xs-12">
                                                <a href="/offer/edit/{{$offer->id}}" class="btn-link-ltc modification-annonce">Modifier</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- Bloc Parrainage -->
                    <div class="container-accordion container-parrainage">
                        <div class="accordion-heading">
                            <h2 data-icon="," class="bandeau-titre-noir">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour"></a>
                                Obtenez 1 mois gratuit !
                            </h2>
                        </div>
                        <div class="content-info-perso accordion-body collapse" id="collapseFour">
                            <div class="accordion-inner">
                                <div class="row container-avis-profil">
                                    <div class="container-texte col-xs-12">
                                        @if ($user->filleuls->count() < 3)
                                        <p class="text-principal">Ajoutez <strong>{{3 - count($user->filleuls)}} {{str_plural('contact', 3 - count($user->filleuls))}}</strong> pour bénéficier d'un compte <strong>Premium</strong></p>
                                        @endif
                                        <p class="no-filleul-attente">{{trans_choice('parrainages.filleul_attente', $user->nbFilleulsAttentes(), ['nombre'=>$user->nbFilleulsAttentes()])}}</p>
                                        <p class="nb-filleul-inscrit">{{trans_choice('parrainages.filleul_inscrit', $user->nbFilleulsInscrits(), ['nombre'=>$user->nbFilleulsInscrits()])}}</p>
                                    </div>
                                    <ul class="col-xs-12">
                                        <li class="col-xs-4">
                                            <a href="@if ($user->filleuls->has(0) && $user->filleuls[0]->statut == 'inscrit') user/profil/{{$user->filleuls[0]->filleul_id}} @else # @endif" data-toggle="modal" @if ($user->filleuls->has(0) && $user->filleuls[0]->statut == 'envoye') {! data-target=".parrainage-2" !} @elseif (!$user->filleuls->has(0)) {! data-target=".parrainage-1" !}@endif>
                                                @if ($user->filleuls->has(0) && $user->filleuls[0]->statut == 'inscrit')
                                                    <figure>
                                                        <img class="photo-view" src="@if ($user->filleulPicture(0)) {{$user->filleulPicture(0)}} @else {{'/img/no_img_profil.png'}}@endif">
                                                    </figure>
                                                @elseif ($user->filleuls->has(0) && $user->filleuls[0]->statut == 'envoye')
                                                    <span class="msg-send" data-icon=","></span>
                                                @else
                                                    <span class="no-filleul" data-icon=","></span>
                                                @endif
                                            </a>
                                        </li>
                                        <li class="col-xs-4">
                                            <a href="@if ($user->filleuls->has(1)  && $user->filleuls[1]->statut == 'inscrit') user/profil/{{$user->filleuls[1]->filleul_id}} @else  # @endif" data-toggle="modal" @if ($user->filleuls->has(1) && $user->filleuls[1]->statut == 'envoye') {! data-target=".parrainage-2" !} @elseif (!$user->filleuls->has(1)) {! data-target=".parrainage-1" !}@endif>
                                                @if ($user->filleuls->has(1) && $user->filleuls[1]->statut == 'inscrit')
                                                    <figure>
                                                        <img class="photo-view" src="@if ($user->filleulPicture(1)) {{$user->filleulPicture(1)}} @else {{'/img/no_img_profil.png'}}@endif">
                                                    </figure>
                                                @elseif ($user->filleuls->has(1) && $user->filleuls[1]->statut == 'envoye')
                                                    <span class="msg-send" data-icon=","></span>
                                                @else
                                                    <span class="no-filleul" data-icon=","></span>
                                                @endif
                                            </a>
                                        </li>
                                        <li class="col-xs-4">
                                            <a href="@if ($user->filleuls->has(2)  && $user->filleuls[2]->statut == 'inscrit') user/profil/{{$user->filleuls[2]->filleul_id}} @else # @endif" data-toggle="modal" @if ($user->filleuls->has(2) && $user->filleuls[2]->statut == 'envoye') {! data-target=".parrainage-2" !} @elseif (!$user->filleuls->has(2)) {! data-target=".parrainage-1" !}@endif>
                                                @if ($user->filleuls->has(2) && $user->filleuls[2]->statut == 'inscrit')
                                                    <figure>
                                                        <img class="photo-view" src="@if ($user->filleulPicture(2)) {{$user->filleulPicture(2)}} @else {{'/img/no_img_profil.png'}}@endif">
                                                    </figure>
                                                @elseif ($user->filleuls->has(2) && $user->filleuls[2]->statut == 'envoye')
                                                    <span class="msg-send" data-icon=","></span>
                                                @else
                                                    <span class="no-filleul" data-icon=","></span>
                                                @endif
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="container-btn col-xs-12">
                                        <a href="#" class="btn-link-ltc">Ajouter plus de contact</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </section><!-- fin div id="content" -->

    <section class="modal fade modal-page-statique modal-parrainage parrainage-1" tabindex="-1" role="dialog" aria-labelledby="parrainage-1">
      <article class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" data-icon="6"></span></button>
              <h1 class="modal-title" id="myModalLabel">Ajouter un contact</h1>
            </div>
            <div class="modal-body">
              <form method="post" action="parrainage" class="row">
                  {!! Form::token() !!}
                  <div class="col-xs-12">
                    <label class="col-xs-12 col-sm-4 col-md-3">Contact :</label>
                    <input type="text" name="mail" placeholder="adresse mail du contact" class="input-ltc col-xs-12 col-sm-8">
                </div>
                <div class="col-xs-12">
                    <label class="col-xs-12 col-sm-4 col-md-3">Relation :</label>
                    <select placeholder="" name="relation" class="select-ltc col-xs-12 col-sm-8">
                        <option disabled selected hidden>Type de relation</option>
                        <option>Conjoint</option>
                        <option>Amis</option>
                        <option>Collègue</option>
                    </select>
                </div>
                <div class="col-xs-12">
                    <label class="col-xs-12 col-sm-4 col-md-3">Message :</label>
                    <textarea class="col-xs-12 col-sm-8" name="message" placeholder="Bonjour, je vous invite à vous inscrire sur le site livetestcar.com afin de faire tester votre véhicule et venir essayer celui de vos rêves ! Rejoignez-moi sur www.livetestcar.com/register "></textarea>
                </div>
                <div class="container-submit col-xs-12">
                    <input type="submit" value="Envoyer" class="btn-ltc">
                </div>
              </form>
            </div>
        </div>
      </article>
    </section>
    <section class="modal fade modal-page-statique modal-parrainage parrainage-2" tabindex="-1" role="dialog" aria-labelledby="parrainage-1">
        <article class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" data-icon="6"></span></button>
                    <h1 class="modal-title" id="myModalLabel">Renvoyer à un contact</h1>
                </div>
                <div class="modal-body">
                    <form method="post" action="parrainage" class="row">
                        {!! Form::token() !!}
                        <div class="col-xs-12">
                            <label class="col-xs-12 col-sm-4 col-md-3">Contact :</label>
                            <input type="text" name="mail" placeholder="adresse mail du contact" class="input-ltc col-xs-12 col-sm-8">
                        </div>
                        <div class="col-xs-12">
                            <label class="col-xs-12 col-sm-4 col-md-3">Relation :</label>
                            <select placeholder="" name="relation" class="select-ltc col-xs-12 col-sm-8">
                                <option disabled selected hidden>Type de relation</option>
                                <option>Conjoint</option>
                                <option>Amis</option>
                                <option>Collègue</option>
                            </select>
                        </div>
                        <div class="col-xs-12">
                            <label class="col-xs-12 col-sm-4 col-md-3">Message :</label>
                            <textarea class="col-xs-12 col-sm-8" name="message" placeholder="Bonjour, je vous invite à vous inscrire sur le site livetestcar.com afin de faire tester votre véhicule et venir essayer celui de vos rêves ! Rejoignez-moi sur www.livetestcar.com/register "></textarea>
                        </div>
                        <div class="container-submit col-xs-12">
                            <input type="submit" value="Envoyer" class="btn-ltc">
                        </div>
                    </form>
                </div>
            </div>
        </article>
    </section>
@stop

@section('script')
    <script src="/crop/cropper.js"></script>
    <script src="/crop/main.js"></script>

    <script>
        $( document ).ready(function() {
            // initialise file input
            $("#input-id").fileinput();


            var $places = $("#places");

            // geocomplete plugin qui va remplir les input lat et lng en fonction du lieu selectionné
            $places.geocomplete({
                details: "form",
                types: ["geocode", "establishment"]
            });

            $('#modifNom').click(function(){
                $('#rowNomForm').removeClass('hidden');
                $('#rowNom').addClass('hidden');
            });
            $('#modifPrenom').click(function(){
                $('#rowPrenomForm').removeClass('hidden');
                $('#rowPrenom').addClass('hidden');
            });
            $('#modifMail').click(function(){
                $('#rowMailForm').removeClass('hidden');
                $('#rowMail').addClass('hidden');
                $('#rowMailCheck').addClass('hidden');
            });
            $('#modifTel').click(function(){
                $('#rowTelForm').removeClass('hidden');
                $('#rowTel').addClass('hidden');
            });

            $('#modifPassword').click(function(){
                $('#rowPasswordForm').removeClass('hidden');
                $('#rowPassword').addClass('hidden');
            });

            $('#modifBrand').click(function(){
                $('#rowBrandForm').removeClass('hidden');
                $('#rowBrand1').addClass('hidden');
                $('#rowBrand2').addClass('hidden');
                $('#rowBrand3').addClass('hidden');
            });


            $('#modifRegion').click(function(){
                $('#rowRegionForm').removeClass('hidden');
                $('#rowRegion').addClass('hidden');
            });

            $('#modifVille').click(function(){
                $('#rowVilleForm').removeClass('hidden');
                $('#rowVille').addClass('hidden');
            });

            $('#modifCp').click(function(){
                $('#rowCpForm').removeClass('hidden');
                $('#rowCp').addClass('hidden');
            });

            $('#modifPicture').click(function(){
                $('#rowPictureForm').removeClass('hidden');
                $('#rowPicture').addClass('hidden');
            });

            var $brand1 = $("#brand1"), $brand2 = $("#brand2"), $brand3 = $("#brand3");

            var brands = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote:
                {
                    url: "search/autocomplete/brand",
                    prepare: function (query, settings) {
                        settings.type = "POST";
                        settings.contentType = "application/json; charset=UTF-8";
                        settings.data = JSON.stringify({"brand" : query});

                        return settings;
                    }
                }
            });

            $brand1.typeahead({
                        autoselect: true,
                        hint: true,
                        highlight: false,
                        minLength: "1",
                        classNames: {
                            input: 'tt-input',
                            hint: 'tt-hint',
                            menu: 'tt-menu',
                            suggestion: 'tt-suggestion',
                            cursor: 'tt-cursor'
                        }
                    },
                    {
                        name: 'brands',
                        source: brands,
                        limit: 10
                    }
            );

            $brand2.typeahead({
                        autoselect: true,
                        hint: true,
                        highlight: false,
                        minLength: "1",
                        classNames: {
                            input: 'tt-input',
                            hint: 'tt-hint',
                            menu: 'tt-menu',
                            suggestion: 'tt-suggestion',
                            cursor: 'tt-cursor'
                        }
                    },
                    {
                        name: 'brands',
                        source: brands,
                        limit: 10
                    }
            );

            $brand3.typeahead({
                        autoselect: true,
                        hint: true,
                        highlight: false,
                        minLength: "1",
                        classNames: {
                            input: 'tt-input',
                            hint: 'tt-hint',
                            menu: 'tt-menu',
                            suggestion: 'tt-suggestion',
                            cursor: 'tt-cursor'
                        }
                    },
                    {
                        name: 'brands',
                        source: brands,
                        limit: 10
                    }
            );

            $('#brand1, #brand2, #brand3').on('typeahead:asyncreceive', function(e)
            {
                var firstElem = $(this).data().ttTypeahead.menu.getTopSelectable();
                $(this).data().ttTypeahead.menu.setCursor(firstElem);

            });
        });
    </script>
@stop