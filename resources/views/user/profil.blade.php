@extends('template')

@section('title')
   Profil - Live Test Car
@stop

@section('body')
<section class="content gabarit">

    <!--============================== page profil =================================-->

    <div class="container-fluid page-profil">

      <!--============================== informations personnelles =================================-->

      <div class="row information-perso">

        <!--============================== description perso =================================-->
        <div class="col-xs-12 col-sm-8 profil-perso-gauche">
          <div class="container-info description-perso">
            <div class="row">
              <div class="col-xs-12 col-sm-2">
                <figure>
                  <img src="{{ $user->picture or "/img/photo-profil-vide.png" }}">
                </figure>
              </div>
              <div class="col-xs-12 col-sm-10 notation-commentaire">
                <h3>{{ $user->first_name." ".substr($user->last_name, 0, 1)."."}}</h3>
                 <div>
                  @if($user->isMailConfirmed)
                       <p data-icon="Q" class="info-valide">E-mail vérifié</p>
                   @else
                       <p data-icon="3" class="info-non-valide">E-mail non vérifié</p>
                   @endif
                </div>
                <div>
                  <p data-icon="3" class="info-non-valide">Téléphone non vérifié</p>
                </div>
                @if (count($user->recommandations) > 0)
                <div class="notation">
                  <span class="@if ($user->note() >=1) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                  <span class="@if ($user->note() >=2) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                  <span class="@if ($user->note() >=3) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                  <span class="@if ($user->note() >=4) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                  <span class="@if ($user->note() >=5) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                  <span class="txt-notation">- note {{(int)$user->note()}}/5</span>
                </div>
                @endif
              </div>
            </div>
          </div>  
        </div>

        <!--============================== notation + commentaire perso =================================-->
        <div class="col-xs-12 col-sm-4 profil-perso-droite">
          <div class="container-info notation-commentaire">
            <div class="row">
              <!--<div class="col-xs-12">
                @if($user->isMailConfirmed)
                     <p data-icon="e" class="info-valide">E-mail vérifié</p>
                 @else
                     <p data-icon="x" class="info-non-valide">E-mail non vérifié</p>
                 @endif
              </div>
              <div class="col-xs-12">
                <p data-icon="x" class="info-non-valide">Téléphone non vérifié</p>
              </div>
              <div class="col-xs-12 notation">
                <span class="cercle-plein"></span>
                <span class="cercle-plein"></span>
                <span class="cercle-plein"></span>
                <span class="cercle-vide"></span>
                <span class="cercle-vide"></span>
                <span class="txt-notation">- note 3/5</span>
              </div>-->
              <p></p>
            </div>  
          </div>
        </div>

      </div>

      <h2>
         @if(count($user->offers) > 1)
            {{ "Véhicules proposés au test" }}
         @else
            {{ "Véhicule proposé au test" }}
         @endif
      </h2>

      <div class="row">
        <div class="col-xs-12 col-sm-8">
      @if(count($user->offers) > 0)
        @for($i=0; $i<count($user->offers); $i++)
           <!--============================== informations vehicule =================================-->

          <div class="row information-vehicule annonce-1">

            <!--============================== description perso =================================-->
            <div class="col-xs-12 annonce-vehicule-gauche">
              <div class="container-info description-annonce">
                <div class="row">
                  <div class="col-xs-12 col-sm-8">
                    <h3>{{ $user->offers[$i]->car->brand }} <strong>{{ $user->offers[$i]->car->model }}</strong> {{ $user->offers[$i]->color->name }}</h3>
                    <!--<p class="detail-annonce">{{ $user->offers[$i]->fuel->name }}, année {{ $user->offers[$i]->annee }}</p>-->
                    <p class="descriptif-annonce">« {{ $user->offers[$i]->description }} »</p>
                    <div class="row">
                      @foreach($user->offers[$i]->pictures as $picture)
                      <figure class="col-xs-6 col-sm-3">
                        <img src="{{$picture->visuel}}">
                      </figure>
                      @endforeach
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4 prix-annonce link-annonce">
                    <p>Prix de l'essai : <span class="prix">{{ $user->offers[$i]->price }} €</span></p>
                    <a href="/reservation/{{ $user->offers[$i]->id }}" class="btn-link-ltc">Tester ce véhicule</a>
                  </div>
                </div>
                <div class="row fiche-technique accordion" id="accordion2">
                  <div class="accordion-group col-xs-12">
                    <div class="accordion-heading">
                      <a class="accordion-toggle link-ltc" data-toggle="collapse" data-parent="#accordion2" href="#collapse{{$i}}">
                        Afficher les informations du véhicule
                      </a>
                    </div>
                    <div id="collapse{{$i}}" class="accordion-body collapse">
                      <div class="accordion-inner">
                        <ul>
                          <li>année {{ $user->offers[$i]->annee }}</li>
                          <li>{{ $user->offers[$i]->fuel->name }}</li>
                          @foreach($user->offers[$i]->options as $option)
                          <li>{{$option->name}}</li>
                          @endforeach
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <!-- Avis -->
                  <div class="col-xs-12 avis-profil">
                    <h3>Avis sur le profil</h3>
                    @if (count($user->offers[$i]->recommandations) == 0)
                        <div class="container-avis">
                          <div class="row">
                              <div class="col-xs-12">
                                <p>Pas de recommandations pour l'instant</p>
                              </div>
                          </div>
                        </div>
                      @else
                      @foreach ($user->offers[$i]->recommandations as $recommandation)
                      <div class="container-avis">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3 photo-nom">
                              <a href="{{ url('user/profil', [$recommandation->user->id]) }}" class="link-ltc">
                              <figure>
                                <img src="{{$recommandation->user->picture or '/img/photo-profil-vide.png'}}">
                              </figure>
                              <h4>{{$recommandation->user->first_name}} {{$recommandation->user->last_name}}</h4>
                              </a>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                              <h4>Test de : <span>{{$user->offers[$i]->car->brand}} <strong>{{$user->offers[$i]->car->model}}</strong> {{$user->offers[$i]->color->name}}</span><span class="annee-avis"> - {{$recommandation->created_at->format('F Y')}}</span></h4>
                              <p>{{$recommandation->commentaire}}</p>
                              <div class="notation">
                                <span class="@if ($recommandation->note()>=1) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                <span class="@if ($recommandation->note()>=2) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                <span class="@if ($recommandation->note()>=3) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                <span class="@if ($recommandation->note()>=4) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                <span class="@if ($recommandation->note()>=5) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                <span class="txt-notation">- note {{(int)$recommandation->note()}}/5</span>
                              </div>
                            </div>
                        </div>
                      </div>
                      @endforeach
                      @endif
                  </div>
                </div>
              </div>  
            </div>
            </div>
        @endfor
      @endif
    </div>

                <!--==============================  trophée  =================================-->
      <div class="col-xs-12 col-sm-4 container-stat-trophee">
        <div class="trophee-profil">
          <h2>Trophées</h2>
          <div class="container-trophee">
            <div class="row">
              <div class="col-xs-4 trophee-gauche">
                <i data-icon=")"></i>
                <p class="compte-trophee">{{count($user->prets()->where('paye', 1)->get())}} {{trans_choice('profil.lend', count($user->prets()->where('paye', 1)->get()))}}</p>
                <p class="detail-trophee">de véhicule</p>
              </div>
              <div class="col-xs-4 trophee-milieu">
                <i data-icon=")"></i>
                <p class="compte-trophee">{{count($user->reservations()->where('paye', 1)->get())}} {{trans_choice('profil.test', count($user->reservations()->where('paye', 1)->get()))}}</p>
                <p class="detail-trophee">{{trans_choice('profil.done', count($user->reservations))}}</p>
              </div>
              <div class="col-xs-4 trophee-droite">
                <i data-icon=")"></i>
                <p class="compte-trophee">{{$user->avisPositifs()}} avis</p>
                <p class="detail-trophee">{{trans_choice('profil.positif', $user->avisPositifs())}}</p>
              </div>
            </div>
          </div>
          <h2>Stats du profil</h2>
          <div class="container-stats">
            <div class="row">
              <div class="col-xs-12">
                <p><span class="nombre">{{count($user->offers)}}</span> {{trans_choice('profil.vehicle', count($user->offers))}}</p>
                <p><span class="nombre">{{count($user->prets()->where('paye', 1)->get())}}</span> {{trans_choice('profil.lend', count($user->prets()->where('paye', 1)->get()))}} de son véhicule</p>
                <p><span class="nombre">{{count($user->reservations()->where('paye', 1)->get())}}</span> {{trans_choice('profil.test_vehicle', count($user->reservations()->where('paye', 1)->get()))}}</p>
                <p><span class="nombre">{{count($user->recommandations)}}</span> {{trans_choice('profil.comment', count($user->recommandations))}}</p>
                <p><span class="nombre">{{$user->avisPositifs()}}</span> {{trans_choice('profil.avis_positif', $user->avisPositifs())}}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>

  </section><!-- fin div id="content" -->
@stop