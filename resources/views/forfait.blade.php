@extends('template')

@section('title')
    Live Test Car
@stop

@section('header')
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="/js/jquery.geocomplete.js"></script>
@stop

@section('body')
        <!--============================== content =================================-->

    <section class="content gabarit">

        <!--============================= page forfait et tarif ==================================-->

        <div class="container-fluid page-forfait-tarif">

            <h1>Forfait et tarification</h1>

            <div class="row">

                <!--============================= colonne gauche ==================================-->

                <div class="col-xs-12 col-sm-8">

                    <div class="row">
                        <!--============================= forfait classique ==================================-->
                        <article class="col-xs-12 col-sm-6">
                            <div class="forfait forait-classique">
                                <h2>Forfait classique</h2>
                                <p class="prix">20 €<em>/mois</em></p>
                                <p class="credit"><span class="txt-barre">200 crédits</span> <span class="txt-vert">220 crédits</span></p>
                                <h4>Services inclus</h4>
                                <ul>
                                    <li>- service 1</li>
                                    <li>- service 2</li>
                                    <li>- service 3</li>
                                </ul>
                                <div class="link-paiement">
                                    <a href="" class="btn-link-ltc">Payer maintenant</a>
                                </div>
                                <p class="infobulle">-10 %</p>
                            </div>
                        </article>
                        <!--============================= forfait premium ==================================-->
                        <article class="col-xs-12 col-sm-6">
                            <div class="forfait forait-premium">
                                <h2>Forfait classique</h2>
                                <p class="prix">30 €<em>/mois</em></p>
                                <p class="credit"><span class="txt-barre">300 crédits</span> <span class="txt-vert">360 crédits</span></p>
                                <h4>Services inclus</h4>
                                <ul>
                                    <li>- service 1</li>
                                    <li>- service 2</li>
                                    <li>- service 3</li>
                                </ul>
                                <div class="link-paiement">
                                    <a href="" class="btn-link-ltc">Payer maintenant</a>
                                </div>
                                <p class="infobulle">-20 %</p>
                            </div>
                        </article>
                        <!--============================= tarification crédits ==================================-->
                        <article class="col-xs-12 tarification">
                            <h3>Forfait classique</h3>
                            <div class="col-xs-12 col-sm-8">
                                <p class="prix">1 € = <span class="txt-vert bold">10 crédits</span>*</p>
                                <p class="annotation">* Les crédits achetés sur le site sont uniquement disponibles et utilisables sur celui-ci.</p>
                            </div>
                            <div class="col-xs-12 col-sm-4 link-paiement">
                                <a href="" class="btn-link-ltc">Payer maintenant</a>
                            </div>
                        </article>
                        <!--============================= Comparaison des services ==================================-->
                        <article class="col-xs-12 comparaison">
                            <h3>Comparasion des services</h3>
                            <div class="row titre-comparaison">
                                <p class="col-xs-12 col-sm-6 col-gauche">Services</p><p class="col-xs-4 col-sm-2 col-check">
                                    Sans forfait</p><p class="col-xs-4 col-sm-2 col-check">
                                    forfait classique</p><p class="col-xs-4 col-sm-2 col-check">
                                    Forfait premium</p>
                            </div>
                            <ul class="container-fluid">
                                <li class="row">
                                    <div class="col-xs-12 col-sm-6 col-gauche">
                                        <p>Service 1 inclus dans tous les achats</p>
                                    </div>
                                    <div class="col-xs-4 col-sm-2 col-check">
                                        <p data-icon="Q"></p>
                                    </div>
                                    <div class="col-xs-4 col-sm-2 col-check">
                                        <p data-icon="Q"></p>
                                    </div>
                                    <div class="col-xs-4 col-sm-2 col-check">
                                        <p data-icon="Q"></p>
                                    </div>
                                </li>
                                <li class="row">
                                    <div class="col-xs-12 col-sm-6 col-gauche">
                                        <p>Service 2 inclus dans les forfaits</p>
                                    </div>
                                    <div class="col-xs-4 col-sm-2 col-check">
                                        <p data-icon="6"></p>
                                    </div>
                                    <div class="col-xs-4 col-sm-2 col-check">
                                        <p data-icon="Q"></p>
                                    </div>
                                    <div class="col-xs-4 col-sm-2 col-check">
                                        <p data-icon="Q"></p>
                                    </div>
                                </li>
                                <li class="row">
                                    <div class="col-xs-12 col-sm-6 col-gauche">
                                        <p>Service 3 inclus dans le forfait premium</p>
                                    </div>
                                    <div class="col-xs-4 col-sm-2 col-check">
                                        <p data-icon="6"></p>
                                    </div>
                                    <div class="col-xs-4 col-sm-2 col-check">
                                        <p data-icon="6"></p>
                                    </div>
                                    <div class="col-xs-4 col-sm-2 col-check">
                                        <p data-icon="Q"></p>
                                    </div>
                                </li>
                            </ul>
                        </article>
                    </div>

                </div>

                <!--============================= colonne droite ==================================-->

                <div class="col-xs-12 col-sm-4">

                    <!--============================= paiement sécurisé ==================================-->
                    <div class="row">
                        <article class="col-xs-12">
                            <div class="block-paiement">
                                <h3>Paiement sécurisé</h3>
                                <p>Le paiement en ligne par carte bancaire est totalement sécurisé.</p>
                                <p>Achat garanti sans risque : vos données personnelles sont protégées par la technologie SSL.</p>
                                <div class="row logo-paiement">
                                    <figure class="col-xs-12 col-sm-6 img-cb">
                                        <img src="/img/logo-cb.png">
                                    </figure>
                                    <figure class="col-xs-12 col-sm-6 img-paypal">
                                        <img src="/img/logo-paypal.png">
                                    </figure>
                                </div>
                            </div>
                        </article>
                    </div>

                    <!--============================= pourquoi choisir ==================================-->
                    <div class="row">
                        <article class="col-xs-12">
                            <div class="block-choisir">
                                <h3>Pourquoi choisir <nobr>livetestcar.com ?</nobr></h3>
                                <ul>
                                    <li data-icon="Q"><strong>Sécurité :</strong> un service de paiement sécurisé</li>
                                    <li data-icon="Q"><strong>Choix :</strong> aucune restriction aux véhicules proposés</li>
                                    <li data-icon="Q"><strong>Sécurité :</strong> un service de paiement sécurisé</li>
                                    <li data-icon="Q"><strong>Choix :</strong> aucune restriction aux véhicules proposés</li>
                                </ul>
                        </article>
                    </div>

                </div>

            </div>
        </div>

    </section><!-- fin div id="content" -->
@stop
