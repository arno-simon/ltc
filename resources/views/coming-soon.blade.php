@extends('template-light')

@section('title')
   Coming soon - Live Test Car
@stop

@section('body')
    <section class="content container-fluid page-coming-soon">
    	<!--============================== page coming soon =================================-->
    	<article>
            <div id="gif"></div>
    	    <video autoplay="autoplay" loop="loop" id="bgvid" muted="muted" poster="../img/fond-coming-soon.jpg">
                <source src="../img/video/FOND1NB.webm" type="video/webm">
                <source src="../img/video/FOND1NB.ogv" type="video/ogv">
                <source src="../img/video/FOND1NB.mp4" type="video/mp4">
            </video>
            <div id="contenu">
                <p>livetestcar vous permet
                <strong class="essayer">d'essayer</strong> ou de <strong class="preter">prêter</strong>
                un véhicule.
                en toute simplicité.
                en attendant
                <strong class="text-mail">saisissez votre mail</strong>
                et gagnez des cadeaux
                dès votre inscription
                à la sortie du site !</p>
                {!! Form::open(array('action' => 'HomeController@postComingSoon'))!!}
                    <fieldset>
                        <input type="email" name="mail" placeholder="Adresse Email..." class="placeholder">
                        <input type="submit" value="ok">
                    </fieldset>
                    {!! Form::Close() !!}
                </form>
                @if(session()->has('status'))
                    <p class="message-success">
                        {{ session()->get('status') }}
                    </p>
                @endif
            </div>
            <div class="" id="contact">
                <nav class="">
                    <ul class="row">
                        <li class="col-xs-8 col-xs-offset-2">
                            <a href="https://www.facebook.com/livetestcar/"><span data-icon="2" class="icon color-fb"></span><span class="texte">suivez nous sur <span class="color-fb">facebook</span></span></a>
                        </li>
                        <li class="col-xs-8 col-xs-offset-2 mt">
                            <a href="mailto:contact@livetestcar.com"><span data-icon="4" class="icon"></span><span class="texte">contactez nous par <span class="color-rouge">mail</span></span></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </article>

    </section>
@stop