<!DOCTYPE html>
<html lang="fr-FR">

<body>
<h2>Votre filleul s'est inscrit !</h2>

<p>Bonjour {{$parrainage->parrain->first_name}} vous avez un nouveau filleul inscrit:</p>

Vous avez {{$parrainage->parrain->filleuls->count()}} filleuls.
@if ($parrainage->parrain->filleuls->count() == 3)
    <p>Félicitations ! Votre compte vient de passer en compte Premium.</p>
@elseif ($parrainage->parrain->filleuls->count() < 3)
    <p>Plus que {{3 - $parrainage->parrain->filleuls->count()}} filleuls et vous passez au compte Premium.</p>
@endif
</body>
</html>