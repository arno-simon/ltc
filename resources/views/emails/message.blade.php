<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta http-equiv="Content-Type"  content="text/html charset=UTF-8" />
</head>

<body style="margin:29px 0 0; background:url(http://www.livetestcar.com/img/bg-mail.jpg) repeat-x top center">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: roboto, arial, sans-serif;">
        <tbody>
        <tr>
            <td>
                <!-- entete -->
                <table width="600" align="center" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td width="150" height="70" valign="top">
                            <img src="http://www.livetestcar.com/img/logo-ltc-vert.png" style="height: 66px;">
                        </td>
                        <td width="450" height="70" valign="top">
                            <h1 style="color: #39bcce;">Avis de message</h1>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!-- contenu -->
                <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background: #fff;box-shadow: #707070 2px 3px 3px;">
                    <tbody>
                    <tr>
                        <td valign="top" width="20"></td>
                        <td valign="top">
                            <table>
                                <tbody>
                                <tr>
                                    <td valign="top" height="30"></td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <p style="font-weight: bold; margin: 16px 0;">Bonjour {{$conversation->destinataire->first_name}},</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <p style="color: #55656f; margin: 0;">Vous venez de recevoir un message de :</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td valign="top" width="10"></td>
                                                <td valign="top">
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td valign="top" width="60">
                                                                <img src="http://www.livetestcar.com{{$conversation->expediteur->picture}}" style="width: 50%; height: auto;">
                                                            </td>
                                                            <td valign="top" width="20">
                                                            </td>
                                                            <td valign="top" width="460">
                                                                <p style="color: #55656f;margin: 33px 0 0 0;">{{$conversation->expediteur->first_name}}</p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td valign="top" width="10"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <p style="color: #55656f; margin: 0;">Pour visualiser ce message rendez-vous sur votre compte livetestcar ou directement sur ce <a href="http://livetestcar.com/messages/{{$conversation->reservation_id}}" style="color: #39bcce">lien</a>.</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" height="30"></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                        <td valign="top" width="20"></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="#21272b" style="background: #21272b;">
                <table align="center" width="600" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td valign="top" height="15"></td>
                    </tr>
                    <tr>
                        <td valign="top" width="10"></td>
                        <td valign="top" width="460">
                            <p style="font-size: 10px; text-align: left; margin: 16px 0 16px 0; color: #fff">Copyright &copy; 2015 LiveTestCar.com - Tous droit réservés</p>
                        </td>
                        <td valign="top" width="60">
                            <a href="">
                                <img src="http://www.livetestcar.com/img/icone-fb.png">
                            </a>
                        </td>
                        <td valign="top" width="60">
                            <a href="">
                                <img src="http://www.livetestcar.com/img/icone-tw.png">
                            </a>
                        </td>
                        <td valign="top" width="10"></td>
                    </tr>
                    <tr>
                        <td valign="top" height="15"></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</body>

</html>