<!DOCTYPE html>
<html lang="fr-FR">

<body>
<h2>Vérifier votre adresse e-mail</h2>

<p> Merci d'avoir créer un compte sur livetestcar.com.
    Veuillez cliquer sur le lien suivant afin de vérifier votre adresse e-mail :</p>


<p><a href="{{ url('register/verify/' . $confirmation_code) }}">{{ url('register/verify/' . $confirmation_code) }}</a></p>

</body>
</html>