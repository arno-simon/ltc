@extends('template')

@section('title')
    Notation du test - Live Test Car
@stop

@section('body')
    <section class="content gabarit">

        <!--============================== page succes =================================-->

        <div class="container-fluid page-succes">
            <h1>Confirmation réservation</h1>
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <h2>Félicitations,</h2>
                    <p>Vous venez de noter votre test</p>
                </div>
            </div>
        </div>

    </section><!-- fin div id="content" -->
@stop