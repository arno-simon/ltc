<!--============================== footer =================================-->
<footer>
    <div class="container-fluid container-footer">
        <nav class="row container-link-footer">
            <div class="col-xs-12 col-sm-3">
                <h4>Infos pratiques</h4>
                <ul>
                    <li><a href="#" data-toggle="modal" data-target=".comment-ca-marche">Comment ça marche ?</a></li>
                    <li><a href="#">Tester une voiture</a></li>
                    <li><a href="#">Proposer sa voiture</a></li>
                    <li><a href="/guides">Guides</a></li>
                    <li><a href="/guide/1">Guide détail</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-3">
                <h4>Expériences utilisateurs</h4>
                <ul>
                    <li><a href="#">Témoignages et anecdotes</a></li>
                    <li><a href="#">Les avis</a></li>
                    <li><a href="#">FAQ / questions fréquentes</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-3">
                <h4>A propos</h4>
                <ul>
                    <li><a href="#">Qui sommes nous ?</a></li>
                    <li><a href="#">Charte de bonne conduite</a></li>
                    <li><a href="#">Paiement sécurisé</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-3 social-link-footer">
                <a href="https://www.facebook.com/livetestcar/" target="_blank" data-icon="2"></a>
                <a href="https://twitter.com/livetestcar" target="_blank" data-icon="*"></a>
            </div>
        </nav>
        <div class="row container-mention-footer">
            <p class="col-xs-12">Copyright <span data-icon="7"></span>2016 LiveTestCar.com <span class="display-none-mobile">-</span><span class="retour-ligne-mobile">Tous droit réservés</span></p>
        </div>
    </div>
</footer>

<section class="modal fade modal-page-statique comment-ca-marche" tabindex="-1" role="dialog" aria-labelledby="Comment ça marche">
  <article class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" data-icon="6"></span></button>
          <h1 class="modal-title" id="myModalLabel">Comment ça marche</h1>
        </div>
        <div class="modal-body">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hendrerit tortor a justo tincidunt pretium. Phasellus porta, urna eu ornare mollis, sem leo iaculis leo, in sagittis justo neque et sapien. Sed viverra erat leo, scelerisque sollicitudin mauris ullamcorper vitae. Ut scelerisque mi felis, a volutpat tellus aliquet et. Etiam sit amet mi eget tellus euismod accumsan nec molestie orci. Nulla eros quam, vehicula at finibus id, hendrerit et augue. Praesent vitae erat at lorem tincidunt semper congue ut metus. Fusce fringilla eros ut quam dictum eleifend. Vestibulum dictum purus a risus gravida venenatis. Phasellus eget felis sed mi gravida varius. Etiam ut tortor nunc. Vivamus malesuada ligula nibh. Donec tincidunt odio eu scelerisque finibus. Duis ut ipsum vel tortor malesuada facilisis at sed libero. Sed tempus nunc ac nibh convallis elementum.</p>

          <p>Maecenas ut neque vel diam maximus commodo. Donec non risus at mauris sagittis cursus. Donec sit amet faucibus risus. Proin a massa non mauris fringilla convallis. Nunc mattis lobortis est sed maximus. Integer gravida felis vitae enim fringilla, ac pellentesque purus facilisis. Nam varius nisl at tristique vehicula. Sed non augue et metus iaculis vehicula sed sit amet risus. Quisque vulputate fermentum tellus a eleifend. Cras viverra vulputate eros, id consequat nibh. Curabitur laoreet quis sem at rhoncus.</p>

          <p>Etiam quis varius dolor. Nullam vel ullamcorper risus. Maecenas bibendum quis neque quis ullamcorper. Quisque accumsan sapien ac felis tristique, quis commodo sem cursus. Sed ultricies, ligula at sagittis elementum, libero libero interdum tortor, ac congue ipsum leo sit amet ipsum. Suspendisse potenti. Aenean bibendum ipsum id semper imperdiet. Sed blandit orci turpis, a imperdiet quam consequat eget. Phasellus hendrerit ex sed dignissim luctus. Donec non elementum metus, eu tincidunt lorem. Duis scelerisque vehicula risus. Praesent vitae mi elit. Praesent tincidunt leo ut purus sagittis luctus.</p>

          <p>Nunc et bibendum nunc. Mauris varius odio quis ligula viverra dapibus eu nec nulla. Donec ut turpis ut felis consectetur molestie. Nulla ullamcorper lectus felis, nec elementum ligula tincidunt a. Nulla auctor sodales felis, at egestas magna facilisis et. Morbi condimentum commodo nulla, in porttitor sapien. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse id elementum arcu. Nulla a placerat ipsum. Duis in aliquet urna, eu vestibulum lorem. Praesent at congue orci. Fusce porta orci turpis, et tempus odio dapibus sit amet. Nullam sagittis mauris consectetur, molestie felis vitae, molestie urna. Nullam sed eros volutpat, consectetur metus sed, tempus felis. Donec rhoncus tempor leo. Proin sed aliquet purus.</p>
        </div>
    </div>
  </article>
</section>