<!--============================== header =================================-->
<header>
    <nav class="navbar navbar-default">
        <div class="container-fluid header-nav">
            <div class="navbar-header">
                <a class="navbar-brand" href="/"><img alt="logo mazet des couleurs" title="logo mazet des couleurs" src="/img/logo-vert-blanc.png"></a>
                <div class="container-btn-univers">
                    <button type="button" class="navbar-toggle collapsed btn-univers" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">Choisissez votre univers
                    <span data-icon="M"></span></button>
                </div>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    @if(Auth::check())
                        @else
                        <li><a href="{{ url('register') }}" class="link-ltc">Créer un compte</a></li>
                        @if(Request::segment(1) != "login")
                            <li><a id="connexion" href="#" class="link-ltc link-connexion-header">Me connecter</a></li>
                        @endif
                        @endif
                        <!--<li>
                            <a href="{{ route('offer.new') }}" class="btn-link-ltc">Proposer ma voiture</a>
                        </li>-->
                        @if(Auth::check())
                        <li class="link-relation">
                            <p class="link-ltc link-mise-relation"><span data-icon="4"></span><span class="nb-relation">{{\Auth::user()->nbUnreadConversations()}}</span></p>
                        </li>
                        <li class="photo-profil">
                            <a href="{{ url('my-account') }}" class="link-photo-profil">
                                <img src="{{isset(\Auth::user()->picture) ? \Auth::user()->picture : "/img/photo-profil-vide.png"}}">
                            </a>
                            <a href="{{ url('my-account') }}" class="nb-credit"><p>{{\Auth::user()->credits}}</p><p>@if (\Auth::user()->credits>1){{"crédits"}}@else{{"crédit"}}@endif</p></a>
                        </li>
                        <li class="header-deconnexion"><a href="{{ url('logout') }}" class="link-ltc" data-icon="a"></a></li>
                    @endif
                    <li>
                        @if(!Auth::check())
                            <a href="{{ url('register/pro') }}" class="link-ltc link-pro">Vous êtes un pro ?</a>
                        @endif
                    </li>
                </ul>
            </div><!-- /.navbar-collapse 1-->
            <div class="collapse navbar-collapse collapse-univers" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                    <li><a href="/citadine" class="link-ltc">Citadine</a></li>
                    <li><a href="/break" class="link-ltc">Break</a></li>
                    <li><a href="/monospace" class="link-ltc">Monospace</a></li>
                    <li><a href="/berline" class="link-ltc">Berline</a></li>
                    <li><a href="/4x4" class="link-ltc">4 x 4</a></li>
                    <li><a href="/sportive" class="link-ltc">Sportive</a></li>
                    <li><a href="/collection" class="link-ltc">Collection</a></li>
                    <li><a href="/electrique" class="link-ltc">Electrique</a></li>
                </ul>
            </div><!-- /.navbar-collapse 1-->
            @if (Auth::check())
            <div class="container-list-relation">
                <p class="close-relation"><span data-icon="6"></span></p>
                <p class="no-msg"><span class="icon" data-icon="3"></span><span class="text">Vous n'avez pas de message actuellement</span></p>
                <ul>
                    @for($i=0; $i<count(\Auth::user()->unreadConversations()) && $i<2; $i++)
                    <li class="row">
                        <figure class="col-xs-3 col-sm-2">
                            <img src="{{isset(\Auth::user()->unreadConversations()[$i]->expediteur->picture) ? \Auth::user()->unreadConversations()[$i]->expediteur->picture : "/img/photo-profil-vide.png"}}">
                        </figure>
                        <div class="col-xs-12 col-sm-7">
                            <a href="/user/profil/{{\Auth::user()->unreadConversations()[$i]->reservation->offer->preteur_id}}" class="relation-vehicule">{{\Auth::user()->unreadConversations()[$i]->reservation->offer->car->model}}</a>
                            <p class="relation-titre">{{\Auth::user()->unreadConversations()[$i]->expediteur->first_name." ".substr(\Auth::user()->unreadConversations()[$i]->expediteur->last_name, 0, 1)."."}} de {{\Auth::user()->unreadConversations()[$i]->expediteur->ville}}</p>
                            <p class="relation-texte">{{\Auth::user()->unreadConversations()[$i]->texte}}</p>
                        </div>
                        <div class="col-xs-12 col-sm-3 relation-date">
                            <p>{{\Auth::user()->unreadConversations()[$i]->created_at->format('d M')}}.</p>
                            <a href="{{url('/messages', [\Auth::user()->unreadConversations()[$i]->reservation_id])}}" class="btn-link-ltc">Répondre</a>
                        </div>
                    </li>
                    @endfor
                </ul>
                <div class="ta-c">
                    <a href="/messages" class="btn-link-ltc">Accéder à ma boite de réception</a>
                </div>
            </div>
            @endif
        </div><!-- /.container-fluid -->
    </nav>
    @if(Request::segment(1) != "login")
        <div id="connexion-form" class="container-fluid connexion-header
            @if(count($errors->login) > 0)
                {{ "open" }}
            @endif">
            {!! Form::open(array('route' => 'login.post')) !!}
                <span class="close-connexion" data-icon="6"></span>
                <p class="message-erreur" id="error-login">
                    {!! count($errors->login) > 0 ? $errors->login->first('email') ."  ". $errors->login->first('password').$errors->login->first('attempt') : "" !!}
                </p>
                <div class="container-login-fb">
                    <a class="login-fb" href="auth/facebook" role="button" data-icon="B"><span>connexion avec facebook</span></a>
                    <div class="connexion-separator">
                        <span class="separator"></span><p>ou</p><span class="separator"></span>
                    </div>
                </div>
                <input type="email" name="email" placeholder="Adresse mail" class="input-ltc" value="{{ old('email') }}">
                <div class="block-mdp">
                    <input type="password" name="password" placeholder="Mot de passe" class="input-ltc">
                    <a href="{{ url('password/email') }}" class="link-ltc">Mot de passe oublié ?</a>
                </div>
                <input type="submit" value="Se connecter" class="btn-ltc">
            {!! Form::close() !!}
        </div>
    @endif

</header>