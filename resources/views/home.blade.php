@extends('template')

@section('title')
   Live Test Car
@stop

@section('header')
    <script src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="/js/jquery.geocomplete.js"></script>
@stop

@section('body')
    <!-- ca marche pas ! -->
    <div
            class="fb-like"
            data-share="true"
            data-width="450"
            data-show-faces="true">
    </div>
<!--============================== content =================================-->
<section class="content">
    <!--============================== Bandeau img =================================-->
    <div class="container-fluid bandeau-img-index" style="background-image: url('../img/fond-home.jpg');">
        <div id="carousel-bandeau" class="carousel slide block-flou row" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="item active fond-home">
                  {{--<img src="../img/image-bandeau.jpg" alt="...">--}}
                  <div id="gifHome"></div>
                  <video autoplay="autoplay" loop="loop" id="bgvidHome" muted="muted" poster="../img/fond-home.jpg">
                      <source src="../img/video/FOND2NB.webm" type="video/webm">
                      <source src="../img/video/FOND2NB.ogv" type="video/ogv">
                      <source src="../img/video/FOND2NB.mp4" type="video/mp4">
                  </video>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 contenu-bandeau-haut">
                <div class="col-xs-12 col-sm-4 col-sm-offset-1 col-md-4 col-md-offset-1 container-univers">
                    <div class="col-xs-12 content-univers">
                        <h2>bienvenue sur Live Test Car</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur imperdiet neque non facilisis egestas. Maecenas at euismod leo. Nulla lobortis massa in urna elementum</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-5 col-md-offset-1 container-form-search">
                    {!! Form::open(array('route' => 'results', 'class' => 'form-search-index', 'id' => 'form')) !!}
                        <h1>Tester une voiture près de chez vous !</h1>
                        <a href="{{ route('offer.new') }}" class="btn-proposer-voiture">Proposer ma voiture<span data-icon="V"></span><span class="effet"></span></a>
                        <label id="localisation-label" for="search-localisation">Localisation (dans un rayon de)</label>
                        @if ($errors->has('localisation'))
                            <p id="localisation-error" class="message-erreur">
                                    <span class="txt-underline">{{ $errors->first('localisation') }}</span><br/>
                            </p>
                        @endif
                         @if ($errors->has('lat'))
                            <p id="localisation-error" class="message-erreur">
                                    <span class="txt-underline">{{ $errors->first('lat') }}</span><br/>
                            </p>
                        @endif
                         @if ($errors->has('lng'))
                            <p id="localisation-error" class="message-erreur">
                                    <span class="txt-underline">{{ $errors->first('lng') }}</span><br/>
                            </p>
                        @endif

                        <div class="block-input-select">
                            <input id="places" type="text" name="localisation" class="input-ltc" placeholder="Ex: Rennes" value="{{ old('localisation') }}"><select id="rayon" class="select-ltc" name="rayon">
                                <option value="30" selected>30 km</option>
                                <option value="60">60 km</option>
                                <option value="90">90 km</option>
                            </select>
                        </div>

                        <label for="brand">Marque</label>
                        @if ($errors->has('brand'))
                            <p class="message-erreur">
                                <span class="txt-underline">{{ $errors->first('brand') }}</span><br/>
                            </p>
                        @endif
                        <div class="container-brand">
                            <input id="brand" class="input-ltc" type="text" name="brand" placeholder="Ex: Mini" value="{{ old('brand') }}">
                            <!-- Logo de la marque choisie a rajouter en variable dans la balise image -->
                            <span id="span-logo-brand" style="visibility: hidden" class="logo-brand">
                                <img id="logo-marque" src="">
                            </span>
                        </div>

                        <label for="model">Modèle</label>
                        @if ($errors->has('model'))
                            <p class="message-erreur">
                                <span class="txt-underline">{{ $errors->first('model') }}</span><br/>
                            </p>
                        @endif
                        <input id="model" type="text" name="model" class="input-ltc" placeholder="Ex: Cooper S" value="{{ old('model') }}">

                        <input id="lat" name="lat" type="hidden" value="" autocomplete="off">
                        <input id="lng" name="lng" type="hidden" value="" autocomplete="off">

                        <input type="submit" value="ok" class="btn-ltc">
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="container-list-univers">
                <div id="carousel-link-univers" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <ul class="col-xs-8 col-xs-offset-2 item active">
                            <li>
                                <a href="/citadine" style="background-image: url('../img/citadine.png')"><span>Citadine</span></a>
                            </li><li>
                                <a href="/break" style="background-image: url('../img/break.png')"><span>Break</span></a>
                            </li><li>
                                <a href="/monospace" style="background-image: url('../img/monospace.png')"><span>Monospace</span></a>
                            </li><li>
                                <a href="/berline" style="background-image: url('../img/berline.png')"><span>Berline</span></a>
                            </li>
                        </ul>
                        <ul class="col-xs-8 col-xs-offset-2 item">
                            <li>
                                <a href="/4x4" style="background-image: url('../img/4x4.png')"><span>4 x 4</span></a>
                            </li><li>
                                <a href="/sportive" style="background-image: url('../img/sportive.png')"><span>Sportive</span></a>
                            </li><li>
                                <a href="/collection" style="background-image: url('../img/collection.png')"><span>Collection</span></a>
                            </li><li>
                                <a href="/electrique" style="background-image: url('../img/electrique.png')"><span>Electrique</span></a>
                            </li>
                        </ul>
                        <ul class="col-xs-8 col-xs-offset-2 item">
                            <li>
                                <a href="/tuning" style="background-image: url('../img/tuning.png')"><span>Tuning</span></a>
                            </li>
                        </ul>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-link-univers" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" data-icon="R" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-link-univers" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" data-icon="O" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!--============================== Video =================================-->

    <div class="container-fluid video-index gabarit">
        <article class="row">
            <div class="col-xs-12">
                <div class="row container-video">
                    <div class="col-xs-12 col-sm-7 video">
                        <iframe src="https://player.vimeo.com/video/170593927" width="100%" height="auto" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>
                    <div class="col-xs-12 col-sm-5 texte">
                        <div class="va-container">
                            <div class="va-middle">
                                <h2>Vivez l'expérience Live Test Car</h2>
                                <p>Rejoignez la communauté Live Test Car en proposant votre véhicule ou en venant essayer un véhicule d'un de nos membres situé proche de chez vous !</p>
                                <div class="container-login-fb">
                                    <a class="login-fb" href="auth/facebook" role="button" data-icon="B"><span>j'aime</span></a>
                                    <a class="login-fb" href="auth/facebook" role="button"><span>partager</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>

    <!--============================== Tuto =================================-->

    <div class="container-fluid video-index gabarit">
        <article class="row">
            <div class="col-xs-12">
                <div class="row container-video">
                    <div class="col-xs-12 col-sm-5 texte">
                        <div class="va-container">
                            <div class="va-middle">
                                <h2>Fonctionnement</h2>
                                <p>Vous êtes propriétaire et voulez faire partager votre véhicule. Vous souhaitez essayer une voiture proche de chez vous ? Retrouver le fonctionnement complet du site selon votre profil.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7 video">
                        <div class="lien-tuto">
                            <a data-toggle="modal" data-target="#modalFonctionnement"><img src="/img/screen-tuto.JPG"></a>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="modalFonctionnement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <a href="/img/fonctionnement-ltc.pdf" target="_blank" class="btn-link-ltc">Télécharger en format pdf</a>
                              </div>
                              <div class="modal-body">
                                <img src="img/fonctionnement-ltc.JPG">
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>

    <!--============================== Slider Avis =================================-->

    <div class="slider-avis-accueil">
        <div id="carousel-avis" class="carousel slide" data-ride="carouse3">
            <div class="row carousel-inner" role="listbox">
                <div class="item active">
                    <div class="content-avis col-xs-10 col-xs-offset-1">
                        <div class="avis-texte">
                            <div class="titre-notation">
                                <h4>Avis laissé à {{$recommandations[0]->offer->preteur->name}}</h4>
                                <div class="notation">
                                    <span class="@if ($recommandations[0]->note()>=1) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[0]->note()>=2) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[0]->note()>=3) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[0]->note()>=4) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[0]->note()>=5) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="txt-notation">- note {{(int)$recommandations[1]->note()}}/5</span>
                                </div>
                            </div>
                            <p>"{{$recommandations[0]->commentaire}}"</p>
                            <div class="auteur-image">
                                <figure class="avis-img">
                                    <img src="{{$recommandations[0]->user->picture or '/img/no_img_profil.png'}}">
                                </figure>
                                <p><h3>{{$recommandations[0]->user->name}}</h3></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="content-avis col-xs-10 col-xs-offset-1">
                        <div class="avis-texte">
                            <div class="titre-notation">
                                <h4>Avis laissé à {{$recommandations[1]->offer->preteur->name}}</h4>
                                <div class="notation">
                                    <span class="@if ($recommandations[1]->note()>=1) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[1]->note()>=2) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[1]->note()>=3) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[1]->note()>=4) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="@if ($recommandations[1]->note()>=5) {{'cercle-plein'}}@else {{'cercle-vide'}}@endif"></span>
                                    <span class="txt-notation">- note {{(int)$recommandations[1]->note()}}/5</span>
                                </div>
                            </div>
                            <p>{{$recommandations[1]->commentaire}}</p>
                            <div class="auteur-image">
                                <figure class="avis-img">
                                    <img src="{{$recommandations[1]->user->picture or '/img/no_img_profil.png'}}">
                                </figure>
                                <p><h3>{{$recommandations[1]->user->name}}</h3></p>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-avis" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" data-icon="R" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-avis" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" data-icon="O" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <!--============================== Articles =================================-->

    <div class="container-fluid container-article-index gabarit">
        <div class="row">
            <article class="col-xs-12 col-sm-4 article-index">
                <h3 data-icon="9">Le prêt du volant ?</h3>
                <p>Livetestcar c’est une nouvelle façon de tester un véhicule près de chez vous ! Conduisez avant d’acheter grâce aux précieux conseils d’un propriétaire du modèle que vous souhaitez tester&nbsp;!</p>
                <a href="#" class="link-ltc">En savoir +</a>
            </article>
            <article class="col-xs-12 col-sm-4 article-index">
                <h3 data-icon="U">Je veux tester une voiture</h3>
                <p>Livetestcar vous permet de tester tous les modèles de voiture mis à disposition de la communauté et à proximité de chez vous. Rien de plus simple, il vous suffit de rechercher le modèle que vous souhaitez, de vous inscrire de réserver le véhicule et échanger avec les membres Live Test Car. Tester un modèle de voiture avec les conseils avisés d’un propriétaire n’aura jamais été aussi simple !</p>
                <a href="http://livetestcar.com/guide/6" class="link-ltc">En savoir +</a>
            </article>
            <article class="col-xs-12 col-sm-4 article-index">
                <h3 data-icon="W">Je veux proposer ma voiture</h3>
                <p>Livetestcar vous offre l’opportunité de faire tester votre véhicule tout en gagnant de l’argent ! Pour cela, devenez membre de la communauté Live Test Car, remplissez votre profil et les caractéristiques de votre véhicule. Après réservation, vous serez directement mis en relation avec un autre membre souhaitant tester votre véhicule ! Une manière simple de gagner un peu d’argent en donnant de précieux conseils automobiles !</p>
                <a href="http://livetestcar.com/guide/5" class="link-ltc">En savoir +</a>
            </article>
        </div>
    </div>

</section><!-- fin div id="content" -->
@stop

@section('script')
    <script>
        $( document ).ready(function() {
            var $places = $("#places");

            // geocomplete plugin qui va remplir les input lat et lng en fonction du lieu selectionné
            $places.geocomplete({
                details: "form",
                types: ["geocode", "establishment"]
            });


            var $brand = $("#brand");
            var $model = $("#model");

            // constructs the suggestion engine
            var brands = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote:
                {
                    url: "search/autocomplete/brand",
                    prepare: function (query, settings) {
                        settings.type = "POST";
                        settings.contentType = "application/json; charset=UTF-8";
                        settings.data = JSON.stringify({"brand" : query});

                        return settings;
                    }
                }
            });
            var models = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote:
                {
                    url: "search/autocomplete/model",
                    prepare: function (query, settings) {
                        settings.type = "POST";
                        settings.contentType = "application/json; charset=UTF-8";
                        settings.data = JSON.stringify({"model" : query, "brand":$brand.val()});

                        return settings;
                    }
                }
            });

            $brand.typeahead({
                        autoselect: true,
                        hint: true,
                        highlight: false,
                        minLength: "1",
                        classNames: {
                            input: 'tt-input',
                            hint: 'tt-hint',
                            menu: 'tt-menu',
                            suggestion: 'tt-suggestion',
                            cursor: 'tt-cursor'
                        }
                    },
                    {
                        name: 'brands',
                        source: brands,
                        limit: 10
                    }
            )
                    .bind('typeahead:select', function(ev, suggestion) {
                        $model.val('');
                        document.getElementById('span-logo-brand').style.visibility="visible";
                        var logo_name = $brand.val().replace(' ', '-');
                        logo_name = logo_name.toLowerCase();
                        $("#logo-marque").attr("src","/img/logo-marque/logo-"+logo_name+".jpg");
                    });

            $model.typeahead({
                        autoselect: true,
                        hint: true,
                        highlight: false,
                        minLength: "1",
                        classNames: {
                            input: 'tt-input',
                            hint: 'tt-hint',
                            menu: 'tt-menu',
                            suggestion: 'tt-suggestion',
                            cursor: 'tt-cursor'
                        }
                    },
                    {
                        name: 'models',
                        source: models,
                        limit: 20
                    }
            );
            $('.twitter-typeahead').css('width', '80%');


            $('#brand, #model').on('typeahead:asyncreceive', function(e)
            {
                var firstElem = $(this).data().ttTypeahead.menu.getTopSelectable();
                $(this).data().ttTypeahead.menu.setCursor(firstElem);
            });

            $model.on('typeahead:select', function(e)
            {
                $.ajax({
                    method: "POST",
                    url: "search/autocomplete/model-brand",
                    data: JSON.stringify({ 'model': $model.val()}),
                    dataType: "json",
                    contentType: "application/json; charset=UTF-8"
                })
                    .done(function( data ) {
                        $brand.val(data.brand);
                    });
            });

            $('#form').submit(function()
            {
                if($('#lat').val() !== '' && $('#lng').val() !== '')
                {
                  return true;

                }
                else
                {
                  $('#localisation-label').after("<p class='message-erreur'><span class='txt-underline'>Veuillez selectionner un lieu dans les suggestions proposées</span><br/></p>");
                  return false;
                }
            });
        });
    </script>
@stop