@extends('template')

@section('title')
    Live Test Car
@stop

@section('header')
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="/js/jquery.geocomplete.js"></script>
@stop

@section('body')
        <!--============================== content =================================-->

    <section class="content gabarit">

        <!--============================= page forfait et tarif ==================================-->

        <div class="container-fluid page-forfait-tarif">
            <p>Paiement par crédits OK. Pas de vérification ni de déduction du nombre de crédit de l'utilisateur.</p>
        </div>

    </section><!-- fin div id="content" -->
@stop
