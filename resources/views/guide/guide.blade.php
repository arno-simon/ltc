@extends('template')

@section('title')
   Guide - Live Test Car
@stop

@section('body')
<section class="content gabarit">

	<!--============================== page guide =================================-->

    <div class="container-fluid page-guide">
        <h1>{{$guide->titre}}</h1>
        <div class="row">
            <div class="col-xs-12 col-sm-8 colonne-gauche">
                <article>
                    <em>Mise à jour : {{$guide->updated_at}}</em>
                    <div>
                        <img src="{{$guide->visuel}}" class="img-responsive pull-left">
                        <p>{!! $guide->contenu !!}</p>
                    </div>
                    <div class="btn-link-guide">
                        <a href="#" class="btn-link-ltc">Empruntez un véhicule</a>
                    </div>
                </article>
            </div>
            <div class="col-xs-12 col-sm-4 colonne-droite">
                <div class="encart-pub-guide">
                    <p>ENCART PUB</p>
                </div>
                <div class="form-guide">
                {!! Form::open(array('route' => 'results', 'class' => 'form-search-index', 'id' => 'form')) !!}
                    <h2>Tester une voiture près de chez vous !</h2>

                    <label id="localisation-label" for="search-localisation">Localisation (dans un rayon de)</label>
                    @if ($errors->has('localisation'))
                        <p id="localisation-error" class="message-erreur">
                                <span class="txt-underline">{{ $errors->first('localisation') }}</span><br/>
                        </p>
                    @endif
                     @if ($errors->has('lat'))
                        <p id="localisation-error" class="message-erreur">
                                <span class="txt-underline">{{ $errors->first('lat') }}</span><br/>
                        </p>
                    @endif
                     @if ($errors->has('lng'))
                        <p id="localisation-error" class="message-erreur">
                                <span class="txt-underline">{{ $errors->first('lng') }}</span><br/>
                        </p>
                    @endif

                    <div class="block-input-select">
                        <input id="places" type="text" name="localisation" class="input-ltc" placeholder="Ex: Rennes" value="{{ old('localisation') }}"><select id="rayon" class="select-ltc" name="rayon">
                            <option value="30" selected>30 km</option>
                            <option value="60">60 km</option>
                            <option value="90">90 km</option>
                        </select>
                    </div>

                    <label for="brand">Marque</label>
                    @if ($errors->has('brand'))
                        <p class="message-erreur">
                            <span class="txt-underline">{{ $errors->first('brand') }}</span><br/>
                        </p>
                    @endif
                    <input id="brand" class="input-ltc" type="text" name="brand" placeholder="Ex: Mini" value="{{ old('brand') }}">

                    <label for="model">Modèle</label>
                    @if ($errors->has('model'))
                        <p class="message-erreur">
                            <span class="txt-underline">{{ $errors->first('model') }}</span><br/>
                        </p>
                    @endif
                    <input id="model" type="text" name="model" class="input-ltc" placeholder="Ex: Cooper S" value="{{ old('model') }}">

                    <input id="lat" name="lat" type="hidden" value="" autocomplete="off">
                    <input id="lng" name="lng" type="hidden" value="" autocomplete="off">

                    <input type="submit" value="ok" class="btn-ltc">
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

</section><!-- fin div id="content" -->
@stop