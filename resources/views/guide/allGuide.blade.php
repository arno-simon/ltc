@extends('template')

@section('title')
   Guide - Live Test Car
@stop

@section('body')
<section class="content gabarit">

        <!--============================== page all guide =================================-->

    <div class="container-fluid all-guide">
        <h1>Guides Live test car</h1>
        <div class="row">
            <!-- Articles -->
            @foreach($guides as $guide)
            <!--<article class="col-xs-6 col-sm-4 col-md-3 colonne-guide" ontouchstart="this.classList.toggle('hover');">
                <div class="block-all-guide">
                    <div class="front">
                        <h2>{{$guide->titre}}</h2>
                        <figure>
                            <img src="{{url($guide->visuel)}}">
                        </figure>
                        <div class="link-front">
                            <p>En savoir +</p>
                        </div>
                    </div>
                    <div class="back">
                        <h2>{{$guide->titre}}</h2>
                        <figure>
                            <img src="{{$guide->visuel}}">
                        </figure>
                        <h3>
                            <a href="{{url('guide', $guide->id)}}">{{$guide->titre}}</a>
                        </h3>
                        <p>{!! substr($guide->contenu, 0, 50) !!}...</p>
                        <div class="link-detail-guide">
                            <a href="{{url('guide', $guide->id)}}" class="btn-link-ltc">{{$guide->titre}}</a>
                        </div>
                    </div>
                </div>
            </article>-->

            <article class="col-xs-6 col-sm-4 col-md-3 colonne-guide" ontouchstart="this.classList.toggle('hover');">
                <div class="block-all-guide">
                    <h2>{{$guide->titre}}</h2>
                    <a href="{{url('guide', $guide->id)}}"  style="background-image: url('{{url($guide->visuel)}}')">
                        <span class="guide-overlay"></span>
                        <span class="btn-link-ltc">En savoir +</span>
                    </a>
                </div>
            </article>
            @endforeach
            <!-- fin articles -->
        </div>
    </div>

  </section><!-- fin div id="content" -->
@stop