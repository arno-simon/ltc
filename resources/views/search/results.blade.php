@extends('template')

@section('title')
    Live Test Car
@stop

@section('header')
    <script type="text/javascript" src="/js/jquery.mixitup.min.js"></script>

    <style>
        #Container .mix{
            display: none;
        }
    </style>

    @stop

    @section('body')
            <!--============================== content =================================-->

    <section class="content gabarit">

        <!--============================== page resultat =================================-->

        <div class="container-fluid page-resultat">

            <!--============================== map =================================-->

            <input type="hidden" id="lat" value="{{$search['lat']}}" />
            <input type="hidden" id="lng" value="{{$search['lng']}}" />

            <div id="bandeau-map" class="bandeau-map">
                <!--               <iframe src="https://www.google.com/maps/embed/v1/place?q={{ $search['localisation'] }}&key=AIzaSyDjj3FX4E1-130KGNxQCFZHcTJrvuO55WM" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe> -->
            </div>


            <!--============================== nombre résultat =================================-->

            <div class="row container-nbr-resultat">
                <div class="col-xs-12 col-sm-8 text-nbr-resultat">
                    <p class="titre-nbr-resultat"><span class="nbr-resultat">{{ count($offers) }} {{ count($offers) > 1 ? "résultats" : "résultat" }}</span> pour votre recherche</p>
                    <p class="marque-nbr-resultat">{{ $search['brand'] }} <span class="bold">{{ $search['model'] }}</span></p>
                    <p class="localisation-nbr-resultat">A {{ $search['localisation'] }} <span class="alentours-nbr-resultat">({{ $search['rayon'] }} km alentours)</span></p>
                    <a href="/" class="link-ltc" onclick="window.history.back()">Modifier la recherche</a>
                </div>
                @if(count($offers) > 0)
                    <div class="col-xs-12 col-sm-4 prix-nbr-resultat">
                        <div class="text-prix">
                            <p class="link-prix"><span>à partir de</span><span class="prix">{{ $minimumPrice }} €</span><span>l'essai</span></p>
                        </div>
                    </div>
                @endif
            </div>

            <!--============================== liste résultat =================================-->
            @if(count($offers) == 0)
                <h2>Pas de résultats</h2>
            @else
                <h2>Résultat(s)</h2>

                <!--========= lien pour ecran large ========-->
                <div class="row container-link-ltc">
                    <div class="col-sm-4 col-sm-offset-2 pr-0 filtre-modele">
                        <a class="link-ltc">Trier par modèle</a><a href="#" data-icon="N" class="sort link-ltc fleche-tri" data-sort="model:asc" onclick="return false"></a><a href="#" data-icon="P" class="sort link-ltc fleche-tri" data-sort="model:desc" onclick="return false"></a>
                    </div>
                    <div class="col-sm-4 pl-0 pr-0 filtre-localisation">
                        <a class="link-ltc">Trier par localisation</a><a href="#" data-icon="N" class="sort link-ltc fleche-tri" data-sort="location:asc" onclick="return false"></a><a href="#" data-icon="P" class="sort link-ltc fleche-tri" data-sort="location:desc" onclick="return false"></a>
                    </div>
                    <div class="col-sm-2 pl-0 filtre-prix">
                        <a class="link-ltc">Trier par prix</a><a href="#" data-icon="N" class="sort link-ltc fleche-tri" data-sort="price:asc" onclick="return false"></a><a href="#" data-icon="P" class="sort link-ltc fleche-tri" data-sort="price:desc" onclick="return false"></a>
                    </div>
                </div>
                <!--========= select pour ecran mobile ========-->
                <div class="row container-select-ltc">
                    <div>
                        <select name="select-tri" class="select-ltc">
                            <option>Filtrer par modèle croissant</option>
                            <option>Filtrer par modèle décroissant</option>
                            <option>Filtrer par localisation croissant</option>
                            <option>Filtrer par localisation décroissant</option>
                            <option>Filtrer par prix croissant</option>
                            <option>Filtrer par prix décroissant</option>
                        </select>
                    </div>
                </div>

                <div id="Container">
                    <!--========= liste des résultats ========-->
                    @for($i=0; $i<count($offers); $i++)
                        <div id="{{$offers[$i]->id}}" href="" class="mix row container-liste-resultat" data-model="{{ $offers[$i]->car->brand." ".$offers[$i]->car->model }}" data-location="{{ $offers[$i]->location_name }}" data-price="{{ $offers[$i]->price }}">
                            <div class="col-xs-12 col-sm-2 photo-liste-resultat">
                                <a href="{{ url('user/profil', [$offers[$i]->preteur->id]) }}" class="link-ltc">
                                    <figure>
                                        <img src="{{ $offers[$i]->preteur->picture or "/img/photo-profil-vide.png" }}">
                                    </figure>
                                    <p>
                                        {{ $offers[$i]->preteur->first_name }} {{ substr($offers[$i]->preteur->last_name, 0, 1)."." }}
                                    </p>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-8 descriptif-liste-resultat">
                                <div class="row">
                                    <a href="{{ url('user/profil', [$offers[$i]->preteur->id]) }}" class="col-xs-12 col-sm-6 model">{{ $offers[$i]->car->brand }} <span class="bold">{{ $offers[$i]->car->model }}</span> {{ $offers[$i]->color->name }}</a>
                                    <p class="col-xs-12 col-sm-6 location"><span class="bold">Localisation :</span> {{ $offers[$i]->location_name }}</p>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        <p class="text-descriptif">« {{ $offers[$i]->description }} »</p>
                                        @if(count($offers[$i]->options) > 0)
                                            <div class="row liste-option">
                                                <h3 class="col-xs-12 col-sm-4">Options :</h3>
                                                <ul class="col-xs-12 col-sm-8">
                                                    @foreach($offers[$i]->options as $option)
                                                        <li>{{ $option->name }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row">
                                            @foreach($offers[$i]->pictures as $picture)
                                                <a title="Image {{$i}}" class="container-img-lb col-xs-6 col-sm-4">
   <!-- attribut data-imgae à mettre dans une variable, par exemple pour pemière ligne resultat mettre ligne1 sur toute les images de la ligne  -->
                                                    <img class="img-lightbox img-responsive" data-image="image-{{$i}}" src="{{$picture->visuel}}">
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-2 prix-liste-resultat">
                                <a href="{{  url('reservation', [$offers[$i]->id]) }}" class="link-ltc">
                                    <div class="text-prix">
                                        <p class="link-prix"><span>Réservez à</span><span class="prix">{{ $offers[$i]->price }} €</span><span>l'essai</span></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="hidden" id="img-repo">
                            @foreach($offers[$i]->pictures as $picture)
                            <!-- #image-{{$i}} -->
    <!-- repeter la variable ci-dessus et l'ajouter dans class après la classe item, par exemple pour pemière ligne resultat mettre ligne1 sur toute les images de la ligne  -->
                            <div class="item image-{{$i}}">
                                <img class="img-lightbox img-responsive" title="Image 11" src="{{$picture->visuel}}">
                            </div>
                            @endforeach
                        </div>
                    @endfor

                    <div class="modal" id="modal-gallery" role="dialog">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                              <button class="close" type="button" data-dismiss="modal">×</button>
                          </div>
                          <div class="modal-body">
                              <div id="modal-carousel" class="carousel">
                                <div class="carousel-inner">
                                </div>
                                <a class="carousel-control left" href="#modal-carousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                <a class="carousel-control right" href="#modal-carousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            @endif

        </div>

    </section><!-- fin div id="content" -->
@stop

@section('script')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDobYOn7L9E8tAMzqeznRmXPO_IgH-Mcg&callback=initMap">
    </script>

    <script>
        $(document).ready(function ($) {
            // delegate calls to data-toggle="lightbox"
            $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
                event.preventDefault();
                return $(this).ekkoLightbox({
                    onShown: function() {
                        if (window.console) {
                            return console.log('Checking our the events huh?');
                        }
                    },
                    onNavigate: function(direction, itemIndex) {
                        if (window.console) {
                            return console.log('Navigating '+direction+'. Current item: '+itemIndex);
                        }
                    }
                });
            });

            //Programatically call
            $('#open-image').click(function (e) {
                e.preventDefault();
                $(this).ekkoLightbox();
            });
            $('#open-youtube').click(function (e) {
                e.preventDefault();
                $(this).ekkoLightbox();
            });

// navigateTo
            $(document).delegate('*[data-gallery="navigateTo"]', 'click', function(event) {
                event.preventDefault();
                return $(this).ekkoLightbox({
                    onShown: function() {

                        var a = this.modal_content.find('.modal-footer a');
                        if(a.length > 0) {

                            a.click(function(e) {
                                e.preventDefault();
                                this.navigateTo(2);
                            }.bind(this));
                        }
                    }
                });
            });
        });
    </script>

    <script>
        $(function(){
            $('body').addClass('js');
            $('#Container').mixItUp();
        });
        $( document ).ready(function() {
        });
    </script>
    <script>
        function initMap() {
            var myLatLng = {
                lat: parseFloat(document.getElementById('lat').value),
                lng: parseFloat(document.getElementById('lng').value)
            };
            var map = new google.maps.Map(document.getElementById('bandeau-map'), {
                zoom: 12,
                center: myLatLng
            });

            // Create a marker and set its position.
            var image = '/img/marqueur-map.png';
            new google.maps.Marker({
                map: map,
                position: myLatLng,
            });
            <?php foreach ($offers as $offer) : ?>
            var marker = new google.maps.Marker({
                map: map,
                position: {lat: <?php echo $offer->lat();?>, lng: <?php echo $offer->lng();?>},
                icon: image
            });
            var infoWindow = new google.maps.InfoWindow({content:""});
            var contentString = '<div id="content" class="infobulle-map">'+
                            '<div class="img-infobulle">' + '<img src="<?php if ((isset($offer->pictures[0])) && (isset($offer->pictures[0]->visuel))) echo $offer->pictures[0]->visuel; else echo '/img/offers/ferari.jpg';  ?>">' + '</div>'+
                            '<div class="contenu-infobulle">' + '<h2><?php echo $offer->car->model; ?></h2>' + '<p><?php echo $offer->location_name; ?></p>' +
                            '<div class="notation"><span class="cercle-plein"></span><span class="cercle-plein"></span><span class="cercle-plein"></span><span class="cercle-vide"></span><span class="cercle-vide"></span></div>' +
                            '</div>'+
                            '<div class="lien-offre"><a href="#<?php echo $offer->id; ?>" class="link-ltc">Voir cette offre</a></div>'+
                            '</div>';
            BindInfoWindow(marker, map, infoWindow, contentString);
            <?php endforeach; ?>
         }
        function BindInfoWindow(marker, map, infoWindow, description) {
            marker.addListener('click', function () {
                infoWindow.setContent(description);
                infoWindow.open(map, this);
            });
        }
    </script>

@stop