@extends('template')

@section('title')
   Créer un compte - se connecter - Live Test Car
@stop

@section('body')

<!--============================== content =================================-->
<section class="content gabarit">
    <div class="container-fluid creation-compte">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                {!! Form::open(array('route' => 'login.post', 'class' => 'form-creation-compte')) !!}
                <h2 data-icon="K" class="bandeau-titre-noir">Connexion</h2>
                <div class="content-form-creation">
                    @if (count($errors->login) > 0)
                        <p class="message-erreur">
                            @foreach ($errors->login->all() as $error)
                                <span class="txt-underline">{{ $error }}</span><br/>
                            @endforeach
                        </p>
                    @endif
                    <div class="container-login-fb">
                        <a class="login-fb" href="auth/facebook" role="button" data-icon="B"><span>connexion avec facebook</span></a>
                        <p class="separator"><span>ou</span></p>
                    </div>
                    <label for="email">Adresse mail :</label>
                    <input type="email" name="email" class="input-ltc" placeholder="E-mail">
                    <label for="password">Mot de passe :</label>
                    <input type="password" name="password" class="input-ltc" placeholder="******">
                    <input type="submit" value="ok" class="btn-ltc">
                </div>
                {!! Form::close() !!}
            </div>
            <div class="col-xs-12 col-sm-6">
                {!! Form::open(array('route' => 'register.post', 'class' => 'form-creation-compte')) !!}
                    <h2 data-icon="K" class="bandeau-titre-noir">Créer un compte</h2>
                    <div class="content-form-creation">
                        @if (count($errors) > 0)
                            <p class="message-erreur">Votre inscription ne s'est pas effectuée correctement.</p>
                            <p class="message-erreur">Nous avons rencontré {{ count($errors) > 1 ? 'les erreurs suivantes' : 'l\'erreur suivante' }} :</p>
                            <ul>
                                 @foreach ($errors->all() as $error)
                                <li class="message-erreur">{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <label for="email">Adresse mail :</label>
                        <input type="email" name="email" value="{{ old('email') }}" class="input-ltc" placeholder="E-mail">
                        <label for="password">Mot de passe :</label>
                        <input type="password" name="password" class="input-ltc" placeholder="******">
                        <label for="password_confirmation">Confirmation du mot de passe :</label>
                        <input type="password" name="password_confirmation" class="input-ltc" placeholder="******">
                        <input type="submit" value="ok" class="btn-ltc">
                        <p>Pour essayer ou proposer un véhicule vous devez préalablement compléter votre profil</p>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section><!-- fin div id="content" -->
@stop