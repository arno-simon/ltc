<!doctype html>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
<script src="/js/jquery.geocomplete.js"></script>
<link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/livetestcar-theme.css" rel="stylesheet">
<link href="/css/font-livetestcar.css" rel="stylesheet">
<link href="/css/autocomplete.css" rel="stylesheet">
<link href="/css/custom.css" rel="stylesheet">

<div class="frame-ltc frame-classique">
    <div class="container-form-search">
        {!! Form::open(array('route' => 'results', 'class' => 'form-search-index', 'id' => 'form')) !!}
        <h1>Tester une voiture près de chez vous !</h1>
        <label id="localisation-label" for="search-localisation">Localisation (dans un rayon de)</label>
        @if ($errors->has('localisation'))
            <p id="localisation-error" class="message-erreur">
                <span class="txt-underline">{{ $errors->first('localisation') }}</span><br/>
            </p>
        @endif
        @if ($errors->has('lat'))
            <p id="localisation-error" class="message-erreur">
                <span class="txt-underline">{{ $errors->first('lat') }}</span><br/>
            </p>
        @endif
        @if ($errors->has('lng'))
            <p id="localisation-error" class="message-erreur">
                <span class="txt-underline">{{ $errors->first('lng') }}</span><br/>
            </p>
        @endif

        <div class="block-input-select">
            <input id="places" type="text" name="localisation" class="input-ltc" placeholder="Ex: Rennes" value="{{ old('localisation') }}"><select id="rayon" class="select-ltc" name="rayon">
                <option value="30" selected>30 km</option>
                <option value="60">60 km</option>
                <option value="90">90 km</option>
            </select>
        </div>

        <label for="brand">Marque</label>
        @if ($errors->has('brand'))
            <p class="message-erreur">
                <span class="txt-underline">{{ $errors->first('brand') }}</span><br/>
            </p>
        @endif
        <div class="container-brand">
            <input id="brand" class="input-ltc" type="text" name="brand" placeholder="Ex: Mini" value="{{ old('brand') }}">
            <!-- Logo de la marque choisie a rajouter en variable dans la balise image -->
                                <span id="span-logo-brand" style="visibility: hidden" class="logo-brand">
                                    <img id="logo-marque" src="">
                                </span>
        </div>

        <label for="model">Modèle</label>
        @if ($errors->has('model'))
            <p class="message-erreur">
                <span class="txt-underline">{{ $errors->first('model') }}</span><br/>
            </p>
        @endif
        <input id="model" type="text" name="model" class="input-ltc" placeholder="Ex: Cooper S" value="{{ old('model') }}">

        <input id="lat" name="lat" type="hidden" value="" autocomplete="off">
        <input id="lng" name="lng" type="hidden" value="" autocomplete="off">

        <input type="submit" value="ok" class="btn-ltc">
        {!! Form::close() !!}
    </div>
</div>

<script>
    $( document ).ready(function() {
        var $places = $("#places");

        // geocomplete plugin qui va remplir les input lat et lng en fonction du lieu selectionné
        $places.geocomplete({
            details: "form",
            types: ["geocode", "establishment"]
        });


        var $brand = $("#brand");
        var $model = $("#model");

        // constructs the suggestion engine
        var brands = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote:
            {
                url: "search/autocomplete/brand",
                prepare: function (query, settings) {
                    settings.type = "POST";
                    settings.contentType = "application/json; charset=UTF-8";
                    settings.data = JSON.stringify({"brand" : query});

                    return settings;
                }
            }
        });
        var models = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote:
            {
                url: "search/autocomplete/model",
                prepare: function (query, settings) {
                    settings.type = "POST";
                    settings.contentType = "application/json; charset=UTF-8";
                    settings.data = JSON.stringify({"model" : query, "brand":$brand.val()});

                    return settings;
                }
            }
        });

        $brand.typeahead({
                    autoselect: true,
                    hint: true,
                    highlight: false,
                    minLength: "1",
                    classNames: {
                        input: 'tt-input',
                        hint: 'tt-hint',
                        menu: 'tt-menu',
                        suggestion: 'tt-suggestion',
                        cursor: 'tt-cursor'
                    }
                },
                {
                    name: 'brands',
                    source: brands,
                    limit: 10
                }
        )
                .bind('typeahead:select', function(ev, suggestion) {
                    $model.val('');
                    document.getElementById('span-logo-brand').style.visibility="visible";
                    var logo_name = $brand.val().replace(' ', '-');
                    logo_name = logo_name.toLowerCase();
                    $("#logo-marque").attr("src","/img/logo-marque/logo-"+logo_name+".jpg");
                });

        $model.typeahead({
                    autoselect: true,
                    hint: true,
                    highlight: false,
                    minLength: "1",
                    classNames: {
                        input: 'tt-input',
                        hint: 'tt-hint',
                        menu: 'tt-menu',
                        suggestion: 'tt-suggestion',
                        cursor: 'tt-cursor'
                    }
                },
                {
                    name: 'models',
                    source: models,
                    limit: 20
                }
        );
        $('.twitter-typeahead').css('width', '80%');


        $('#brand, #model').on('typeahead:asyncreceive', function(e)
        {
            var firstElem = $(this).data().ttTypeahead.menu.getTopSelectable();
            $(this).data().ttTypeahead.menu.setCursor(firstElem);
        });

        $model.on('typeahead:select', function(e)
        {
            $.ajax({
                method: "POST",
                url: "search/autocomplete/model-brand",
                data: JSON.stringify({ 'model': $model.val()}),
                dataType: "json",
                contentType: "application/json; charset=UTF-8"
            })
                    .done(function( data ) {
                        $brand.val(data.brand);
                    });
        });

        $('#form').submit(function()
        {
            if($('#lat').val() !== '' && $('#lng').val() !== '')
            {
                return true;

            }
            else
            {
                $('#localisation-label').after("<p class='message-erreur'><span class='txt-underline'>Veuillez selectionner un lieu dans les suggestions proposées</span><br/></p>");
                return false;
            }
        });
    });
</script>