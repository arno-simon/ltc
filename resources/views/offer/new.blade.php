@extends('template')

@section('title')
   Proposer ma voiture - Live Test Car
@stop

@section('header')
    <link href="/css/datepicker/jquery.datepick.css" rel="stylesheet">
    <script src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="/js/jquery.geocomplete.js"></script>
    <script src="/js/datepicker/jquery.plugin.js"></script>
    <script src="/js/datepicker/jquery.datepick.js"></script>
    <script src="/js/datepicker/jquery.datepick-fr.js"></script>
@stop                                <!-- TEST -->

    @section('body')
     <!--============================== content =================================-->
   <section class="content gabarit">

       <!--============================== page resultat =================================-->

       <div class="container-fluid page-creer-profil">
           @if(session()->has('status'))
               <p class="message-success">
                   {{ session()->get('status') }}
               </p>
           @endif

            <!--============================== Photo de l'annonce =================================-->
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                @if (isset($offer))
                   <h2 data-icon="-" class="bandeau-titre-noir">Modifier une annnonce</h2>
                @else
                   <h2 data-icon="-" class="bandeau-titre-noir">Créer une annnonce</h2>
                @endif
                    <div class="container-annonce">
                        <div class="row container-photo-annonce">
                        <form method="POST" action="/cropa.php" enctype="multipart/form-data" accept-charset="UTF-8" class="col-xs-12 col-sm-4 form-modification-info" id="rowForm">
                            {!! Form::token() !!}
                            <input type="hidden" name="offer_id" value="@if(isset($offer)){{$offer->id}}@else{{'0'}}@endif">
                            <input type="hidden" name="imageIdx" value="0">
                            <div class="row container-info">
                                <div class="info-enregistre" id="rowPicture">
                                    <div>
                                        <figure>
                                            <img class="photo-view" src="@if(isset($offer)){{$offer->pictures()->where('imageIdx', 0)->first()->visuel or '/img/no_img_exterieur.png'}}@elseif (session()->has('visuel.0')){{session('visuel.0')}} @else {{'/img/no_img_exterieur.png'}}@endif">
                                        </figure>
                                    </div>
                                    <div>
                                        <div id="crop-avatar">
                                            <!-- Current avatar -->
                                            <div class="avatar-view" title="changer ma photo : véhicule extérieur">
                                                <!--<img src="/img/no_img.png" alt="Avatar">-->
                                                <span class="btn-ltc">Modifier ma photo</span>
                                            </div>
                                            <!-- Cropping modal -->
                                            <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="avatar-form">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"><span data-icon="6"></span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="image-rotate">
                                                                    <img src="/img/rotate.png" alt="image rotation telephone" title="image rotation telephone">
                                                                </div>
                                                                <div class="avatar-body">
                                                                    <!-- Upload image and data -->
                                                                    <div class="avatar-upload">
                                                                        <input type="hidden" class="avatar-src" name="avatar_src">
                                                                        <input type="hidden" class="avatar-data" name="avatar_data">
                                                                        <input type="file" class="avatar-input file" id="avatarInput" name="avatar_file" data-show-preview="false">
                                                                    </div>
                                                                    <!-- Crop and preview -->
                                                                    <div class="row">
                                                                        <div class="col-md-9">
                                                                            <div class="avatar-wrapper"></div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="avatar-preview preview-lg"></div>
                                                                            <div class="avatar-preview preview-md"></div>
                                                                            <!--<div class="avatar-preview preview-sm"></div>-->
                                                                        </div>
                                                                    </div>
                                                                    <div class="row avatar-btns">
                                                                        <div class="col-xs-12 ta-c">
                                                                            <button type="button" class="btn btn-primary avatar-rotate" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><span data-icon="b"></span></button>
                                                                            <button type="submit" class="btn btn-primary btn-block avatar-save">Valider</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- /.modal -->

                                            <!-- Loading state -->
                                            <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- FIN TEST -->
                        <form method="POST" action="/cropa.php" enctype="multipart/form-data" accept-charset="UTF-8" class="col-xs-12 col-sm-4 form-modification-info" id="rowForm">
                           {!! Form::token() !!}
                            <input type="hidden" name="offer_id" value="@if(isset($offer)){{$offer->id}}@else{{'1'}}@endif">
                            <input type="hidden" name="imageIdx" value="1">
                           <div class="row container-info">
                               <div class="info-enregistre" id="rowPicture">
                                   <div>
                                       <figure>
                                           <img class="photo-view" src="@if(isset($offer)){{$offer->pictures()->where('imageIdx', 1)->first()->visuel or '/img/no_img_3_4.png'}}@elseif (session()->has('visuel.1')){{session('visuel.1')}} @else {{'/img/no_img_3_4.png'}}@endif">
                                       </figure>
                                   </div>
                                   <div>
                                       <div id="crop-avatar1">
                                           <!-- Current avatar -->
                                           <div class="avatar-view" title="changer ma photo : véhicule extérieur">
                                               <!--<img src="/img/no_img.png" alt="Avatar">-->
                                               <span class="btn-ltc">Modifier ma photo</span>
                                           </div>
                                           <!-- Cropping modal -->
                                           <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                               <div class="modal-dialog modal-lg">
                                                   <div class="modal-content">
                                                       <div class="avatar-form">
                                                           <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal"><span data-icon="6"></span></button>
                                                           </div>
                                                           <div class="modal-body">
                                                               <div class="image-rotate">
                                                                   <img src="/img/rotate.png" alt="image rotation telephone" title="image rotation telephone">
                                                               </div>
                                                               <div class="avatar-body">
                                                                   <!-- Upload image and data -->
                                                                   <div class="avatar-upload">
                                                                       <input type="hidden" class="avatar-src" name="avatar_src">
                                                                       <input type="hidden" class="avatar-data" name="avatar_data">
                                                                       <input type="file" class="avatar-input file" id="avatarInput" name="avatar_file" data-show-preview="false">
                                                                   </div>
                                                                   <!-- Crop and preview -->
                                                                   <div class="row">
                                                                       <div class="col-md-9">
                                                                           <div class="avatar-wrapper"></div>
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                           <div class="avatar-preview preview-lg"></div>
                                                                           <div class="avatar-preview preview-md"></div>
                                                                           <!--<div class="avatar-preview preview-sm"></div>-->
                                                                       </div>
                                                                   </div>
                                                                   <div class="row avatar-btns">
                                                                       <div class="col-xs-12 ta-c">
                                                                           <button type="button" class="btn btn-primary avatar-rotate" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><span data-icon="b"></span></button>
                                                                           <button type="submit" class="btn btn-primary btn-block avatar-save">Valider</button>
                                                                       </div>
                                                                   </div>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div><!-- /.modal -->

                                           <!-- Loading state -->
                                           <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </form>
                       <!-- FIN TEST -->
                       <form method="POST" action="/cropa.php" enctype="multipart/form-data" accept-charset="UTF-8" class="col-xs-12 col-sm-4 form-modification-info" id="rowForm">
                           {!! Form::token() !!}
                           <input type="hidden" name="offer_id" value="@if(isset($offer)){{$offer->id}}@else{{'0'}}@endif">
                           <input type="hidden" name="imageIdx" value="2">
                           <div class="row container-info">
                               <div class="info-enregistre" id="rowPicture">
                                   <div>
                                       <figure>
                                           <img class="photo-view" src="@if(isset($offer)){{$offer->pictures()->where('imageIdx', 2)->first()->visuel or '/img/no_img_interieur.png'}}@elseif (session()->has('visuel.2')){{session('visuel.2')}} @else {{'/img/no_img_interieur.png'}}@endif">
                                       </figure>
                                   </div>
                                   <div>
                                       <div id="crop-avatar2">
                                           <!-- Current avatar -->
                                           <div class="avatar-view" title="changer ma photo : véhicule intérieur">
                                               <!--<img src="/img/no_img.png" alt="Avatar">-->
                                               <span class="btn-ltc">Modifier ma photo</span>
                                           </div>
                                           <!-- Cropping modal -->
                                           <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                               <div class="modal-dialog modal-lg">
                                                   <div class="modal-content">
                                                       <div class="avatar-form">
                                                           <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal"><span data-icon="6"></span></button>
                                                           </div>
                                                           <div class="modal-body">
                                                               <div class="image-rotate">
                                                                   <img src="/img/rotate.png" alt="image rotation telephone" title="image rotation telephone">
                                                               </div>
                                                               <div class="avatar-body">
                                                                   <!-- Upload image and data -->
                                                                   <div class="avatar-upload">
                                                                       <input type="hidden" class="avatar-src" name="avatar_src">
                                                                       <input type="hidden" class="avatar-data" name="avatar_data">
                                                                       <input type="file" class="avatar-input file" id="avatarInput" name="avatar_file" data-show-preview="false">
                                                                   </div>
                                                                   <!-- Crop and preview -->
                                                                   <div class="row">
                                                                       <div class="col-md-9">
                                                                           <div class="avatar-wrapper"></div>
                                                                       </div>
                                                                       <div class="col-md-3">
                                                                           <div class="avatar-preview preview-lg"></div>
                                                                           <div class="avatar-preview preview-md"></div>
                                                                           <!--<div class="avatar-preview preview-sm"></div>-->
                                                                       </div>
                                                                   </div>
                                                                   <div class="row avatar-btns">
                                                                       <div class="col-xs-12 ta-c">
                                                                           <button type="button" class="btn btn-primary avatar-rotate" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><span data-icon="b"></span></button>
                                                                           <button type="submit" class="btn btn-primary btn-block avatar-save">Valider</button>
                                                                       </div>
                                                                   </div>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div><!-- /.modal -->
                                           <!-- Loading state -->
                                           <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </form>
                   </div>
                    </div>{!! Form::open(array('url'=>'offer/new', 'class'=>'form-modification-info', 'accept-charset'=>'UTF-8', 'files'=>true)) !!}

                   <input type="hidden" name="idOffer" value="@if (isset($offer)){{$offer->id}}@endif">
                   <!--============================== Annonce =================================-->
                       <div class="content-annonce">
                       <!--============================== Marque annonce =================================-->
                           <div class="container-annonce row form-modification-info">
                                   <label for="brand" class="col-xs-12 col-sm-4">Marque :</label>
                                   <div class="col-xs-12 col-sm-5">
                                       <input type="text" id="brand" name="brand" class="input-ltc" placeholder="Marque" value="@if (old('brand')){{ old('brand') }}@elseif (isset($offer)){{ $offer->car->brand}}@endif">
                                   </div>
                                   <div class="col-xs-12 col-sm-4">
                                       @if ($errors->has('brand'))
                                           <p class="message-erreur">{{ $errors->first('brand') }}</p>
                                       @endif
                                   </div>
                           </div>
                           <!--============================== Modèle annonce =================================-->
                           <div class="container-annonce row form-modification-info">
                                   <label for="model" class="col-xs-12 col-sm-4">Modèle :</label>
                                   <div class="col-xs-12 col-sm-5">

                                       <input type="text" id="model" name="model" class="input-ltc" placeholder="Modèle"  value="@if (old('model')){{ old('model') }}@elseif (isset($offer)){{$offer->car->model}}@endif">
                                   </div>
                                   <div class="col-xs-12 col-sm-4">
                                       @if ($errors->has('model'))
                                           <p class="message-erreur">{{ $errors->first('model') }}</p>
                                       @endif
                                   </div>
                           </div>
                           <!--============================== Année modèle annonce =================================-->
                           <div class="container-annonce row form-modification-info">
                               <label for="annee" class="col-xs-12 col-sm-4">Année :</label>
                               <div class="col-xs-12 col-sm-5">
                                   <input type="number" name="annee" class="input-ltc" placeholder="Année"  value="@if (old('annee')){{ old('annee') }}@elseif (isset($offer)){{$offer->annee}}@endif">
                               </div>
                               <div class="col-xs-12 col-sm-4">
                                   @if ($errors->has('annee'))
                                       <p class="message-erreur">{{ $errors->first('annee') }}</p>
                                   @endif
                               </div>
                           </div>
                           <!--============================== Catégorie de la voiture =================================-->
                           <div class="container-annonce row form-modification-info">
                               {!! Form::label('categorie', 'Catégorie :', array('class' => 'col-xs-4')); !!}
                               <div class="col-xs-12 col-sm-8">
                                    <div class="row container-categorie">
                                        <div class="col-xs-6 col-md-4">
                                            {!! Form::checkbox('categorie[]', 'monospace', false, array('class'=>'')) !!}Monospace
                                        </div>
                                        <div class="col-xs-6 col-md-4">
                                            {!! Form::checkbox('categorie[]', 'break', false, array('class'=>'')) !!}Break
                                        </div>
                                        <div class="col-xs-6 col-md-4">
                                            {!! Form::checkbox('categorie[]', 'sportive', false, array('class'=>'')) !!}sportive
                                        </div>
                                        <div class="col-xs-6 col-md-4">
                                            {!! Form::checkbox('categorie[]', 'berline', false, array('class'=>'')) !!}Berline
                                        </div>
                                        <div class="col-xs-6 col-md-4">
                                            {!! Form::checkbox('categorie[]', 'citadine', false, array('class'=>'')) !!}Citadine
                                        </div>
                                    </div>
                               </div>
                           </div>
                           <!--============================== Description annonce =================================-->
                           <div class="container-annonce row form-modification-info">

                               <label for="description" class="col-xs-12 col-sm-4">Description :</label>
                               <div class="col-xs-12 col-sm-5">
                                   <textarea class="input-ltc" rows="5" name="description" placeholder="Descripton de l'annonce">@if (old('description')){{ old('description') }}@elseif (isset($offer)){{$offer->description}}@endif</textarea>
                               </div>
                               <div class="col-xs-12 col-sm-4">
                                   @if ($errors->has('description'))
                                       <p class="message-erreur">{{ $errors->first('description') }}</p>
                                   @endif
                               </div>
                           </div>
                           <!--============================== Carburant annonce =================================-->
                           <div class="container-annonce row form-modification-info">
                              <label for="fuel" class="col-xs-12 col-sm-4">Carburant :</label>
                              <div class="col-xs-12 col-sm-5">
                                       <select class="select-ltc" name="fuel">
                                           <option value="" selected>Choisissez...</option>
                                           @foreach($fuels as $fuel)
                                               <option value="{{ $fuel->id }}" @if(old('fuel') == $fuel->id) {{ 'selected' }} @elseif ((isset($offer)) && ($offer->fuel->id == $fuel->id)) {{'selected'}} @endif>{{ $fuel->name }}</option>
                                           @endforeach
                                       </select>
                                   </div>
                                   <div class="col-xs-12 col-sm-4">
                                       @if ($errors->has('fuel'))
                                           <p class="message-erreur">{{ $errors->first('fuel') }}</p>
                                       @endif
                                   </div>
                           </div>
                           <!--============================== Boite de vitesse annonce =================================-->
                           <div class="container-annonce row form-modification-info">
                                   <label for="gearbox" class="col-xs-12 col-sm-4">Boite de vitesse :</label>
                                   <div class="col-xs-12 col-sm-5">
                                       <select class="select-ltc" name="gearbox">
                                           <option value="" selected>Choisissez...</option>
                                           @foreach($gearboxes as $gearbox)
                                               <option value="{{ $gearbox->id }}" @if(old('gearbox') == $gearbox->id) {{ 'selected' }}@elseif ((isset($offer)) && ($offer->gearbox->id == $gearbox->id)) {{'selected'}}@endif>{{ $gearbox->name }}</option>
                                           @endforeach
                                       </select>
                                   </div>
                                   <div class="col-xs-12 col-sm-4">
                                       @if ($errors->has('gearbox'))
                                           <p class="message-erreur">{{ $errors->first('gearbox') }} </p>
                                       @endif
                                   </div>
                           </div>
                           <!--============================== Options véhicule annonce =================================-->
                               @if(old('options'))
                                   @foreach(old('options') as $value)
                                   <div class="container-annonce row form-modification-info row-option">
                                           <label for="options" class="col-xs-12 col-sm-4">@if(head(old('options')) == $value) {{ "Options :" }}@endif</label>
                                           <div class="col-xs-12 col-sm-5">
                                               <select class="select-ltc" name="options[]">
                                                   <option value="" selected>Choisissez...</option>
                                                   @foreach($options as $option)
                                                       <option value="{{ $option->id }}" @if($value == $option->id) {{'selected'}}@endif>{{ $option->name }}</option>
                                                   @endforeach
                                               </select>
                                           </div>
                                   </div>
                                   @endforeach

                                   @if(count(old('options')) < 3)
                                    <div class="container-annonce row form-modification-info row-option" id="row-add">
                                           <div for="options" class="col-xs-12 col-sm-4"></div>
                                           <div class="col-xs-12 col-sm-5">
                                               <a id="add-option" href="#" onclick="return false">Ajouter une option</a>
                                           </div>
                                   </div>
                                   @endif
                               @elseif (isset($offer))
                                   @foreach($offer->options as $value)
                                       <div class="container-annonce row form-modification-info row-option">
                                           <label for="options" class="col-xs-12 col-sm-4">@if(head($offer->options) == $value) {{ "Options :" }}@endif</label>
                                           <div class="col-xs-12 col-sm-5">
                                               <select class="select-ltc" name="options[]">
                                                   <option value="" selected>Choisissez...</option>
                                                   @foreach($options as $option)
                                                       <option value="{{ $option->id }}" @if($value->id == $option->id) {{ 'selected' }}@endif>{{ $option->name }}</option>
                                                   @endforeach
                                               </select>
                                           </div>
                                       </div>
                                   @endforeach
                               @else
                                   <div class="container-annonce row form-modification-info row-option">
                                       <label for="options" class="col-xs-12 col-sm-4">Options :</label>
                                           <div class="col-xs-12 col-sm-5">
                                               <select class="select-ltc" name="options[]">
                                                   <option value="" selected>Choisissez...</option>
                                                   @foreach($options as $option)
                                                       <option value="{{ $option->id }}">{{ $option->name }}</option>
                                                   @endforeach
                                               </select>
                                               <div>
                                                   <a href="#" id="remove-option" onclick="return false">Remove</a>
                                               </div>
                                           </div>
                                   </div>
                               @endif

                               <div class="container-annonce row form-modification-info" id="row-add">
                                       <div class="col-xs-12 col-sm-4"></div>
                                       <div class="col-xs-12 col-sm-5">
                                           <a id="add-option" href="#" onclick="return false" class="btn-link-ltc">Ajouter une option</a>
                                       </div>
                               </div>
                           <!--============================== Couleur annonce =================================-->
                            <div class="container-annonce row form-modification-info">
                                   <label for="color" class="col-xs-12 col-sm-4">Couleur :</label>
                                   <div class="col-xs-12 col-sm-5">
                                       <select class="select-ltc" name="color">
                                           <option value="" selected>Choisissez...</option>
                                           @foreach($colors as $color)
                                               <option value="{{ $color->id }}" @if(old('color') == $color->id) {{ 'selected' }} @elseif ((isset($offer)) && ($offer->color->id == $color->id)) {{'selected'}} @endif>{{ $color->name }}</option>
                                           @endforeach
                                       </select>
                                   </div>
                           </div>
                           <!--============================== Tarif de l'essai annonce =================================-->
                            <div class="container-annonce row form-modification-info">
                                   <label for="price" class="col-xs-12 col-sm-4">Tarif de l'essai :</label>
                                   <div class="col-xs-12 col-sm-8">
                                       <div class="row">
                                           <div class="col-xs-6 col-sm-4">
                                               <input type="number" name="price" class="input-ltc" value="@if (old('price')){{ old('price') }}@elseif (isset($offer)){{$offer->price}}@endif">
                                           </div>
                                           <div class="col-xs-1 tarif">
                                               <p class="separateur-select">€</p>
                                           </div>
                                           <div class="col-xs-12 col-sm-5">
                                               @if ($errors->has('price'))
                                                   <p class="message-erreur">{{ $errors->first('price') }} </p>
                                               @endif
                                            </div>
                                       </div>
                                   </div>
                           </div>
                           <!--============================== Disponibilite annonce =================================-->
                            <div class="container-annonce row form-modification-info">
                               <label for="hours" class="col-xs-12 col-sm-4">Disponibilités :</label>
                               <div class="col-xs-12 col-sm-5">
                                   <div id="selectDate"></div>
                                   <input type="hidden" id="dateSelected" name="dateSelected">
                                   <div class="row select-horaire">
                                       <div class="col-xs-5">
                                           <select class="select-ltc" name="hour-from">
                                               <option value=""></option>
                                               @for($i=0; $i<24; $i++)
                                                   <option value="{{$i}}" @if(old('hour-from') == $i) {{'selected'}}@elseif (isset($offer) && $offer->timeFrom() == $i) {{ 'selected' }}@endif>{{$i}} H</option>
                                               @endfor
                                           </select>
                                       </div>
                                       <div class="col-xs-2">
                                           <p class="separateur-select">à</p>
                                       </div>
                                       <div class="col-xs-5">
                                           <select class="select-ltc" name="hour-to">
                                               <option value="" ></option>
                                               @for($i=0; $i<24; $i++)
                                                   <option value="{{$i}}" @if(old('hour-to') == $i) {{'selected'}}@elseif (isset($offer) && $offer->timeTo() == $i) {{ 'selected' }}@endif>{{$i}} H</option>
                                               @endfor
                                           </select>
                                       </div>
                                       </div>
                                   </div>
                           </div>
                           <!--============================== Localisation annonce =================================-->
                           <div class="container-annonce row form-modification-info">
                               <label for="localisation" class="col-xs-12 col-sm-4">Localisation :</label>
                               <div class="col-xs-12 col-sm-5">
                                   <input id="lat" name="lat" type="hidden" value="@if (old('lat')){{old('lat')}}@elseif (isset($offer)){{$offer->lat()}}@endif" autocomplete="off">
                                   <input id="lng" name="lng" type="hidden" value="@if (old('lng')){{old('lng')}}@elseif (isset($offer)){{$offer->lng()}}@endif" autocomplete="off">
                                   <input id="places" type="text" name="localisation" class="input-ltc" placeholder="Ex: Rennes" value="@if (old('localisation')) {{ old('localisation') }}@elseif (isset($offer)){{$offer->location_name}}@endif">
                               </div>
                               <div id="localisation-error" class="col-xs-12 col-sm-4">
                                   @if ($errors->has('localisation'))
                                       <p class="message-erreur">{{ $errors->first('localisation') }}</p>
                                   @endif
                                   @if ($errors->has('lng'))
                                       <p class="message-erreur">{{ $errors->first('lng') }}</p>
                                   @endif
                                   @if ($errors->has('lat'))
                                       <p class="message-erreur">{{ $errors->first('lat') }}</p>
                                   @endif
                               </div>
                           </div>
                           <div class="container-annonce row form-modification-info">
                               {!! Form::label('paused', 'Invisible :', array('class' => 'col-xs-4')); !!}
                               <div class="col-xs-8">
                               @if (isset($offer))
                                   {!! Form::checkbox('paused', 'yes', $offer->paused, array('class'=>'')) !!}
                               @else
                                   {!! Form::checkbox('paused', 'yes', old('paused'), array('class'=>'')) !!}
                               @endif
                               </div>
                           </div>
                           <div class="container-annonce row form-modification-info">
                               <div class="col-xs-12 col-sm-4 pull-right">
                                   <input type="submit" value="Enregistrer" id="submit" class="btn-ltc pull-right">
                               </div>
                           </div>
                       </div><!-- Fin content annonce -->

               {!! Form::close() !!}
               </div>
           </div>
       </div>

   </section><!-- fin div id="content" -->
@stop

@section('script')
    <script src="/crop/cropper.js"></script>
    <script src="/crop/main.js"></script>
    <script>
      // initialise file input
        /*$("#input-id").fileinput();
        var btnCust = ''; 
        $(".avatar1").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src="/img/no_img.png" alt="Votre Photo">',
            layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
        $(".avatar2").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src="/img/no_img.png" alt="Votre Photo">',
            layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
        $(".avatar3").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src="/img/no_img.png" alt="Votre Photo">',
            layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });*/

        function showDate(date)
        {
            var dates = $('#selectDate').datepick('getDate');
            var value = '';
            for (var i = 0; i < dates.length; i++) {
                value += (i == 0 ? '' : ',') + $.datepick.formatDate(dates[i]);
            }
            $('#dateSelected').val(value || 'none');
//            alert('The date chosen is ' + $('#dateSelected').val());
        }
        $( document ).ready(function() {
            $('#selectDate').datepick({monthsToShow:1,multiSelect:999, onSelect: showDate});
            <?php if (isset($offer)) : ?>
            var dateDispo = [
            <?php foreach ($offer->disponibilites as $disponibilite) : ?>
                <?php echo '"'.$disponibilite->dateIso().'",'; ?>
            <?php endforeach; ?>
                ];
            $('#selectDate').datepick('setDate', dateDispo);
            <?php endif; ?>
            var $places = $("#places");
            var $brand = $("#brand");
            var $model = $("#model");

            // geocomplete plugin qui va remplir les input lat et lng en fonction du lieu selectionné
            $places.geocomplete({
                details: "form",
                types: ["geocode", "establishment"]
            });

            var brands = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote:
                {
                    url: "/search/autocomplete/brand",
                    prepare: function (query, settings) {
                        settings.type = "POST";
                        settings.contentType = "application/json; charset=UTF-8";
                        settings.data = JSON.stringify({"brand" : query});

                        return settings;
                    }
                }
            });

            var models = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote:
                {
                    url: "/search/autocomplete/model",
                    prepare: function (query, settings) {
                        settings.type = "POST";
                        settings.contentType = "application/json; charset=UTF-8";
                        settings.data = JSON.stringify({"model" : query, "brand":$brand.val()});

                        return settings;
                    }
                }
            });

            $brand.typeahead({
                        autoselect: true,
                        hint: true,
                        highlight: false,
                        minLength: "1",
                        classNames: {
                            input: 'tt-input',
                            hint: 'tt-hint',
                            menu: 'tt-menu',
                            suggestion: 'tt-suggestion',
                            cursor: 'tt-cursor'
                        }
                    },
                    {
                        name: 'brands',
                        source: brands,
                        limit: 10
                    }
            );

            $model.typeahead({
                        autoselect: true,
                        hint: true,
                        highlight: false,
                        minLength: "1",
                        classNames: {
                            input: 'tt-input',
                            hint: 'tt-hint',
                            menu: 'tt-menu',
                            suggestion: 'tt-suggestion',
                            cursor: 'tt-cursor'
                        }
                    },
                    {
                        name: 'models',
                        source: models,
                        limit: 20
                    }
            );

            $('#brand, #model').on('typeahead:asyncreceive', function(e)
            {
                var firstElem = $(this).data().ttTypeahead.menu.getTopSelectable();
                $(this).data().ttTypeahead.menu.setCursor(firstElem);
            });

            var nbOption = $('.row-option').length;
            $('#add-option').on('click', function()
            {
                $rowAdd = $('#row-add');

                if(nbOption < 3)
                {
                    $container = $('#container-option');
                    $('.row-option').first().clone(true).val('').insertBefore($rowAdd).find('label').html('');
                    nbOption++;
                }
                if(nbOption == 3) $rowAdd.remove();
            });

            $('#remove-option').on('click', function()
            {
                if (nbOption > 1)
                    $(this).parent().parent().remove();
                nbOption--;
//                $option.remove();
            });

            $('#form').submit(function()
            {
                if($('#lat').val() !== '' && $('#lng').val() !== '')
                {
                  return true;
                }
                else
                {
                  $('#localisation-error').html("<p class='message-erreur'><span class='txt-underline'>Veuillez selectionner un lieu dans les suggestions proposées</span><br/></p>");
                  return false;
                }
            });
        });
    </script>
@stop