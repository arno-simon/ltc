@extends('template')

@section('title')
   Proposer ma voiture - Live Test Car
@stop

@section('body')

<!--============================== content =================================-->
<section class="content gabarit">
       <!--============================== création annonce succès =================================-->
       <div class="container-fluid creation-compte-success">
           <div class="row">
               <div class="col-sm-3"></div>
               <div class="col-xs-12 col-sm-6">
                   <h1 data-icon="K" class="bandeau-titre-noir">Créer une annonce</h1>
                   <div class="content-creation-success">
                       <h2 class="message-success">Votre annonce a été enregistrée avec succès</h2>
                       <p>Votre annonce est en attente de validation. Un e-mail de confirmation vous sera envoyé lorsque votre annonce sera publiée.</p>
                       <a href="/" class="link-ltc">Retourner à la page d'accueil</a>
                   </div>
               </div>
               <div class="col-sm-3"></div>
           </div>
       </div>
</section><!-- fin div id="content" -->
@stop