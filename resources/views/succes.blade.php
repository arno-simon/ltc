@extends('template')

@section('title')
   Confirmation - Live Test Car
@stop

@section('body')
<section class="content gabarit">

        <!--============================== page succes =================================-->

    <div class="container-fluid page-succes">
        <h1>Confirmation réservation</h1>
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h2>Félicitations,</h2>
                <p>Vous venez d'effectuer la reservation suivante&nbsp;:</p>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="container-fluid">
                    <div class="row container-reservation">
                        <div class="col-xs-12 col-sm-4 col-md-2 photo">
                            <a href="#">
                                <figure>
                                    <img src="/img/users/36.JPG">
                                </figure>
                                <p>NOM PRETEUR</p>
                            </a>
                        </div>
                        <div class="col-xs-10 col-xs-offset-1 separate"></div>
                        <div class="col-xs-12 col-sm-8 col-md-10 vehicule">
                            <div class="nom-vehicule">
                               <p><span>MARQUE </span><span>NOM DU VEHICULE</span></p>
                            </div>
                            <figure class="photo-vehicule">
                                <img src="/img/offers/mini-j1.jpeg">
                            </figure><div class="option">
                                <p><strong>Localisation : </strong>LOCALISATION</p>
                                <ul>
                                    <span><strong>Options : </strong></span>
                                    <li>Vitres électriques</li>
                                    <li>Climatisation</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 detail-transaction">
                <h3>Détail de votre réservation</h3>
                <ul>
                    <li><strong>Date de test choisie : </strong>DATE</li>
                    <li><strong>Montant : </strong>MONTANT</li>
                    <li><strong>Type de paiement : </strong>TYPE DE PAIEMENT</li>
                </ul>
                <div class="information">
                    <p>Vous allez recevoir un mail vous permettant de rentrer en relation avec NOM DU PRETEUR.</p>
                    <p>Des échanges seront possible entre vous jusqu'à la fin du test et la validation de celui-ci qui s'effectuera lorsque vous aurez répondu au formulaire de notation de fin de test. Vous recevrez ce formulaire par e-mail après le test.</p>
                </div>
            </div>
        </div>
    </div>

 </section><!-- fin div id="content" -->
@stop