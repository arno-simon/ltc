<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Parrainages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on profil page.
    |
    */
    'filleul_attente' => '{0} Aucun filleul en attente|{1} Vous avez un filleul en attente|[2,Inf] Vous avez :nombre filleuls en attente',
    'filleul_inscrit' => '{0} Aucun filleul inscrit|{1} Vous avez un filleul inscrit|[2,Inf] Vous avez :nombre filleuls inscrits',
];
