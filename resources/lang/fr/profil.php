<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Profil Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on profil page.
    |
    */
    'vehicle' => 'véhicule proposé|véhicules proposés',
    'test_vehicle' => 'véhicule testé|véhicules testés',
    'test' => 'test|tests',
    'done' => 'effectué|effectués',
    'comment' => 'commentaire sur le profil|commentaires sur le profil',
    'lend' => 'prêt|prêts',
    'avis_positif' => 'avis positif|avis positifs',
    'positif' => 'positif|positifs',
];
