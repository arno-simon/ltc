<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{

	protected $table = 'cars';
	public $timestamps = false;
	protected $fillable = array('brand', 'model', 'year');

	public function countries()
	{
		return $this->belongsToMany('App\Models\Country');
	}
}