<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gearbox extends Model {

	protected $table = 'gearbox';
	public $timestamps = false;
	protected $fillable = array('name');

}