<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $fillable = array('id','texte');
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
//    protected $dateFormat = "c";
    public $timestamps = true;

    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
/*    public function newCollection(array $models = [])
    {
        return new CustomCollection($models);
    }
*/
    public function expediteur()
    {
        return $this->belongsTo('App\Models\User', 'expediteur_id');
    }

    public function destinataire()
    {
        return $this->belongsTo('App\Models\User', 'destinataire_id');
    }

    public function reservation()
    {
        return $this->belongsTo('App\Models\Reservation', 'reservation_id');
    }
}
