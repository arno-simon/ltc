<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $casts = [
        'imageIdx' => 'integer',
    ];
    protected $fillable = array('offer_id','visuel');

    public function offer()
    {
        return $this->belongsTo('App\Models\Offer', 'offer_id');
    }

}
