<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recommandation extends Model
{
    protected $fillable = array('id','commentaire');
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
   // protected $dateFormat = "c";

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function offer()
    {
        return $this->belongsTo('App\Models\Offer', 'offer_id');
    }

    public function note()
    {
        $score = 0;
        $score += $this->attributes['ambiance'];
        $score += $this->attributes['duree'];
        $score += $this->attributes['explications'];
        $score += $this->attributes['accueil'];
        return (int)$score/4;
    }
}