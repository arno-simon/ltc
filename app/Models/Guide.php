<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guide extends Model
{
    protected $table = 'guides';
    public $timestamps = true;
//    protected $fillable = array('id', 'test_date', 'description', 'location', 'location_name', 'price', 'annee', 'hour_from', 'hour_to', 'car_id', 'picture', 'color_id');

    public function resume()
    {
        return substr($this->contenu, 0, 50);
    }
}