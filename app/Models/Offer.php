<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Offer extends Model
{
	protected $table = 'offers';
	public $timestamps = true;
	protected $fillable = array('id', 'test_date', 'description', 'location', 'location_name', 'price', 'annee', 'hour_from', 'hour_to', 'car_id', 'picture', 'color_id','paused');

	public function recommandations()
	{
		return $this->hasMany('App\Models\Recommandation', 'offer_id');
	}

	public function disponibilites()
	{
		return $this->hasMany('App\Models\Disponibilite', 'offer_id');
	}

	public function pictures()
	{
		return $this->hasMany('App\Models\Picture', 'offer_id');
	}

	public function localisation()
	{
		return $this->belongsTo('App\Models\Localisation');
	}

	public function options()
	{
		return $this->belongsToMany('App\Models\Option');
	}

	public function car()
	{
		return $this->belongsTo('App\Models\Car', 'car_id');
	}

	public function preteur()
	{
		return $this->belongsTo('App\Models\User', 'preteur_id');
	}

	public function fuel()
	{
		return $this->belongsTo('App\Models\Fuel');
	}

	public function gearbox()
	{
		return $this->belongsTo('App\Models\Gearbox');
	}

	public function color()
	{
		return $this->belongsTo('App\Models\Color');
	}

	public function setLocation($lng, $lat)
	{
		$lng -= 0.03;
		$lng += rand(0, 1200)/10000;

		$lat -= 0.015;
		$lat += rand(0, 600)/10000;

		$this->attributes['location'] = DB::raw("POINT($lat,$lng)");
	}

	/**
	 *
	 * @return mixed
	 */
	public function lat()
	{
		$query = "SELECT X(location) AS X FROM offers where id=".$this->id;
		$result = \DB::select($query);
		return $result[0]->X;
	}

	public function lng()
	{
		$query = "SELECT Y(location) AS Y FROM offers where id=".$this->id;
		$result = \DB::select($query);
		return $result[0]->Y;
	}

	public function note()
	{
		if (count($this->recommandations) == 0)
			return 0;
		$score = 0;
		foreach($this->recommandations as $recommandation)
		{
			$score += $recommandation->note();
		}
		return (int)$score/count($this->recommandations);
	}

	public function timeFrom()
	{
		if (isset($this->disponibilites[0]))
			return date("G", strtotime($this->disponibilites[0]->timeFrom));
		else
			return null;
	}

	public function timeTo()
	{
		if (isset($this->disponibilites[0]))
			return date("G", strtotime($this->disponibilites[0]->timeTo));
		else
			return null;
	}
}