<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'reservation_at'];
    public $timestamps = true;
//    protected $fillable = array('id', 'test_date', 'description', 'location', 'location_name', 'price', 'annee', 'hour_from', 'hour_to', 'car_id', 'picture', 'color_id');

    public function offer()
    {
        return $this->belongsTo('App\Models\Offer', 'offer_id');
    }

    public function testeur()
    {
        return $this->belongsTo('App\Models\User', 'testeur_id');
    }

    public function preteur()
    {
        return $this->belongsTo('App\Models\User', 'preteur_id');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\conversation');
    }

    public function retrieveMessages()
    {
        $messages = $this->messages;
        if (\Auth::user()->id == $this->testeur_id)
            $messages->pull('0');
        else
            $messages->pull('1');
        return $messages;
    }

    /**
     * @todo: a faire optimus lib
     * @return int
     */
    public function encodedId()
    {
        return $this->id;
    }
}