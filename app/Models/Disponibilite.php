<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Disponibilite extends Model
{
    protected $fillable = array('offer_id','date','timeFrom','timeTo');

    public function offer()
    {
        return $this->belongsTo('App\Models\Offer', 'offer_id');
    }

    public static function explodeRequest($compactDates)
    {
        return explode(',', $compactDates);
    }

    public function dateIso()
    {
//        print_r($this->date);
        return date("d/m/Y", strtotime($this->date));
    }

    public function mois()
    {
        return date("m", strtotime($this->date));
//        return new DateTime($this->date);
    }

    public function jour()
    {
        return date("d", strtotime($this->date));
    }
}
