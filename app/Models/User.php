<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
	use Authenticatable, CanResetPassword;

	protected $table = 'users';
	public $timestamps = true;
	protected $fillable = array('last_name', 'first_name', 'email', 'phone', 'register_ip', 'isPreteur', 'password',
		'confirmation_code', 'isMailConfirmed', 'region', 'ville', 'cp', 'picture', 'favourite_brands', 'isEvent');
	protected $guarded = ['id','password', 'remember_token'];

	public function roles()
	{
		return $this->belongsToMany('App\Models\Role');
	}

	public function offers()
	{
		return $this->hasMany('App\Models\Offer', 'preteur_id');
	}

	/**
	 * @todo �a marche ????? C'est utilis� ?
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function tests()
	{
		return $this->hasMany('App\Models\User', 'testeur_id');
	}

	public function conversations()
	{
		return $this->hasMany('App\Models\Conversation', 'destinataire_id');
	}

	public function reservations()
	{
		return $this->hasMany('App\Models\Reservation', 'testeur_id');
	}

	public function prets()
	{
		return $this->hasMany('App\Models\Reservation', 'preteur_id');
	}

	public function recommandations()
	{
		return $this->hasManyThrough('App\Models\Recommandation', 'App\Models\Offer', 'preteur_id', 'offer_id');
	}

	public function filleuls()
	{
		return $this->hasMany('App\Models\Parrainage', 'parrain_id');
	}
	/**
	 *
	 * Get formatted name as attribute
	 * @param $value
	 * @return string
	 */
	public function getNameAttribute($value)
	{
		return $this->attributes['first_name'].' '.strtoupper(substr($this->attributes['last_name'], 0, 1)).'.';
	}

	public function note()
	{
		$result = DB::select('select (SUM(`ambiance`)+SUM(`duree`)+SUM(`explications`)+SUM(`accueil`)) AS total from `recommandations` inner join `offers` on `offers`.`id` = `recommandations`.`offer_id` where `offers`.`preteur_id` = :id', ['id'=>$this->id]);
		return (int)($result[0]->total/(count($this->recommandations)*4));
	}

    public function nbFilleulsAttentes()
    {
        return \App\Models\Parrainage::where('parrain_id', $this->id)
            ->where('statut', 'envoye')->count();
    }

    public function nbFilleulsInscrits()
    {
        return \App\Models\Parrainage::where('parrain_id', $this->id)
            ->where('statut', 'inscrit')->count();
    }

	public function filleulPicture($i)
    {
//		var_dump($this->filleuls[0]);
//		echo 'le statut :'.$this->filleuls[0]->statut;
//		exit;
		if ($this->filleuls->has($i))
        {
            if ($this->filleuls[$i]->filleul)
                return $this->filleuls[$i]->filleul->picture;
            else
                return '/img/no_img_profil.png';
        }
		else
        {
            return null;
        }
	}

	/**
	 * Recherche le nombre d'avis positifs
	 */
	public function avisPositifs()
	{
		$result = DB::select('SELECT count(*) AS avisPositifs from `recommandations` inner join `offers` on `offers`.`id` = `recommandations`.`offer_id` where `offers`.`preteur_id` = :id AND (accueil+duree+explications+ambiance)/4 >= 2.5', ['id'=>$this->id]);
		return $result[0]->avisPositifs;
	}

	/**
	 * Recherche le nombre de conversations non lues
	 * Warning : read is a reserved keywords of MySQL
	 */
	public function nbUnreadConversations()
	{
		$result = DB::select('SELECT count(*) as unread FROM conversations WHERE destinataire_id=:id AND conversations.read=false', ['id'=>$this->id]);
		return $result[0]->unread;
	}

	public function unreadConversations()
	{
		$conversations = Conversation::where('destinataire_id', $this->id)
							->where('conversations.read', false)->get();
		return $conversations;
	}
}