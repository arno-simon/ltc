<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model {

	protected $table = 'options';
	public $timestamps = false;
	protected $fillable = array('id', 'name');

	public function offers()
	{
		return $this->belongsToMany('App\Models\Offer');
	}

}