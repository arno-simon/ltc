<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Parrainage extends Model
{
    protected $table = 'parrainages';
    protected $fillable = ['parrain_id', 'mail', 'relation', 'message', 'parrain_code'];

    public $timestamps = true;

    public function parrain()
    {
        return $this->belongsTo('App\Models\User', 'parrain_id');
    }

    public function filleul()
    {
        return $this->belongsTo('App\Models\User', 'filleul_id');
    }
}