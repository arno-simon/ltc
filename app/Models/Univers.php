<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Univers extends Model
{
    public $timestamps = false;
    protected $fillable = array('titre', 'description', 'background', 'identifiant','texteBas','visuelBas');

    public static function uploadUniversImage(UploadedFile $file)
    {
        $location = '/img/univers/';
        $fileName = $file->getClientOriginalName();
        $file->move(public_path().$location, $fileName);
        return $location.$fileName;
    }
}
