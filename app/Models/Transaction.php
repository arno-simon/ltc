<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';
    public $timestamps = true;

//    protected $fillable = array('offer_id','visuel');

    public function debiteur()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function crediteur()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
