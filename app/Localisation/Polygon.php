<?php
namespace App\Localisation;

use App\Localisation\Place as Place;

class Polygon
{
    private $places;

    public function __construct(array $places)
    {
        $this->places = $places;
    }

    /**
     * @return Polygon SQL string
     */
    public function toSqlPolygon()
    {
        $c1 = head($this->places);
        $string = "POLYGON((";
        foreach($this->places as $place)
        {
            $string .= $place->getLongitude()." ".$place->getLatitude().",";
        }
        $string .= $c1->getLongitude()." ".$c1->getLatitude()."))";

        return $string;
    }
}