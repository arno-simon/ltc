<?php namespace App\Repositories;

use App\Models\Conversation;
use DB;

class ConversationRepository extends ResourceRepository
{
    public function __construct(Conversation $conversation)
    {
        $this->model = $conversation;
    }

    /**
     * Search a brand
     *
     * @param $term : brand to search
     * @param null $limit : number of results returned
     * @return array|static[]
     */
    public function searchBrands($term, $limit = null)
    {
        return DB::table('cars')
            ->select('brand')
            ->distinct()
            ->where('brand', 'LIKE',  '%'.$term.'%')
            ->take($limit)->get();
    }
}