<?php

namespace App\Repositories;

use App\Models\Guide;
use DB;

class GuideRepository extends ResourceRepository
{
    public function __construct(Guide $guide)
    {
        $this->model = $guide;
    }

    /**
     * Search a brand
     *
     * @param $term : brand to search
     * @param null $limit : number of results returned
     * @return array|static[]
     */
    public function searchBrands($term, $limit = null)
    {
        return DB::table('cars')
            ->select('brand')
            ->distinct()
            ->where('brand', 'LIKE',  '%'.$term.'%')
            ->take($limit)->get();
    }

    /**
     * Search a model's brand
     *
     * @param $term : model to search
     * @param $brand : model's brand
     * @param null $limit : number of results returned
     * @return array|static[]
     */
    public function searchModelsFromBrands($term, $brand, $limit = null)
    {
        return DB::table('cars')
            ->select('model')
            ->distinct()
            ->where('brand', $brand)
            ->where('model', 'LIKE',  '%'.$term.'%')
            ->take($limit)->get();
    }

    /**
     * Search a model
     *
     * @param $term : model to research
     * @param null $limit : number of results returned
     * @return array|static[]
     */
    public function searchModels($term, $limit = null)
    {
        return DB::table('cars')
            ->select('model')
            ->distinct()
            ->where('model', 'LIKE',  '%'.$term.'%')
            ->take($limit)->get();
    }
}
