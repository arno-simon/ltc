<?php namespace App\Repositories;

use App\Models\Car;
use DB;

class CarRepository extends ResourceRepository
{
    public function __construct(Car $car)
    {
        $this->model = $car;
    }

    /**
     * Search a brand
     *
     * @param $term : brand to search
     * @param null $limit : number of results returned
     * @return array|static[]
     */
    public function searchBrands($term, $limit = null)
    {
        return DB::table('cars')
            ->select('brand')
            ->distinct()
            ->where('brand', 'LIKE',  '%'.$term.'%')
            ->take($limit)->get();
    }

    /**
     * Search a model's brand
     *
     * @param $term : model to search
     * @param $brand : model's brand
     * @param null $limit : number of results returned
     * @return array|static[]
     */
    public function searchModelsFromBrands($term, $brand, $limit = null)
    {
        return DB::table('cars')
            ->select('model')
            ->distinct()
            ->where('brand', $brand)
            ->where('model', 'LIKE',  '%'.$term.'%')
            ->take($limit)->get();
    }

    /**
     * Search a model
     *
     * @param $term : model to research
     * @param null $limit : number of results returned
     * @return array|static[]
     */
    public function searchModels($term, $limit = null)
    {
        return DB::table('cars')
            ->select('model')
            ->distinct()
            ->where('model', 'LIKE',  '%'.$term.'%')
            ->take($limit)->get();
    }

    /**
     * Get brand from model
     *
     * @param $model
     * @return mixed|static
     */
    public function getBrandFromModel($model)
    {
        return DB::table('cars')
            ->select('brand')
            ->distinct()
            ->where('model', $model)
            ->first();
    }

    public function getCar($brand, $model)
    {
        return DB::table('cars')
            ->select('*')
            ->where('model', $model)
            ->where('brand', $brand)
            ->first();
    }
}