<?php

namespace App\Repositories;

use App\Http\Requests\UniversRequest;
use App\Models\Univers;

class UniversRepository extends ResourceRepository
{
    public function __construct(Univers $univers)
    {
        $this->model = $univers;
    }

    /**
     * @return static Collection
     */
    public function getAllUnivers()
    {
        return Univers::all()->pluck('identifiant');
    }

    public function getUnivers($identifiant)
    {
        return Univers::firstOrNew(['identifiant'=>$identifiant]);
    }

    public function displayUnivers($identifiant)
    {
        $univers = Univers::where('identifiant',$identifiant)->first();
        if (empty($univers->background))
            $univers->background = '../img/image-'.$univers->identifiant.'.jpg';
        return $univers;
    }

    public function updateUnivers(UniversRequest $request)
    {
        $universValues = $request->except(['_token', 'visuel','idUnivers']);
        if ($request->hasFile('visuel')) {
            $universValues['background'] = Univers::uploadUniversImage($request->file('visuel'));
        }
        if ($request->hasFile('visuelBas')) {
            $universValues['visuelBas'] = Univers::uploadUniversImage($request->file('visuelBas'));
        }
        $this->update($request->idUnivers, $universValues);
    }

    public function createUnivers(UniversRequest $request)
    {
        $universValues = $request->except(['_token', 'visuel','idUnivers']);
        if ($request->hasFile('visuel')) {
            $universValues['background'] = Univers::uploadUniversImage($request->file('visuel'));
        }
        if ($request->hasFile('visuelBas')) {
            $universValues['visuelBas'] = Univers::uploadUniversImage($request->file('visuelBas'));
        }
        $this->store($universValues);
    }
}
