<?php

namespace App\Repositories;

use App\Models\Recommandation;
use DB;

class RecommandationRepository extends ResourceRepository{

    public function __construct(Recommandation $recommandation)
    {
        $this->model = $recommandation;
    }

    /**
     * @return int
     */
    public function randomRecommandation()
    {
        return $this->model
            ->orderByRaw("RAND()")
            ->take(2)
            ->get();
    }
}
