<?php

namespace App\Repositories;

use App\Localisation\Polygon;
use App\Models\Offer;
use DB;

class OfferRepository extends ResourceRepository{

    public function __construct(Offer $offer)
    {
        $this->model = $offer;
    }

    public function searchOffers(Polygon $polygon, $brand = null, $model = null)
    {
        // SQL Polygon
        $sqlPolygon = $polygon->toSqlPolygon();

        $query = "SELECT o.id, o.price FROM offers as o";
        // Jointure
        if($brand !== '' || $model !== '') $query .= " INNER JOIN cars as c ON o.car_id = c.id";

        // Where
//        $query .= " WHERE st_Intersects( location,GeomFromText('$sqlPolygon'))";

        // ANDs
        if($brand !== '') $query .= " where c.brand = '$brand'";
        if($model !== '') $query .= " AND c.model = '$model'";

        // Offre active
        $query .= " AND o.paused=0";

        return DB::select($query);
    }

    /**
     * Get offers with car category which is $category
     * @param $categorie
     * @return Collection
     */
    public function offerCategorie($categorie)
    {
        $offers = DB::table('cars')
            ->join('offers', 'cars.id', '=', 'offers.car_id')
            ->where('cars.categorie', 'like', "%$categorie%")
            ->orderByRaw("RAND()")
            ->take(3)
            ->get();
        return $offers;
    }
}
