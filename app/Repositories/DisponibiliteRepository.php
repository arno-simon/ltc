<?php
/**
 * Created by PhpStorm.
 * User: stephanepau
 * Date: 13/09/15
 * Time: 15:43
 */

namespace App\Repositories;

use App\Localisation\Polygon;
use App\Models\Disponibilite;
use DB;

class DisponibiliteRepository extends ResourceRepository
{
    public function __construct(Offer $disponibilite)
    {
        $this->model = $disponibilite;
    }

    public function searchOffers(Polygon $polygon, $brand = null, $model = null)
    {
        // SQL Polygon
        $sqlPolygon = $polygon->toSqlPolygon();

        $query = "SELECT o.id, o.price FROM offers as o";
        /*        // Jointure
                if($brand !== '' || $model !== '') $query .= " INNER JOIN cars as c ON o.car_id = c.id";

                // Where
                $query .= " WHERE st_Intersects( location,GeomFromText('$sqlPolygon'))";

                // ANDs
                if($brand !== '') $query .= " AND c.brand = '$brand'";
                if($model !== '') $query .= " AND c.model = '$model'";
        */
        return DB::select($query);
    }
}