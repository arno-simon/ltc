<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Accueil
/*Route::get('/', function (){
    return view('home');
});
*/
Route::get('/coming-soon', function () {
    return view('coming-soon');
});

Route::post('/coming-soon', 'HomeController@postComingSoon');

Route::get('/', 'HomeController@viewHome');

Route::get('forfait', function() {
    return view('forfait');
});

Route::get('guides', ['uses'=>'GuideController@viewAllGuides']);

Route::get('guide/{id}', ['as'=>'guide.id', 'uses'=>'GuideController@guide']);

Route::get('reservation/{id}', ['as' => 'reservation', 'uses'=>'OfferController@getOfferReservation']);
/*    function (){
    return view('reservation');
});
*/
// Utilisateurs non connectés seulement
Route::group(['middleware' => 'guest'], function () {
    // Création de compte
    Route::get('/register/{parrainCode?}', 'Auth\RegistrationController@getRegister');
    Route::post('/register', ['as' => 'register.post', 'uses' => 'Auth\RegistrationController@postRegister']);

    // Création compte avec facebook
    Route::get('auth/facebook', 'Auth\RegistrationController@redirectToProvider');
    Route::get('auth/facebook/callback', 'Auth\RegistrationController@handleProviderCallback');

    // Création de compte pro
    Route::get('/register/pro', 'Auth\RegistrationController@getRegisterPro');
    Route::post('/register/pro', ['as' => 'register.post.pro', 'uses' => 'Auth\RegistrationController@postRegisterPro']);

    // Login
    Route::get('/login', 'Auth\LoginController@getLogin');
    Route::post('/login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@postLogin']);

    // Login dans le parcours de réservation
    Route::get('/creation-connexion', 'Auth\LoginController@getCreationConnexion');

    // Password reset link request routes...
    Route::get('/password/email', 'Auth\PasswordController@getEmail');
    Route::post('/password/email', ['as' => 'password.email.post', 'uses' => 'Auth\PasswordController@postEmail']);

    // Password reset routes...
    Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
    Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\PasswordController@postReset']);

    // Test iframe
});

Route::get('/frame', function () {
    return view('test-frame');
});

// Utilisateurs connectés seulement
Route::group(['middleware' => 'auth'], function () {
    // Email vérification
    Route::get('/register/verify/{confirmationCode}', 'Auth\RegistrationController@confirm');
    Route::get('/registration-success', 'Auth\RegistrationController@registerSuccess');

    // Logout
    Route::get('/logout', 'Auth\LoginController@logout');

    // My account
    Route::get('my-account', ['as'=>'my-account', 'uses'=>'UserController@myAccount']);
    Route::post('my-account', ['as'=>'profil', 'uses'=>'UserController@updateAccount']);
    Route::get('my-account/upload/img/{fichierCrop}',  'UserController@getCrop');
    Route::get('my-account/upload', 'UserController@uploadRedirect');

    // Gestion des images
    Route::get('/offer/upload/img/{fichierCrop}/{offer}/{imageIdx}', 'OfferController@getCrop');

    // Update account routes
    Route::get('/my-account/confirm-email', ['as' => 'account.mail.confirm', 'uses' => 'UserController@sendConfirmationEmail']);

    // New offer
    Route::get('/offer/new', ['as' => 'offer.new', 'uses' => 'OfferController@newOffer']);
    Route::post('/offer/new', ['as' => 'offer.post.new', 'uses' => 'OfferController@postNewOffer']);
    Route::get('/create-offer-success', 'OfferController@newOfferSuccess');

    // update offer
    Route::get('/offer/edit/{id}', ['as'=> 'offer.edit', 'uses'=>'OfferController@editOffer']);
    Route::post('/offer/edit/{id}', ['as'=>'offer.post.update', 'uses'=>'OfferController@updateOffer']);

    // conversation via messagerie
    Route::get('messages/{reservationActiveId?}', ['uses'=>'ConversationController@getMessages']);
    Route::post('messages', ['as'=>'messages.post', 'uses'=>'ConversationController@postMessages']);

    Route::get('paiement/{id}', 'ReservationController@getPaiementCredit');
    Route::post('paiement', 'ReservationController@postPaiement');
    // Get offers hours available
    Route::post('/horaires', 'ReservationController@horaires');

    // Parrainage
    Route::post('/parrainage', 'Auth\ParrainageController@postParrainage');

});

Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware', 'prefix' => 'admin'], function()
{
    // accueil
    Route::get('accueil', function () {
        return view('admin.accueil');
    });

    Route::get('edit', 'Admin\editCarController@getCarList');

    // Univers
    Route::get('univers', 'Admin\UniversController@showUnivers');
    // Edition d'un univers
    Route::get('univers/{id}', 'Admin\UniversController@showOneUnivers');
    Route::post('updateUnivers', 'Admin\UniversController@updateUnivers');
    //Création d'un univers
    Route::get('univers/new', 'Admin\UniversController@showOneUnivers');
    Route::get('create-univers-success', 'Admin\UniversController@newGuideSuccess');

    // Guides
    Route::get('guides', 'Admin\EditGuideController@showGuides');
    // Edition d'un guide
    Route::get('guide/{id}', 'Admin\EditGuideController@showGuide');
    Route::post('showGuide', 'Admin\EditGuideController@updateGuide');
    // Création d'un guide
    Route::get('guide/new', '\AdminEditGuideController@newGuide');
    Route::get('create-guide-success', 'AdminEditGuideController@newGuideSuccess');
});

//mail pour notation
Route::get('test-termine/{reservationId}', 'ReservationController@getMailTestTermine');
// Notation après un test
Route::get('/notation/{reservationId}', 'RecommandationController@getNotation');
Route::post('/merci', 'RecommandationController@postNotation');

// Autocomplete
Route::post('search/autocomplete/brand', 'SearchController@autocompleteBrand');
Route::post('search/autocomplete/model', 'SearchController@autocompleteModel');
Route::post('search/autocomplete/model-brand', 'SearchController@brandFromModel');

// Resultat
Route::post('/search/results', ['as' => 'results', 'uses' => 'SearchController@postSearch']);

// Profil
Route::get('/user/profil/{id}', ['as' => 'user.profil', 'uses' => 'UserController@getProfil']);

// Categorie
Route::get('/citadine', 'HomeController@citadine');
Route::get('/break',  'HomeController@touring' );
Route::get('/monospace', 'HomeController@monospace');
Route::get('/electrique', 'HomeController@electrique');
Route::get('/sportive', 'HomeController@sportive');
Route::get('/4x4', 'HomeController@quatrex4');
Route::get('/collection', 'HomeController@collection');
Route::get('/berline', 'HomeController@berline');
Route::get('/tuning', 'HomeController@tuning');

// Download PDF

Route::get('/img/fonctionnement-ltc.pdf', function () {
    return view('/img/fonctionnement-ltc.pdf');
});

//Message pulling
Route::get('/update-messages', 'ConversationController@streamedMessages');

//Route::post('crop.php', '');

Route::get('/testeur', function()
{
///    echo gethostname();
    return view('test');
});

