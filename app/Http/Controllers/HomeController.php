<?php
/**
 * Created by PhpStorm.
 * User: stephanepau
 * Date: 23/12/2015
 * Time: 14:55
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use App\Repositories\OfferRepository;
use App\Repositories\RecommandationRepository;
use App\Repositories\UniversRepository;
use DB;

class HomeController extends Controller
{
    protected $offerRepository;
    protected $recommandationRepository;
    protected $universRepository;

    public function __construct(OfferRepository $offerRepository, RecommandationRepository $recommandationRepository, UniversRepository $universRepository)
    {
        $this->offerRepository = $offerRepository;
        $this->recommandationRepository = $recommandationRepository;
        $this->universRepository = $universRepository;
    }

    public function viewHome()
    {
        $recommandations = $this->recommandationRepository->randomRecommandation();
        return view('home', ['recommandations'=>$recommandations]);
    }

    public function citadine()
    {
        return $this->voiture('citadine');
    }

    public function touring()
    {
        return $this->voiture('break');
    }

    public function berline()
    {
        return $this->voiture('berline');
    }

    public function sportive()
    {
        return $this->voiture('sportive');
    }

    public function collection()
    {
        return $this->voiture('collection');
    }

    public function quatrex4()
    {
        return $this->voiture('4x4');
    }

    public function electrique()
    {
        return $this->voiture('electrique');
    }

    public function monospace()
    {
        return $this->voiture('monospace');
    }

    public function tuning()
    {
        return $this->voiture('tuning');
    }

    public function postComingSoon(Request $request)
    {
        $mail = $request->only('mail');
        $validator = $this->validatorEmail($mail);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator);
        }

        DB::table('collect_mail')->insert(
            ['mail' => $mail]
        );
        return redirect()->back()->with('status', 'email enregistré');
    }

    protected function voiture($categorie)
    {
//        $univers = Models\Univers::where('identifiant', $categorie)->first();
        $univers = $this->universRepository->displayUnivers($categorie);
        $offers = $this->offerRepository->offerCategorie($categorie);
        $recommandations = $this->recommandationRepository->randomRecommandation();
        return view('home-categorie', ['univers'=>$univers, 'offers'=>$offers, 'recommandations'=>$recommandations]);
    }

    // Email
    protected function validatorEmail(array $data)
    {
        return \Validator::make($data, [
            'mail' => 'required|email',
        ]);
    }
}