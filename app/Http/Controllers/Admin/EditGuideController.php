<?php

namespace App\Http\Controllers\Admin;

use App\Models\Guide;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\GuideRequest;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Repositories\GuideRepository;
use App\Console\Commands;
use DB;

/**
 * Created by PhpStorm.
 * User: stephanepau
 * Date: 04/10/15
 * Time: 22:53
 */
class EditGuideController extends Controller
{
    protected $guideRepository;

    public function __construct(GuideRepository $guideRepository)
    {
        $this->guideRepository = $guideRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function showGuides()
    {
        $guides = DB::table('guides')->orderBy('id', 'desc')->get();
        return view('admin.showGuides', ['guides' => $guides]);
    }

    public function showGuide($id)
    {
        $guide = Guide::findOrNew($id);
        return view('admin.showGuide', ['guide'=>$guide]);
    }

    public function updateGuide(GuideRequest $request)
    {
        $guide = Guide::findOrNew($request->get('idGuide'));
        $guide->titre = $request->get('titre');
        $guide->contenu = $request->get('contenu');
        $guide->auteur = $request->get('auteur');
        if ($request->hasFile('visuel'))
        {
            $fileName = $request->file('visuel')->getClientOriginalName();
            $path = public_path() . '/img/guide/';
            $request->file('visuel')->move($path, $fileName);
            $guide->visuel = '/img/guide/'.$fileName;
        }
        $guide->save();

        if ($request->has('idGuide'))
            return redirect()->back()->with('status', 'Guide mis à jour');
        else
            return redirect('admin/create-guide-success');
    }

    public function newGuide()
    {
        $guide = new Guide();
        return view('admin.showGuide', ['guide'=>$guide]);
    }

    public function newGuideSuccess()
    {
        return view('admin.create-guide-success');
    }
}
