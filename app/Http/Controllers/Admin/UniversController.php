<?php

namespace App\Http\Controllers\Admin;

use App\Models\Univers;
use App\Http\Controllers\Controller;
use App\Repositories\UniversRepository;
use App\Http\Requests\UniversRequest;
use App\Console\Commands;


/**
 * Created by PhpStorm.
 * User: stephanepau
 * Date: 04/10/15
 * Time: 22:53
 */
class UniversController extends Controller
{
    protected $universRepository;

    /**
     * @param UniversRepository $universRepository
     */
    public function __construct(UniversRepository $universRepository)
    {
        $this->universRepository = $universRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function showUnivers()
    {
//        if (!\Auth::check())
//            return redirect('coming-soon');
        $univers = $this->universRepository->getAllUnivers();
        return view('admin.showUnivers', ['universes' => $univers]);
    }

    /**
     * @param string $id
     * @return \Illuminate\View\View
     */
    public function showOneUnivers($id)
    {
        $univers = $this->universRepository->getUnivers($id);
        return view('admin.showOneUnivers', ['univers'=>$univers]);
    }

    /**
     * @param UniversRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateUnivers(UniversRequest $request)
    {
        if ($request->has('idUnivers')) {
            $this->universRepository->updateUnivers($request);
            return redirect()->back()->with('status', 'Univers mis à jour');
        }
        else {
            $this->universRepository->createUnivers($request);
            return view('admin.create-univers-success');
        }
    }

    public function newGuideSuccess()
    {
        return view('admin.create-univers-success');
    }
}
