<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Repositories\CarRepository;
use App\Models\Car;
use App\Console\Commands;

/**
 * Created by PhpStorm.
 * User: stephanepau
 * Date: 30/08/15
 * Time: 22:53
 */
class editCarController extends Controller
{
    protected $carRepository;

    public function __construct(carRepository $carRepository)
    {
        $this->carRepository = $carRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getCarList()
    {
        $cars = Car::where('brand','Abarth')->get();
/*        $result = $this->carRepository->searchModelsFromBrands('', 'Abarth');
        if(count($result) > 0)
        {
            foreach ($result as $modele)
                $models[] = Car::find($modele->model);
        }

        $model = $result[0]->model;
*/
        return view('admin.editCar', ['cars' => $cars]);
    }
}