<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use DB;

class GuideController extends Controller
{
    protected $dateFormat = 'U';

    public function guide($id)
    {
        $guide = Models\Guide::findOrFail($id);
        return view('guide.guide', ['guide'=>$guide]);
    }

    public function viewAllGuides()
    {
        $guides = DB::table('guides')->where('actif', 1)->orderBy('id', 'desc')->take(5)->get();
        return view('/guide/allGuide', ['guides'=>$guides]);
    }
}