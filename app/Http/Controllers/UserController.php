<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Models\User;
use Storage;


class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | UserController, handle any user action
    |--------------------------------------------------------------------------
    */
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function myAccount()
    {
        $user = \Auth::user();
        $brands = explode(',',$user->favourite_brands);
        return view('user.account')->with(array('user' =>  $user, 'brands' => $brands));
    }

    public function getProfil($id)
    {
        $user = User::findOrFail($id);
        return view('user.profil')->with(array('user' =>  $user));
    }

    public function getCrop($crop)
    {
        $user = User::find(\Auth::user()->id);
        $user->picture = '/img/'.$crop;
        $user->save();
        $brands = explode(',',$user->favourite_brands);
        return redirect()->back();
 //       return view('user.account')->with(array('user' =>  $user, 'brands' => $brands));
    }

    public function uploadRedirect()
    {
//        return redirect()->back();
        $user = \Auth::user();
        $brands = explode(',',$user->favourite_brands);
        return view('user.account')->with(array('user' =>  $user, 'brands' => $brands));
    }

    /**
     * Send confirmation email to the logged user
     */
    public function sendConfirmationEmail()
    {
        $confirmationCode = str_random(30);

        \Auth::user()->confirmation_code = $confirmationCode;
        \Auth::user()->save();

        \Mail::send('emails.verify', array('confirmation_code' => $confirmationCode), function($message) {
            $message->to(\Auth::user()->email)
                ->subject('Verifier votre adresse e-mail');
        });

        return redirect()->back()->with('status', 'Email de confirmation envoyé');
    }

    /**
     * Update account
     * @todo : validation input data
     * @param $data : datas from form
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateAccount(Request $request)
    {
        $user = \Auth::user();
        $ville = $request->only('ville');
        $validator = $this->validatorVille($ville);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator);
        }
        $user->ville = $request->get('ville');
        $user->last_name = $request->get('last_name');
        $user->first_name = $request->get('first_name');
        $user->email = $request->get('email');
        $user->phone = $request->get('phone');
        $user->birthyear = $request->get('birthyear');

        if($request->hasFile('picture'))
            $user->picture = $this->postPicture($request);
        $request->has('isEvent') ? $user->isEvent = 1 : $user->isEvent = 0;

        $user->favourite_brands = $this->postBrands($request);

        $user->update();
        return redirect()->back()->with('status', 'Profil mis à jour');
    }

    public function postBrands(Request $request)
    {
        $brands = $request->only('brand1', 'brand2', 'brand3');

        /*        $validator = $this->validatorBrands($brands);


                   if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                }
        */
        $brands = '';
        if($request->get('brand1') !== '') $brands = $request->get('brand1');
        if($request->get('brand2') !== '') $brands .= ','.$request->get('brand2');
        if($request->get('brand3') !== '') $brands .= ','.$request->get('brand3');
        return $brands;
    }

    // Validators

    // LastName
    protected function validatorLastName(array $data)
    {
        return \Validator::make($data, [
            'last_name' => 'string',
        ]);
    }

    // First name
    protected function validatorFirstName(array $data)
    {
        return \Validator::make($data, [
            'first_name' => 'string',
        ]);
    }

    // Email
    protected function validatorEmail(array $data)
    {
        return \Validator::make($data, [
            'email' => 'required|email|unique:users',
        ]);
    }

    // Password
    protected function validatorPassword(array $data)
    {
        return \Validator::make($data, [
            'password' => 'required|confirmed|min:6|max:100',
        ]);
    }

    // Phone
    protected function validatorPhone(array $data)
    {
        return \Validator::make($data, [
            'phone' => 'min:10|max:12',
        ]);
    }

    // Brands
    protected function validatorBrands(array $data)
    {
        return \Validator::make($data, [
            'brand1' => 'exists:cars,brand',
            'brand2' => 'exists:cars,brand',
            'brand3' => 'exists:cars,brand'
        ]);
    }

    // Région
    protected function validatorRegion(array $data)
    {
        return \Validator::make($data, [
            'region' => 'string',
        ]);
    }

    // Ville
    protected function validatorVille(array $data)
    {
        return \Validator::make($data, [
            'ville' => 'string',
        ]);
    }

    // Code postale
    protected function validatorCodePostal(array $data)
    {
        return \Validator::make($data, [
            'cp' => 'min:5|max:10|alpha_num',
        ]);
    }

    // Picture
    protected function validatorPicture(array $data)
    {
        return \Validator::make($data, [
            'picture' => 'mimes:jpg,jpeg,bmp,png',
        ]);
    }
}
