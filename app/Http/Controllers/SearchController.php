<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use Illuminate\Http\Request;
use App\Repositories\CarRepository;
use App\Repositories\OfferRepository;
use App\Http\Requests\SearchRequest;
use App\Localisation\Utils;
use App\Localisation\Place;
use App\Models\User;
use App\Models\Car;
use App\Models\Color;



class SearchController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | SearchController, handle Ajax requests for autocomplete
    |--------------------------------------------------------------------------
    */
    protected $carRepository;
    protected $offerRepository;

    public function __construct(CarRepository $carRepository, OfferRepository $offerRepository)
    {
        $this->carRepository = $carRepository;
        $this->offerRepository =$offerRepository;
    }

    public function postSearch(SearchRequest $request, Utils $utils)
    {
        $offers = array();

        if($request->has('lat') && $request->has('lng'))
        {
            $place = new Place($request->get('lat'), $request->get('lng'));
            $rayon = $request->get('rayon') * 1000;
            $polygon = $utils->getPolygon($place, $rayon);
            $polygon->toSqlPolygon();
            $brand = $request->get('brand');
            $model = $request->get('model');

            $results = $this->offerRepository->searchOffers($polygon, $brand, $model);

            if(count($results) > 0)
            {
                $minimumPrice = $this->getMinimumPrice($results);

                foreach($results as $offer)
                {
                    $offers[] = Offer::find($offer->id);
                }
                return view('search.results', ['offers' => $offers, 'minimumPrice' => $minimumPrice, 'search' => $request->only('localisation', 'model', 'brand', 'rayon', 'lat', 'lng')]);
            }
        }
        return view('search.results', ['offers' => $offers, 'search' => $request->only('localisation', 'model', 'brand', 'rayon', 'lat', 'lng')]);
    }

    /**
     * Return brands in a JSON response to an Ajax request
     * The request should have a brand parameter to research
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function autocompleteBrand(Request $request)
    {
        if($request->ajax() && $request->has('brand'))
        {
                $results = array();

                // Get brands results
                $queries = $this->carRepository->searchBrands($request->get('brand'), 5);

                foreach ($queries as $query)
                {
                    $results[] = $query->brand;
                }

                return \Response::json($results);

        }
        return \Response::json("Error, this is not an ajax request");
    }

    /**
     * Return models in a JSON response to an Ajax request
     * The request should have a model and a brand (optionnal) parameter to research
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function autocompleteModel(Request $request)
    {
        if($request->ajax() && $request->has('model'))
        {
            $results = array();

            // Get models results
            if($request->has('brand'))
            {
                $queries = $this->carRepository->searchModelsFromBrands($request->get('model'), $request->get('brand'), 10);
            }
            else
            {
                $queries = $this->carRepository->searchModels($request->get('model'), 10);
            }

            foreach ($queries as $query)
            {
                $results[] = $query->model;
            }

            return \Response::json($results);

        }
        return \Response::json("Error, this is not an ajax request");
    }

    /**
     * Return model's brand
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function brandFromModel(Request $request)
    {

        if($request->ajax() && $request->has('model'))
        {
            $result = $this->carRepository->getBrandFromModel($request->get('model'));

            return \Response::json($result);
        }
        return \Response::json("Error, this is not an ajax request");

    }

    private function getMinimumPrice($offers)
    {
        $prices = array();
        foreach($offers as $offer)
        {
            $prices[] = $offer->price;
        }

        return min($prices);
    }

}
