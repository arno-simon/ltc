<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewOfferRequest;
use App\Repositories\CarRepository;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Repositories\OfferRepository;
use App\Models;
use Session;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OfferController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | OfferController, create, update, show user's offers
    |--------------------------------------------------------------------------
    */
    protected $userRepository;
    protected $offerRepository;
    protected $carRepository;

    public function __construct(UserRepository $userRepository, OfferRepository $offerRepository, CarRepository $carRepository)
    {
        $this->userRepository = $userRepository;
        $this->offerRepository = $offerRepository;
        $this->carRepository = $carRepository;
    }

    public function newOffer()
    {
        $fuels = Models\Fuel::all();
        $gearboxes = Models\Gearbox::all();
        $options = Models\Option::all();
        $colors = Models\Color::all();

        return view('offer.new')->with(
            [
                'fuels' => $fuels,
                'gearboxes' => $gearboxes,
                'options' => $options,
                'colors' => $colors
            ]);
    }

    /**
     *
     * @todo gestion de l'absence de dates sélectionnées
     * @param NewOfferRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postNewOffer(NewOfferRequest $request)
    {
//        var_dump($request->get('categorie'));
//        exit;

        $offer = Models\Offer::findOrNew($request->get('idOffer'));

        $brand = $request->get('brand');
        $model = $request->get('model');

        $car = Models\Car::where('brand', $brand)->where('model', $model)->first();

        if ($car instanceof Models\Car)
        {
            $offer->car_id = $car->id;

            $offer->description = $request->get('description');
            $offer->setLocation($request->get('lng'), $request->get('lat'));
            $offer->location_name = $request->get('localisation');
            $offer->annee = $request->get('annee');
            $offer->color_id = $request->get('color');
            $offer->fuel_id = $request->get('fuel');
            $offer->gearbox_id = $request->get('gearbox');
            $offer->price = $request->get('price');
            $offer->hour_from = $request->get('hour-from');
            $offer->hour_to = $request->get('hour-to');
            $offer->paused = $request->get('paused') === 'yes' ? true:false;
            $offer->preteur_id = $request->user()->id;
            $offer->save();

            // Pictures
            // Add available pictures
            for($i=0; $i<=3; $i++)
            {
                if (session()->has('visuel.'.$i))
                {
                    $picture = Models\Picture::firstOrNew(['offer_id' => $offer->id, 'imageIdx' => $i]);
                    $picture->imageIdx = $i;
                    $picture->visuel = session('visuel.'.$i);
                    $offer->pictures()->save($picture);
                }
            }

            // Options
            if ($request->has('options'))
                $offer->options()->sync($request->get('options'), true);

            // Horaires
            if ($request->get('dateSelected') != '')
            {
                Models\Disponibilite::where('offer_id', $offer->id)->delete();

                $dates = Models\Disponibilite::explodeRequest($request->get('dateSelected'));
                foreach ($dates as $date) {
                    $timeFrom = $request->get('hour-from') . ':00:00';
                    $timeTo = $request->get('hour-to') . ':00:00';
                    $date = explode('/', $date);
                    $mysqlDate = $date[2] . '-' . $date[1] . '-' . $date[0];
                    $offer->disponibilites()->save(new Models\Disponibilite(array('date' => $mysqlDate, 'timeFrom' => $timeFrom, 'timeTo' => $timeTo)));
                }
            }
            if ($request->has('idOffer'))
                return redirect()->back()->with('status', 'annonce mise à jour');
            else
                return redirect('/create-offer-success');
        }
        return redirect()->back()->withErrors(['car' => 'La voiture renseignée est introuvable']);
    }

    public function editOffer($id)
    {
        $user = \Auth::user();
        $offer = Models\Offer::findOrFail($id);
        if($user->id != $offer->preteur_id)
            return redirect('/');

        $fuels = Models\Fuel::all();
        $gearboxes = Models\Gearbox::all();
        $options = Models\Option::all();
        $colors = Models\Color::all();

        return view('offer.new')->with(
            [
                'fuels' => $fuels,
                'gearboxes' => $gearboxes,
                'options' => $options,
                'colors' => $colors,
                'offer' => $offer
            ]);
    }

    public function getCrop($crop, $offer_id, $imageIdx)
    {
        session(['visuel.'.$imageIdx => '/img/'.$crop]);
        try
        {
            $offer = Models\Offer::findOrFail($offer_id);
            $picture = Models\Picture::firstOrNew(['offer_id' => $offer_id, 'imageIdx' => $imageIdx]);
            $picture->imageIdx = $imageIdx;
            $picture->visuel = '/img/' . $crop;
            $offer->pictures()->save($picture);
        }
        catch(ModelNotFoundException $e)
        {
            // do nothing
        }
        return redirect()->back();
    }

    public function updateOffer(Request $request)
    {
        return redirect('/create-offer-success');
    }

    public function newOfferSuccess()
    {
        return view('offer.create-success');
    }

    public function getOfferReservation($id)
    {
        $offer = Models\Offer::findOrFail($id);
        $reservation = new Models\Reservation();
        $reservation->offer_id = $id;
        if (\Auth::user())
            $reservation->testeur_id = \Auth::user()->id;
        $reservation->preteur_id = $reservation->offer->preteur_id;
        $reservation->save();
        if (!\Auth::check()) {
            return redirect()->guest('/creation-connexion');
        }
        return view('reservation', ['offer'=>$offer, 'reservation_id'=>$reservation->id]);
    }
}
