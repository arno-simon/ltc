<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SearchRequest;
use App\Repositories\ConversationRepository;
use App\Models;
use App\Models\Reservation;
use DB;

class ConversationController extends Controller
{
    protected $conversationRepository;

    public function __construct(ConversationRepository $conversationRepository)
    {
        $this->conversationRepository = $conversationRepository;
    }

    /**
     * @todo : requete à revoir
     * @info : Le test reservationActiveId == -1 ne fonctionne pas. Cela aurait été mieux que $reservationActive== null
     * @param $reservationActiveId
     * @return \Illuminate\View\View
     */
    public function getMessages($reservationActiveId=-1)
    {
        if (!\Auth::check())
            return view('/');
        $reservations = Reservation::where('testeur_id', \Auth::user()->id)
                        ->where('paye', 1)->get();

        $resId = DB::select('SELECT distinct(R.id) FROM `conversations` AS C, reservations AS R, offers AS O WHERE C.destinataire_id= ? AND C.reservation_id=R.id AND R.offer_id=O.id AND O.preteur_id= ? AND R.paye=1', [\Auth::user()->id,\Auth::user()->id]);
        foreach($resId as $r)
        {
            $reservations->push(Reservation::find($r->id));
        }
            $reservationActive = Reservation::find($reservationActiveId);
            Models\Conversation::where('reservation_id', $reservationActiveId)
                ->where('destinataire_id', \Auth::user()->id)
                ->update(['read' => true]);
            if ($reservationActive == null)
                $reservationActive = $reservations->first();
        return view('auth.messagerie', ['reservations'=>$reservations, 'reservationActive'=>$reservationActive]);
    }

    public function postMessages(Request $request)
    {
        $conversation = new Models\Conversation();
        $conversation->reservation_id = $request->reservation_id;
        $conversation->expediteur_id = $request->expediteur_id;
        $conversation->texte = $request->messagerie;
        $reservation = Models\Reservation::findOrFail($request->reservation_id);
        if ($reservation->testeur_id != $request->expediteur_id)
            $conversation->destinataire_id = $reservation->testeur_id;
        else
            $conversation->destinataire_id = $reservation->offer->preteur_id;
        $conversation->save();

        \Mail::send('emails.message', array('conversation' => $conversation), function($message) use ($conversation) {
            $message->to($conversation->destinataire->email)
                ->subject('Vous avez un message');
        });

        return redirect()->back()->with('status', 'Message transmis');
    }
}