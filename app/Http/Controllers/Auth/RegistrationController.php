<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Parrainage;
use Illuminate\Http\Request;
use Auth;
use Socialite;
use App\Http\Controllers\Controller;

class RegistrationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration Controller
    |--------------------------------------------------------------------------
    */

    public function getRegister($parrainCode=-1)
    {
        $parrainage = Parrainage::firstOrNew(['parrain_code'=>$parrainCode]);

        return view('auth.register', ['parrainage'=>$parrainage]);
    }

    public function getRegisterPro()
    {
        return view('auth.register-pro');
    }

    public function postRegister(Request $request)
    {
        $inputs = $request->only('email', 'password', 'password_confirmation');

        $validator = $this->validator($inputs);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $confirmation_code = str_random(30);
        $inputs['confirmation_code'] = $confirmation_code;
        $user = $this->create($inputs);
        Auth::login($user);

        \Mail::send('emails.verify', array('confirmation_code' => $confirmation_code), function($message) {
            $message->to(\Input::get('email'))
                ->subject('Verifier votre adresse e-mail');
        });

        if ($request->get('parrainage') != null)
            $this->trtParrainage($request->get('parrainage'));

        return redirect()->intended('registration-success')->with('register_ok', '');
    }

    public function postRegisterPro(Request $request)
    {
        $inputs = $request->only('email', 'phone', 'password', 'password_confirmation');

        $validator = $this->validatorPro($inputs);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput($request->only('email', 'phone'))
                ->withErrors($validator);
        }
        $confirmation_code = str_random(30);

        $inputs['confirmation_code'] = $confirmation_code;

        Auth::login($this->createPro($inputs));

        \Mail::send('emails.verify', array('confirmation_code' => $confirmation_code), function($message) {
            $message->to(\Input::get('email'))
                ->subject('Verifier votre adresse e-mail');
        });

        return redirect('registration-success')->with('register_ok', '');
    }

    public function confirm($confirmation_code)
    {
        if( !$confirmation_code ) abort(404);

        $user = User::where('confirmation_code',$confirmation_code)->first();

        if ( !$user ) abort(404);

        $user->isMailConfirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        return redirect('registration-success')->with('confirmation_ok', '');
    }

    public function registerSuccess(Request $request)
    {
        if($request->session()->has('register_ok') || $request->session()->has('confirmation_ok'))
        {
            return view('auth.register-success');
        }

        return redirect('/');
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
//        try {
            $user = Socialite::driver('facebook')->user();
//        } catch (Exception $e) {
//            return redirect('auth/facebook');
//        }

        $authUser = $this->findOrCreateUser($user);

        Auth::login($authUser, true);

        return redirect('registration-success')->with('register_ok', '');
//        return redirect()->route('/');
    }

    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $facebookUser
     * @return User
     */
    private function findOrCreateUser($facebookUser)
    {
        $authUser = User::where('facebook_id', $facebookUser->id)->first();

        if ($authUser){
            return $authUser;
        }

        return User::create([
            'last_name' => $facebookUser->name,
            'email' => $facebookUser->email,
            'facebook_id' => $facebookUser->id,
            'avatar' => $facebookUser->avatar
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return \Validator::make($data, [
            'email' => 'required|email|max:100|unique:users',
            'password' => 'required|confirmed|min:6|max:100',
        ]);
    }

    protected function validatorPro(array $data)
    {
        return \Validator::make($data, [
            'email' => 'required|email|max:100|unique:users',
            'password' => 'required|confirmed|min:6|max:100',
            'phone' => 'required|min:10|max:12'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'confirmation_code' => $data['confirmation_code']
        ]);
    }

    protected function createPro(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => bcrypt($data['password']),
            'confirmation_code' => $data['confirmation_code']
        ]);
    }

    protected function trtParrainage($id)
    {
        $parrainage = Parrainage::findorFail($id);
        $parrainage->statut = 'inscrit';
        $parrainage->parrain_code = null;
        $parrainage->filleul_id = Auth::user()->id;
        $parrainage->save();

        if ($parrainage->parrain->filleuls->count() == 3)
        {
            $parrain = $parrainage->parrain;
            $parrain->isPremium = true;
            $parrain->save();
        }
        \Mail::send('emails.parrain', array('parrainage' => $parrainage), function($message) use ($parrainage) {
            $message->to($parrainage->parrain->email)
                ->subject('Vous avez un nouveau filleul inscrit');
        });
    }
}
