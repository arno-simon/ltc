<?php

namespace App\Http\Controllers\Auth;

use App\Models\Parrainage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ParrainageController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Parrainage Controller
    |--------------------------------------------------------------------------
    */

    public function postParrainage(Request $request)
    {
        $parrain_id = \Auth::user()->id;
        $parrainage = $request->only('mail', 'relation', 'message');
        $parrainage['parrain_id'] = $parrain_id;

        $validator = $this->validator($parrainage);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator);
        }
        $parrainage['parrain_code'] = str_random();
        $filleul = \App\Models\Parrainage::create($parrainage);

        \Mail::send('emails.filleul', array('filleul'=>$filleul), function($message) {
            $message->to(\Input::get('mail'))
                ->subject('Message de LTC');
        });

        return redirect()->intended('registration-success')->with('register_ok', '');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator =  \Validator::make($data, [
            'mail' => 'required|email|max:100|unique:parrainages,mail',
            'parrain_id' => 'required|integer',
            'relation' => 'required|in:Conjoint,Amis,Collègue',
            'message' => 'string'
        ]);

/*        $validator->after(function($validator) {
            if ($this->somethingElseIsInvalid()) {
                $validator->errors()->add('field', 'Something is wrong with this field!');
            }
        });*/
        return $validator;
    }
}
