<?php

namespace App\Http\Controllers\Auth;

use App\Models;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class LoginController extends Controller
{
    use AuthenticatesAndRegistersUsers;
    /*
    |--------------------------------------------------------------------------
    | Login - Logout Controller
    |--------------------------------------------------------------------------
    */

    public function getLogin()
    {
        return view('auth.login');
    }

    /**
     *
     * @todo: Pas de redirection dans le cas d'une connexion depuis la page creation-connexion
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function postLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $validator = $this->validator($credentials);

        if ($validator->fails()) {
            return redirect('/')
                ->withInput($request->only('email', 'remember'))
                ->withErrors($validator, 'login');
        }

        if (Auth::attempt($credentials, $request->has('remember'))) {
                return redirect()->intended();
        }

        return redirect('/')
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'attempt' => 'Adresse e-mail invalide ou mot de passe incorrect',
            ], 'login');
    }

    public function getCreationConnexion()
    {
        return view('creation-connexion');
    }

    public function logout()
    {
        Auth::logout();
        session()->flush();
        return redirect('/');
    }

    protected function validator(array $data)
    {
        return \Validator::make($data, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
    }


}
