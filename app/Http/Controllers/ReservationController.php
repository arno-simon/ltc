<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\SearchRequest;
use App\Models;
use DB;
use Log;

class ReservationController extends Controller
{
    public function __construct()
    {
        //$this->middleware('ajax', ['only' => 'horaires']);
    }

    public function getPaiementCredit($id)
    {
        $reservation = Models\Reservation::findOrFail($id);
        $reservation->testeur_id = \Auth::user()->id;
        $reservation->paye = 1;
        $reservation->save();

        $conversation = new Models\Conversation();
        $conversation->reservation_id = $reservation->id;
        $conversation->expediteur_id = 1;
        $conversation->texte = "Votre véhicule vient d'être reservé par ".$conversation->reservation->testeur->first_name ."! Cliquez sur répondre pour rentrer en contact.";
        $conversation->destinataire_id = $reservation->offer->preteur_id;
        $conversation->save();

        $conversation = new Models\Conversation();
        $conversation->reservation_id = $reservation->id;
        $conversation->expediteur_id = 1;
        $conversation->texte = "Votre réservation a bien été prise en compte. Vous êtes maintenant en contact avec ".$reservation->offer->preteur->first_name ."!";
        $conversation->destinataire_id = $reservation->testeur_id;
        $conversation->save();

        \Mail::send('emails.commande', array('reservation' => $reservation, 'preteur'=>$reservation->preteur, 'offer'=>$reservation->offer), function($message) use ($reservation) {
            $message->to($reservation->testeur->email)
                ->subject('Votre commande de véhicule');
        });
        \Mail::send('emails.reservation-preteur', array('reservation' => $reservation, 'preteur'=>$reservation->preteur, 'offer'=>$reservation->offer), function($message) use ($reservation) {
            $message->to($reservation->preteur->email)
                ->subject('Félicitation ! votre véhicule a été réservé');
        });

        return view('auth.reservation-success');
    }

    public function postPaiement(Request $request)
    {
        $reservation = Models\Reservation::findOrFail($request->reservation_id);
        $reservation->testeur_id = \Auth::user()->id;
        $reservation->reservation_at = Carbon::createFromFormat('d/m/Y H',
            $request->dateSelected.' '.$request->heure);
        $reservation->paye = 1;
        $reservation->save();

        $conversation = new Models\Conversation();
        $conversation->reservation_id = $reservation->id;
        $conversation->expediteur_id = 1;
        $conversation->texte = "Votre véhicule vient d'être reservé par ".$conversation->reservation->testeur->first_name ."! Cliquez sur répondre pour rentrer en contact.";
        $conversation->destinataire_id = $reservation->offer->preteur_id;
        $conversation->save();

        $conversation = new Models\Conversation();
        $conversation->reservation_id = $reservation->id;
        $conversation->expediteur_id = 1;
        $conversation->texte = "Votre réservation a bien été prise en compte. Vous êtes maintenant en contact avec ".$reservation->offer->preteur->first_name ."!";
        $conversation->destinataire_id = $reservation->testeur_id;
        $conversation->save();

        \Mail::send('emails.commande', array('reservation' => $reservation, 'preteur'=>$reservation->preteur, 'offer'=>$reservation->offer), function($message) use ($reservation) {
            $message->to($reservation->testeur->email)
                ->subject('Votre commande de véhicule');
        });
        \Mail::send('emails.reservation-preteur', array('reservation' => $reservation, 'preteur'=>$reservation->preteur, 'offer'=>$reservation->offer), function($message) use ($reservation) {
            $message->to($reservation->preteur->email)
                ->subject('Félicitation ! votre véhicule a été réservé');
        });

        return view('auth.reservation-success');
    }

    public function getMailTestTermine($id)
    {
        $reservation = Models\Reservation::findOrFail($id);
        \Mail::send('emails.test-termine', ['reservation'=>$reservation], function($message) use ($reservation) {
           $message->to($reservation->testeur->email)
               ->subject('Test du véhicule terminé ? Venez noter votre test');
        });
    }

    /**
     * @todo : Le datepicker doit pouvoir nous renvoyer une date mieux formatée
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function horaires(Request $request)
    {
        $response = $request->only('date');
        $dateString = $response['date'][0];
        $dateString = substr($dateString, 0, -15);
        $date = Carbon::createFromFormat('D M j Y H:i:s', $dateString);
        $response = $request->only('offer_id');
        $dispo = Models\Disponibilite::where('offer_id', $response['offer_id'])->where('date', $date->format('Y-m-d'))->get();
        $heureDebut = Carbon::createFromFormat('H:i:s', $dispo[0]->timeFrom)->format('G');
        $heureFin = Carbon::createFromFormat('H:i:s', $dispo[0]->timeTo)->format('G');
        return response()->json(['debut' => $heureDebut, 'fin'=>$heureFin]);
    }
}