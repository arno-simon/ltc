<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\Recommandation;
use App\Models;
use DB;

class RecommandationController extends Controller
{
//    protected $dateFormat = 'U';
    protected $casts = ['explications'=>'integer'];

    public function getNotation($reservationId)
    {
        $reservation = Models\Reservation::findorFail($reservationId);
        \Auth::login($reservation->testeur);

        return view('formulaire-fin', ['reservation'=>$reservation]);
    }

    public function postNotation(Request $request)
    {
        $inputs = Input::all();
        $recommandation = new Recommandation($inputs);// how automatically cast form data to integer
        $recommandation->explication = $inputs['explication'];
        $recommandation->accueil = $inputs['accueil'];
        $recommandation->duree = $inputs['duree'];
        $recommandation->ambiance = $inputs['ambiance'];
        $recommandation->reservation_id = $inputs['reservation_id'];

        $reservation = Models\Reservation::findorFail($inputs['reservation_id']);
        $recommandation->offer_id = $reservation->offer_id;
        $recommandation->user_id = $reservation->testeur_id;
        $recommandation->save();

        return view('merci');
    }
}