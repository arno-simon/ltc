<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!is_object($request->user()))
            return redirect()->guest('/login');

        if (!$request->user()->isAdmin)
        {
            return redirect()->guest('/login');
        }

        return $next($request);
    }

}