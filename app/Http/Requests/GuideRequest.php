<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class GuideRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @todo les règles
     * @return array
     */
    public function rules()
    {
        return [
            'titre' => 'required|string|max:255',
            'auteur' => 'required',
            'contenu'=> 'required',
        ];
    }
}