<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewOfferRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand' => 'required|string',
            'model' => 'required|string',
            'annee' => 'required|numeric',
            'description' => 'required|string',
            'fuel' => 'required|string',
            'gearbox' => 'required|string',
            'price' => 'required|numeric',
            'localisation' => 'required|string',
//            'picture' => 'required|mimes:jpg,jpeg,bmp,png',
            'lng' => 'required',
            'lat' => 'required'
        ];
    }

}