$( document ).ready(function() {

	/* ------ hauteur aligné des deux div profil perso -----------*/

	/*var isDesktop = window.matchMedia("only screen and (min-width: 767px)");

	if(isDesktop.matches) {
		var hauteurProfilGauche = $(".profil-perso-gauche").height();
		var hauteurProfilDroite = $(".profil-perso-droite").height();

		if(hauteurProfilGauche > hauteurProfilDroite) {
			$(".profil-perso-droite .container-info").css("height", hauteurProfilGauche);
		}else {
			$(".profil-perso-gauche .container-info").css("height", hauteurProfilDroite);
		}
	}*/

    /* ------ fonctionnement js page messagerie -----------*/

	$(".block-reception-droite .contenu-messagerie").hide();
	$(".block-reception-droite .contenu-messagerie.active").show();

	var hauteurBlocGauche = $('.block-reception-gauche').height();
	var hauteurBlocDroit = $('.block-reception-droite .contenu-messagerie.active').height();
	$(".block-reception-droite").css("min-height", hauteurBlocGauche - 15);

	var isDesktop = window.matchMedia("only screen and (min-width: 767px)");

	var hauteurHeader = $("header").height();
	var hauteurFooter = $("footer").height();
	var hauteurEcran = $(window).height();
	var calcul = hauteurEcran-hauteurHeader-hauteurFooter;

	var largeurli = $(".block-reception-gauche li").width();
	var largeurLiImg = $(".block-reception-gauche li figure").width();
	var calculLiReplie = largeurli - largeurLiImg + 10;

	if(isDesktop.matches) {
		$(".page-messagerie").css("min-height", calcul);
		var hauteurPageMessagerie = $(".page-messagerie").height();
		var hauteurh1 = $(".page-messagerie h1").height();
		var hauteurh2 = $(".page-messagerie h1 .bandeau-titre-noir").height();
		var calculMessagerie = hauteurPageMessagerie - hauteurh1 - hauteurh2;
		$(".container-reception").css("min-height", calculMessagerie);

		$(".block-reception-gauche li").css("left", calculLiReplie);
		$(".block-reception-gauche li.active").css("left", 0);

		$('.block-reception-gauche li').on('click', function() {
			var idMessage = $(this).attr("id");
			$('.block-reception-gauche li').removeClass('active');
			$('#'+idMessage).addClass('active');
			$(".block-reception-gauche li").animate({
				left: calculLiReplie,
			}, 300, function() {
				// Animation complete.
			});
			$('#'+idMessage).animate({
				left: 0,
			}, 300, function() {
				// Animation complete.
			});
			/*$(".block-reception-droite .contenu-messagerie.active").fadeOut(600, function() {
				$(".block-reception-droite .contenu-messagerie.active").removeClass('active');
				$(".block-reception-droite #contenu-"+idMessage).show(600);
				$(".block-reception-droite #contenu-"+idMessage).addClass('active');
				$(".block-reception-droite").css("height", 'auto');
			});*/
			$(".block-reception-droite .contenu-messagerie.active").animate({
				opacity: 0,
			}, 600, function() {
				$(".block-reception-droite .contenu-messagerie.active").hide();
				$(".block-reception-droite .contenu-messagerie.active").removeClass('active');
				$(".block-reception-droite #contenu-"+idMessage).addClass('active');
				$(".block-reception-droite #contenu-"+idMessage).animate({
					opacity: 1,
				}, 600, function() {
					$(".block-reception-droite .contenu-messagerie.active").show();
					$(".block-reception-droite").css("height", 'auto');
				});
			});
		});
	}

	$('.block-reception-gauche li').on('click', function() {
		var idMessage = $(this).attr("id");
		$('.block-reception-gauche li').removeClass('active');
		$('#'+idMessage).addClass('active');
		/*$(".block-reception-droite .contenu-messagerie.active").fadeOut(600, function() {
			$(".block-reception-droite .contenu-messagerie.active").removeClass('active');
			$(".block-reception-droite #contenu-"+idMessage).show(600);
			$(".block-reception-droite #contenu-"+idMessage).addClass('active');
			$(".block-reception-droite").css("height", 'auto');
		});*/
		$(".block-reception-droite .contenu-messagerie.active").animate({
			opacity: 0,
		}, 600, function() {
			$(".block-reception-droite .contenu-messagerie.active").hide();
			$(".block-reception-droite .contenu-messagerie.active").removeClass('active');
			$(".block-reception-droite #contenu-"+idMessage).addClass('active');
			$(".block-reception-droite #contenu-"+idMessage).animate({
				opacity: 1,
			}, 600, function() {
				$(".block-reception-droite .contenu-messagerie.active").show();
				$(".block-reception-droite").css("height", 'auto');
			});
		});
	});

	/* ------ lightbox page resultat -----------*/

	/* activate the carousel */
	$("#modal-carousel").carousel({interval:false});

	/* change modal title when slide changes */
	$("#modal-carousel").on("slid.bs.carousel", function () {
		$(".modal-title").html($(this).find(".active img").attr("title"));
	})

	/* when clicking a thumbnail */
	$(".row .img-lightbox").click(function(){
		var content = $(".carousel-inner");
		var title = $(".modal-title");

		content.empty();
		title.empty();

		var id = $(this).attr("data-image");
		var repoCopy = $("#img-repo .item."+id).clone();
		var active = repoCopy.first();

		active.addClass("active");
		title.html(active.find("img").attr("title"));
		content.append(repoCopy);

		// show the modal
		$("#modal-gallery").modal("show");
	});

	/* -------- page accueil -------------*/

	/*carousel bootstrap bandeau*/
	var isPC = window.matchMedia("only screen and (min-width: 767px)");
	var ismobile = window.matchMedia("only screen and (max-width: 769px)");

	if(isPC.matches) {
		$('#carousel-bandeau').carousel({
			interval: false
		})
	}
	$('#carousel-bandeau').carousel({
		interval: false
	})

	/*carousel lien univers*/
	$('#carousel-link-univers').carousel({
		interval: false
	})

	/*carousel avis*/
	$('#carousel-avis').carousel({
		interval: false
	})

	/* ------- placeholder ----------- */
	$('[placeholder]').focus(function() {
		var input = $(this);
		if (input.val() == input.attr('placeholder')) {
			input.val('');
			input.removeClass('placeholder');
		}
	}).blur(function() {
		var input = $(this);
		if (input.val() == '' || input.val() == input.attr('placeholder')) {
			input.addClass('placeholder');
			input.val(input.attr('placeholder'));
		}
	}).blur();

	$('[placeholder]').parents('form').submit(function() {
		$(this).find('[placeholder]').each(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
			}
		})
	});

	/* page comming soon et video page home */
	function tailleBgVideo()
	{
		/* centrage vertical sur home */
		var ispc = window.matchMedia("only screen and (min-width: 767px)");
		if(ispc.matches) {
			var divGauche = $('.contenu-bandeau-haut .container-univers').height();
			var divDroite = $('.contenu-bandeau-haut .container-form-search').height();
			if(divDroite > divGauche){
				var marginTop = (divDroite-divGauche)/2;
				$('.contenu-bandeau-haut .container-univers').css('margin-top', marginTop);
			}

		}

		var hauteurEcran = $(window).height();
		var largeuEcran = $(window).width();

		if(hauteurEcran > largeuEcran){
			$('#bgvid').css('height', '100%').css('width', 'auto');
		}
		else {
			$('#bgvid').css('height', 'auto').css('width', '100%');
		}

		if(largeuEcran < 600) {
			$('#bgvidHome').css('height', '100%').css('width', 'auto');
			$('#gifHome').css('height', '100%').css('width', 'auto');
		}
		else {
			$('#bgvidHome').css('height', 'auto').css('width', '100%');
			$('#gifHome').css('height', '100%').css('width', '100%');
		}

		$('#bgvid').show();
		$('#bgvidHome').show();
		$('#gif').hide();
		$('#gifHome').hide();

		var ismobile = window.matchMedia("only screen and (max-width: 641px)");
		if(ismobile.matches) {
			var contenu = $('#contenu').height();
			var calcul = hauteurEcran-(contenu/2);
			$('#contenu').css('margin-top', calcul);
		}

		if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPad/i)))
		{
			$('#bgvid').hide();
			$('#bgvidHome').hide();
			$('#gif').show();
			$('#gifHome').show();
		}
		else
		{
			$('#bgvid').show();
			$('#bgvidHome').show();
			$('#gif').hide();
			$('#gifHome').hide();
		}
	}

	tailleBgVideo();
	$(window).resize(tailleBgVideo);

});