$( document ).ready(function() {

	/*fonctionnement div connexion */

	var connexionHeight = ($(".connexion-header").height() + 4);

	var isMobile = window.matchMedia("only screen and (max-width: 767px)");

	$(".connexion-header").css("top",-connexionHeight);

/*
	if($('#error-login').text().length != 38)
	{
		$(".connexion-header").css("top",100);
	}*/

	$(".link-connexion-header").on('click',function() {

		var pTop = $(".connexion-header").position();

		if(pTop.top == 100) {
			$(".connexion-header").animate({
			    top: -connexionHeight,
			  	}, 1000, function() {
			    // Animation complete.
	  		});
		}else if(isMobile.matches) {
			$(".connexion-header").animate({
			    top: 51,
			  	}, 1000, function() {
			    // Animation complete.
		  	});
		}else {
			$(".connexion-header").animate({
			    top: 100,
			  	}, 1000, function() {
			    // Animation complete.
		  	});
		}

		return false;
	});

	$(".close-connexion").on('click',function() {
        $(".block-flou").removeClass('flou');
		$(".connexion-header").animate({
		    top: -connexionHeight,
		  	}, 1000, function() {
		    // Animation complete.
            $(".block-flou").removeClass('flou');
  		});
	});

	/* fonctionnement div relation */

	var relationHeight = ($(".container-list-relation").height() + 24);

	$(".container-list-relation").css("top",-relationHeight);

	$(".link-mise-relation").on('click',function() {

		var pTop = $(".container-list-relation").position();

		if(pTop.top == 100) {
            $(".block-flou").removeClass('flou')
			$(".container-list-relation").animate({
			    top: -relationHeight,
			  	}, 1000, function() {
			    // Animation complete.
			    $(".container-list-relation").hide();
	  		});
		}else if(isMobile.matches) {
            $(".block-flou").addClass('flou')
			$(".container-list-relation").show();
			$(".container-list-relation").animate({
			    top: 51,
			  	}, 1000, function() {
			    // Animation complete.
		  	});
		}else {
            $(".block-flou").addClass('flou')
			$(".container-list-relation").show();
			$(".container-list-relation").animate({
			    top: 100,
			  	}, 1000, function() {
			    // Animation complete.
		  	});
		}
	});

	$(".close-relation span").on('click',function() {
        $(".block-flou").removeClass('flou')
		$(".container-list-relation").animate({
		    top: -relationHeight,
		  	}, 1000, function() {
		    // Animation complete.
		    $(".container-list-relation").hide();
  		});
	});
	
});